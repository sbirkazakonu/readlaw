/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Martin
 */
public class FindText {

    public List<String> allHtmlTagAttributes(String code, String tag) {
        List<String> result = new ArrayList<String>();
        List<Integer> starts = allHtmlTagStart(code, tag);
        List<Integer> ends = allHtmlTagEnd(code, tag);

        for (int i = 0; i < starts.size(); i++) {
            String tagText = "";
            for (int j = starts.get(i); j < ends.get(i); j++) {
                tagText += code.charAt(j);
            }
            result.add(tagText);
        }

        return result;
    }
    /*
     allTextIndexStart - Zadam slovo z textu a vrati se mi list indexu,
     kde vsude to presny slovo zacina v textu. Pokud je tam slovo detkrat, 
     vrati se mi deset hodnot indexu, kde to slovo zacina
     */

    public List<Integer> allTextIndexStart(String text, String scrap) {
        String help = "";
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < text.length() - scrap.length(); i++) {
            //System.out.println(text.substring(i, i+scrap.length()));
            if (text.substring(i, i + scrap.length()).equals(scrap)) {
                result.add(i);
            }
        }
        return result;
    }

    public static List<Integer> allHtmlTagStart(String code, String tag) {
        List<Integer> startList = new ArrayList<Integer>();
        //System.out.println(code);
        int breaks = 0;
        for (int i = 0; isText(code, tag); i++) {
            int start = textIndexStart(code, tag);
            //int end = textIndexEnd(code, tag);
            //String fullTag = getDeterText(code, tag);
            for (int j = start; j > 0; j--) {
                //fullTag = code.charAt(j) + fullTag;
                if (code.charAt(j) == '<') {
                    startList.add(j + breaks);
                    breaks += start + tag.length();
                    code = code.substring(start + tag.length());
                    break;
                }

            }

        }
        //System.out.println(startList);
        return startList;
    }

    public static List<Integer> allHtmlTagEnd(String code, String tag) {
        List<Integer> endList = new ArrayList<Integer>();
        //System.out.println(code);
        int breaks = 1;
        for (int i = 0; isText(code, tag); i++) {
            int start = textIndexStart(code, tag);
            //int end = textIndexEnd(code, tag);
            //String fullTag = getDeterText(code, tag);
            for (int j = start; j < code.length(); j++) {
                //fullTag = code.charAt(j) + fullTag;
                if (code.charAt(j) == '>') {
                    endList.add(j + breaks);
                    breaks += start + tag.length() + 1;
                    code = code.substring(start + tag.length());
                    break;
                }

            }

        }
        //System.out.println(endList);
        return endList;
    }

    public static String getDeterText(String text, String scrap) {
        int start = textIndexStart(text, scrap);
        int end = textIndexEnd(text, scrap);
        String result = "";
        for (int i = start; i < end; i++) {
            result += text.charAt(i);
        }
        return result;

    }

    public List<String> HtmlTag(String code, String tag) {
        int start = this.textIndexStart(code, tag);
        int end = this.textIndexEnd(code, tag);
        String fullTag = getDeterText(code, tag);
        for (int i = textIndexStart(code, tag); i < 0; i--) {
            fullTag = code.charAt(i) + fullTag;
            if (code.charAt(i) == '<') {

                break;
            }
        }
        for (int i = textIndexEnd(code, tag); i < code.length(); i++) {

        }
        return null;
    }

    public static int textIndexStart(String text, String scrap) {
        String help = "";
        for (int i = 0; i < text.length(); i++) {
            if (i < scrap.length()) {
                help += text.charAt(i);
            } else {
                help += text.charAt(i);
                String help2 = help;
                help = "";
                for (int j = 1; j < help2.length(); j++) {
                    help += help2.charAt(j);
                }

            }
            if (scrap == null ? help == null : scrap.equals(help)) {
                //System.out.println("score");
                return i - scrap.length() + 1;
            }

        }
        return 0;
    }

    public static int textIndexEnd(String text, String scrap) {
        String help = "";
        for (int i = 0; i < text.length(); i++) {
            if (i < scrap.length()) {
                help += text.charAt(i);
            } else {
                help += text.charAt(i);
                String help2 = help;
                help = "";
                for (int j = 1; j < help2.length(); j++) {
                    help += help2.charAt(j);
                }

            }
            if (scrap == null ? help == null : scrap.equals(help)) {
                //System.out.println("score");
                return i + 1;
            }

        }
        return 0;
    }

    public static boolean isText(String text, String scrap) {
        String help = "";
        for (int i = 0; i < text.length(); i++) {
            if (i < scrap.length()) {
                help += text.charAt(i);
            } else {
                help += text.charAt(i);
                String help2 = help;
                help = "";
                for (int j = 1; j < help2.length(); j++) {
                    help += help2.charAt(j);
                }

            }
            if (scrap.equals(help)) {
                return true;
            }

        }

        return false;
    }

    public static boolean isTextIgnoreCase(String text, String scrap) {
        String help = "";
        for (int i = 0; i < text.length(); i++) {
            if (i < scrap.length()) {
                help += text.charAt(i);
            } else {
                help += text.charAt(i);
                String help2 = help;
                help = "";
                for (int j = 1; j < help2.length(); j++) {
                    help += help2.charAt(j);
                }

            }
            if (scrap.equalsIgnoreCase(help)) {
                return true;
            }

        }

        return false;
    }

    public static List<String> findTagContent(String code, String tag) {
        List<String> result = new ArrayList<>();
        String beginTag = "<" + tag + ">";
        String closeTag = "</" + tag + ">";
        String value = "";
        boolean counting = false;
        for (int i = 0; i < code.length(); i++) {
            if (i + beginTag.length() < code.length() && code.substring(i, i + beginTag.length()).equalsIgnoreCase(beginTag)) {
                //System.out.println(code.substring(i, i + beginTag.length()));
                counting = true;
            }
            if (counting) {
                value = value + code.charAt(i);
            }
            if (i - closeTag.length() > 0 && code.substring(i - closeTag.length(), i).equalsIgnoreCase(closeTag)) {
                //System.out.println(code.substring(i-closeTag.length(), i));
                counting = false;
                result.add(value);
                value = "";
            }
        }
        return result;
    }
        public static List<String> findTagContentWithoutTags(String code, String tag) {
        List<String> result = new ArrayList<>();
        String beginTag = "<" + tag + ">";
        String closeTag = "</" + tag + ">";
        String value = "";
        boolean counting = false;
        for (int i = 0; i < code.length(); i++) {
            if (i + beginTag.length() < code.length() && code.substring(i, i + beginTag.length()).equalsIgnoreCase(beginTag)) {
                //System.out.println(code.substring(i, i + beginTag.length()));
                counting = true;
            }
            if (counting) {
                value = value + code.charAt(i);
            }
            if (i - closeTag.length() > 0 && code.substring(i - closeTag.length(), i).equalsIgnoreCase(closeTag)) {
                //System.out.println(code.substring(i-closeTag.length(), i));
                counting = false;
                value = value.replaceAll("<"+tag.toUpperCase()+">","");
                value = value.replaceAll("</"+tag.toUpperCase()+">","");     
                //System.out.println("</"+tag+">");
                result.add(value);
                value = "";
            }
        }
        
        return result;
    }

    public static String findTagContentWithoutTagsSingle(String code, String tag) {
        String result = "";
        String beginTag = "<" + tag + ">";
        String closeTag = "</" + tag + ">";

        boolean counting = false;
        for (int i = 0; i < code.length(); i++) {
            if (i - beginTag.length() > 0 && code.substring(i - beginTag.length(), i).equalsIgnoreCase(beginTag)) {
                //System.out.println(code.substring(i, i + beginTag.length()));
                counting = true;
            }

            if (i + closeTag.length() < code.length() && code.substring(i, i + closeTag.length()).equalsIgnoreCase(closeTag)) {
                //System.out.println(code.substring(i-closeTag.length(), i));

                return result;

            }
            if (counting) {
                result = result + code.charAt(i);
            }
        }
        return result;

    }
}
