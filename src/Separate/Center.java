/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Separate;

import Utils.Util;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Martin
 */
public class Center {

    static int[][] pixels;
    static BufferedImage img;
    static String filePath;
    static int centerDensity;

    static int height = 20;

    public static Rectangle getCenter(BufferedImage img) {

        // Vytvoří obrázek z předeslané cesty k němu, která vstupuje jako parametr metody s 
        Center.img = img;
        //Získá informace o barvách na každém pozadí obrázku
        pixels = Util.getPixelArray(img);
        //Najde střed dané stŕanky zákona - protože je to ve dvou sloupcích
        centerDensity = getBlackCount();
        Rectangle middle = getMiddle(img);
        //najde kde je samotný text zákona (tzn. odkud pokud je dvousloupcové znění zákona)
//        String savePath = "/Users/Martin/Documents/zakony/test/" + Math.round(Math.random() * 10000) + s.substring(s.lastIndexOf("/") + 1);
//        Util.saveImage(img, savePath);
        return middle;
    }

    static Rectangle getCenter(String s) {

        // Vytvoří obrázek z předeslané cesty k němu, která vstupuje jako parametr metody s 
        img = Util.readImage(s);
        //Získá informace o barvách na každém pozadí obrázku
        pixels = Util.getPixelArray(img);
        //Najde střed dané stŕanky zákona - protože je to ve dvou sloupcích
        centerDensity = getBlackCount();
        Rectangle middle = getMiddle(img);
        //najde kde je samotný text zákona (tzn. odkud pokud je dvousloupcové znění zákona)
        //String savePath = "/Users/Martin/Documents/zakony/test/" + Math.round(Math.random() * 10000) + s.substring(s.lastIndexOf("/") + 1);
        //Util.saveImage(img, savePath);
        return middle;
    }

    static BufferedImage paintCenter(String s) {
        Center.filePath = s;

        // Vytvoří obrázek z předeslané cesty k němu, která vstupuje jako parametr metody s 
        img = Util.readImage(s);
        //Získá informace o barvách na každém pozadí obrázku
        pixels = Util.getPixelArray(img);
        //Najde střed dané stŕanky zákona - protože je to ve dvou sloupcích
        centerDensity = getBlackCount();

        Rectangle rect = getMiddle(img);
        //Util.saveImage(img, "/Users/Martin/Documents/zakony/2955/"+Math.round(Math.random()*1000)+ ".png");
        //najde kde je samotný text zákona (tzn. odkud pokud je dvousloupcové znění zákona)
        return img;
    }

    //získá prostředek zákona, protože je ve dvou sloupcích je to nutné
    private static Rectangle getMiddle(BufferedImage img) {
        int result = 0;

        List<Integer> middle = new ArrayList();
        for (int i = (int) (pixels[0].length * 0.46); i < (int) (pixels[0].length * 0.53); i++) {

            if (isMiddle(i)) {
                middle.add(i);
            }
        }
        //System.out.println("middle size " + middle.size());
        Rectangle rect = colorMiddle(middle);

        return rect;
    }

    private static boolean isMiddle(int column) {
        int blackCount = 0;
        //System.out.println("pixels.length : "+ pixels.length);
        for (int i = 0; i < pixels.length; i++) {
            if (pixels[i][column] != -1) {
                //System.out.println(column+ " - " + pixels[i][column]);
                blackCount++;
            }
        }
        //System.out.println("Column :" + column + "BlackCount " + blackCount);
        if (blackCount < centerDensity) {
            return true;
        }
        return false;
    }

    private static Rectangle colorMiddle(List<Integer> middle) {

        int horizontal = getHorizontal(middle);
        System.out.println("horiz: "+horizontal);
        List<Integer> vertical = getVerticalDimension(horizontal);
        vertical = shapeVertical(vertical);
        for (int i = 0; i < vertical.size(); i++) {
            //img.setRGB(horizontal, vertical.get(i), 8000000);
        }
        //Util.saveImage(img, "/Users/Martin/Documents/weby/1822/test/_"+Math.random()*1000+".png");
        
        Rectangle rect = new Rectangle(horizontal, vertical.get(0), 1, vertical.get(vertical.size() - 1) - vertical.get(0));
        return rect;

    }

    /**
     * Řeší vertikální umístění střední čáry - aby tam nebyly nadpisy a pičoviny
     * ale vyloženě pouze dva sloupce,kde je text zákona
     *
     * Ve dvou fázích - 1) Okolo musí být dotatek bílýho prostoru 2) V tom řádku
     * přibližně musí být typický sloupcový rozdělení
     *
     * @param horizontal - Index prostředního sloupce
     * @return Hodnoty, ve kterých je prostředek vertikálně rozloženej
     */
    private static List<Integer> getVerticalDimension(int horizontal) {
        List<Integer> result = new ArrayList();
        int width = 40;
        int height = 20;
        int verticalLevel = 0;
        int index = 0;
        /**
         * První úroveň
         */
        boolean firstTest[] = firstLevelTest(verticalLevel, horizontal, width, height);
        for (int i = 0; i < firstTest.length; i++) {
            for (int j = 0; j < height && firstTest[i]; j++) {
                //img.setRGB(horizontal - 15, j + height * i, 1543210);
            }
        }

        boolean thirdTest[] = thirdTest(horizontal, height);
        for (int i = 0; i < thirdTest.length; i++) {
            for (int j = 0; j < height && thirdTest[i]; j++) {
                //img.setRGB(horizontal - 15, j + height * i, 1543210);
            }
        }
        boolean footnote[] = findFootNote();
        for (int i = 0; i < footnote.length; i++) {
            for (int j = 0; j < height && footnote[i]; j++) {
                //img.setRGB(horizontal - 15, j + height * i, 1543210);
            }
        }
        boolean rowHeight[] = findBigRows(horizontal);
        boolean fin[] = consolidateArrays(firstTest, thirdTest);
        fin = consolidateArrays(fin, rowHeight);
        fin = consolidateArrays(fin, footnote);
        //zabrani tomu, aby zacatek byl true - minimalne 100px je dycky okraj, pak mi to haze kraviny
        //prvnich pet pryc

        for (int i = 0; i < 5; i++) {
            fin[i] = false;
        }
        fin = findLongest(fin);
        //System.out.println(Arrays.toString(fin));
        for (int i = 0; i < fin.length; i++) {
            for (int j = 0; j < height && fin[i]; j++) {
                //img.setRGB(horizontal + 5, j + height * i, 1543210);
                result.add(j + height * i);
            }
        }
        //pridani parametru, kde mohou byt footnote - poznamky pod carou

        return result;
    }

    /**
     * Spocita pocet cernejch castic ve ctverci o sirce width kolem
     * horizontalniho stredu - podle toho
     *
     * @param offset - posunuti od stredu (10,20,30)
     * @param verticalLevel - vyska
     * @param horizontal - souradnice stredu
     * @param width - pouyita sirka bloku, ktery vzhodnocuju
     * @return
     */
    private static boolean firstTest(int verticalLevel, int horizontal, int width, int height) {
        List<Integer> result = new ArrayList();
        int count = 0;
        int bestCount = 1000000;
        int finalOffset = 0;
        for (int offset = 0; offset < 5 && horizontal - offset * 10 > 0; offset++) {
            for (int i = verticalLevel; i < verticalLevel + height; i++) {
                for (int j = horizontal - offset * 10; j < horizontal + width - offset * 10; j++) {
                    if (i >= pixels.length || j >= pixels[i].length) {
                        continue;
                    }
                    if (pixels[i][j] != -1) {
                        count++;
                    }
                }
            }
            if (count < bestCount) {
                bestCount = count;
                finalOffset = offset * 10;
            }
            count = 0;
        }
        if (bestCount < 25) {
            return true;
        } else {
            return false;
        }

    }

    /**
     *
     * @param firstTest
     * @return
     */
    private static boolean[] beforeAfter(boolean[] firstTest, boolean test, int index, int verticalLevel, int height) {
        if (!test && index > 0) {
            firstTest[index - 1] = false;
        }
        if (!test && verticalLevel + 2 * height < pixels.length) {
            firstTest[index + 1] = false;
        }
//        if (!test && index > 1) {
//            firstTest[index - 2] = false;
//        }
//        if (!test && verticalLevel + 3 * height < pixels.length) {
//            firstTest[index + 2] = false;
//        }
        return firstTest;
    }

    private static boolean[] consolidateArrays(boolean[] firstTest, boolean[] secondTest) {

        if (firstTest.length != secondTest.length) {
            System.out.println("Nerovnaji se velikosti second test a firsttest");
            System.exit(0);
        }
        //dá oboje dohromady
        boolean result[] = new boolean[secondTest.length];

        for (int i = 0; i < result.length; i++) {
            if (secondTest[i] & firstTest[i]) {
                result[i] = true;
                //System.out.println("ahoj");
            } else {
                result[i] = false;
            }
        }

        return result;

    }

    private static int getBlackCount() {
        int blackCount = 0;
        //System.out.println("pixels.length : "+ pixels.length);
        List<Integer> counts = new ArrayList();
        for (int column = (int) (pixels[0].length * 0.45);
                column < (int) (pixels[0].length * 0.55); column++) {
            blackCount = 0;
            for (int i = 0; i < pixels.length; i++) {
                if (pixels[i][column] != -1) {
                    //System.out.println(column+ " - " + pixels[i][column]);
                    blackCount++;
                }
            }
            counts.add(blackCount);
        }

        Collections.sort(counts);

        //System.out.println("Column :" + column + "BlackCount " + blackCount);
        if (counts.size() < 1) {
            counts.add(0);
        }
        int result = (int) (counts.get(0) * 1.3 - counts.get(0) > 20 ? counts.get(0) * 1.3 : counts.get(0) + 20);

        return result;
    }

    private static boolean[] firstLevelTest(int verticalLevel, int horizontal, int width, int height) {
        boolean firstTest[] = new boolean[pixels.length / height];
        int index = 0;
        while (verticalLevel + height < pixels.length) {

            boolean test = (firstTest(verticalLevel, horizontal, width, height));
            firstTest[index] = test;
            firstTest = beforeAfter(firstTest, test, index, verticalLevel, width);

            verticalLevel += height;
            index++;
        }
        return firstTest;
    }

    private static boolean[] findLongest(boolean[] fin) {
        int beginIndex = 0;
        int endIndex = 0;
        int length = 0;

        for (int i = 0; i < fin.length; i++) {
            if (fin[i]) {
                for (int j = i; true; j++) {
                    if (j >= fin.length || !fin[j]) {
                        if (length < j - i) {
                            beginIndex = i;
                            endIndex = j;
                            length = j - i;
                        }
                        break;
                    }
                }
            }
        }

        boolean[] result = new boolean[fin.length];
        for (int i = beginIndex; i < endIndex; i++) {
            result[i] = true;
        }
        return result;
    }

    private static boolean[] thirdTest(int horizontal, int height) {
        boolean[] result = new boolean[pixels.length / height];
        for (int i = 0; i < result.length; i++) {
            int count = 0;
            for (int j = 0; j < height * 5 && j + i * height < pixels.length; j++) {
                for (int k = 0; k < pixels[0].length; k++) {
                    if (pixels[j + i * height][k] != -1) {
                        count++;
                    }
                }
            }
            if (count > 10) {
                result[i] = true;
            } else {
                //System.out.println("");
                result[i] = false;
            }

        }
        return result;
    }

    /**
     *
     */
    private static List<Integer> shapeVertical(List<Integer> vertical) {
        int first = 0;
        int last = 0;
        if (vertical.size()>0) {
            first = vertical.get(0);
            last = vertical.get(vertical.size() - 1);
        }else{
            
        }
        

        int index = 0;
        //System.out.println(vertical);
        while (true) {
            boolean check1 = true;
            boolean check2 = true;
            for (int i = 0; i < pixels[first + index].length; i++) {
                if (pixels[first + index][i] != -1) {
                    check1 = false;
                    break;
                }
            }
            for (int i = 0; i < pixels[first - index].length; i++) {
                if (pixels[first - index][i] != -1) {
                    check2 = false;
                    break;
                }
            }
            if (check1) {
                first = first + index;
                //System.out.println(index);
                break;
            }
            if (check2) {
                //System.out.println(index);
                first = first - index;
                break;
            }
            index++;
        }
        index = 0;
        while (true) {
            boolean check1 = true;
            boolean check2 = true;
            for (int i = 0; i < pixels[last + index].length; i++) {
                if (pixels[last + index][i] != -1) {
                    check1 = false;
                    break;
                }
            }
            for (int i = 0; i < pixels[last - index].length; i++) {
                if (pixels[last - index][i] != -1) {
                    check2 = false;
                    break;
                }
            }
            if (check1) {
                last = last + index;
                break;
            }
            if (check2) {
                last = last - index;
                break;
            }
            index++;
        }
        List<Integer> result = new ArrayList();
        for (int i = first; i <= last; i++) {
            result.add(i);
        }
        //System.out.println(result.get(0) + "   " + result.get(result.size() - 1));
        return result;
    }

    private static boolean[] findFootNote() {

        List<Integer> lines = findStraightLines(500);
        lines = selectBottomLines(lines);
        //transform lines<Integer> na lines ktery je boolean[]
        int height = 20;
        boolean result[] = new boolean[pixels.length / height];
        Arrays.fill(result, Boolean.TRUE);
        for (int i = 0; i < lines.size(); i++) {
            int index = lines.get(i) / height;
            result[index] = false;
        }
        //System.out.println(Arrays.toString(result));

        return result;
    }

    /**
     * Najde rovné čáry, které oddělují řádky apod. Vrátí jejich indexy
     *
     * @return
     */
    private static List<Integer> findStraightLines(int limit) {
        List<Integer> lines = new ArrayList();
        for (int i = 0; i < pixels.length; i++) {
            boolean check = true;
            for (int j = 0; j < limit && j < pixels[i].length; j++) {

                if (pixels[i][j] != -1) {
                    check = false;
                    break;
                }
            }
            if (check) {
                lines.add(i);
            }
        }
        List<Integer> result = new ArrayList();
        //Pokud je nekde jen max dvoupixelová díra, udela z ni caru
// 

        //Oddela jednopixelovy cary - nejmin tripixelova
        for (int i = 2; i < lines.size() - 2; i++) {
            if ((lines.get(i) - 1 == lines.get(i - 1) && lines.get(i) + 1 == lines.get(i + 1))
                    || (lines.get(i) - 2 == lines.get(i - 2) && (lines.get(i) - 1 == lines.get(i - 1)))
                    || (lines.get(i) + 2 == lines.get(i + 2) && lines.get(i) + 1 == lines.get(i + 1))) {
                boolean check = true;
                for (int j = 0; j < result.size(); j++) {

                    if (result.get(j) == lines.get(i)) {
                        check = false;
                        break;
                    }

                }
                if (check) {
                    result.add(lines.get(i));
                }
            }
        }

        return lines;

    }

    /**
     * Najde jen ty sprany cary - tzn cca od 2-10px vysoky, primerene siroky a s
     * odpovidajici hustotou cernych pixelu
     *
     * @param lines
     * @return
     */
    private static List<Integer> selectBottomLines(List<Integer> lines) {
        List<Integer> result = new ArrayList();
        for (int i = 0; i < lines.size(); i++) {
            if (i > 10) {
                int rozdil = lines.get(i) - lines.get(i - 1);
                //System.out.println("-------------------");
                if (rozdil > 2 && rozdil < 13 && isFootnote(lines.get(i - 1), lines.get(i))) {
                    result.add(lines.get(i));
                    result.add(lines.get(i - 1));
                }
            }
        }

        return result;
    }

    /**
     * vyhodnoti, jestli ma optimalni delku a hustotu cary
     *
     * @param startIndex
     * @param endIndex
     * @return
     */
    private static boolean isFootnote(Integer startIndex, Integer endIndex) {
        boolean length = false;
        int[][] array = new int[endIndex - startIndex][500];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = pixels[startIndex + i][j];
            }
        }
        //zjisti yadcatek radku, aby se mohl vypocitat jeho delka
        int begin = -1;
        for (int i = 0; i < array[0].length; i++) {
            boolean check = false;
            for (int j = 0; j < array.length; j++) {
                if (array[j][i] != -1) {
                    begin = i - 1 < 0 ? i : i - 1;

                    check = true;
                    break;
                }
            }
            if (check) {
                break;
            }
        }
        //zjisti konec toho radku
        int end = -1;
        for (int i = array[0].length - 1; i >= 0; i--) {
            boolean check = false;
            for (int j = array.length - 1; j >= 0; j--) {
                if (array[j][i] != -1) {
                    end = i + 1;
                    check = true;
                    break;
                }
            }
            if (check) {
                break;
            }
        }
        //zjisteni delky / pokud je dobra - da length true - jinak vrsti false
        if (end - begin > 100) {
            length = true;
        } else {
            return false;
        }
        //System.out.println("begin "+ begin + "end " + end);
        //pocet cernejch tecek co tam je - je jihc dost je to cara
        int densityCount = 0;
        for (int i = startIndex; i < endIndex; i++) {
            for (int j = begin; j < end; j++) {
                if (pixels[i][j] != -1) {
                    densityCount++;
                }
            }
        }

        double totalCount = (endIndex - startIndex) * (end - begin);
        double density = (densityCount / totalCount);
        if (densityCount > 100) {
            return true;
        }

        boolean result = false;
        return result;
    }

    private static int getHorizontal(List<Integer> middle) {
        int result = 10000;
        if (middle.size() == 0) {
            //porad v okraji, ale yaroven neco najde (vetsinou se jedna o prilohy)
            return pixels[0].length/2;
        }
        result = (middle.get(0) + middle.get(middle.size() - 1)) / 2;
        if (middle.get(0) + middle.get(middle.size() - 1) > 40) {
            result = (middle.get(0) + middle.get(middle.size() / 3 * 2)) / 2;
        }
        return result;
    }

    private static boolean[] findBigRows(int horizontal) {
        boolean result[] = new boolean[pixels.length / height];
        Arrays.fill(result, Boolean.TRUE);
        int tolerance = 40;
        List<Integer> lines = findStraightLinesWithTolerance(horizontal, tolerance);
        List<Integer> bigLines = new ArrayList();
        for (int i = 1; i < lines.size(); i++) {
            if (lines.get(i) - lines.get(i - 1) > 50) {
                int index = lines.get(i) / height;
                result[index] = false;
            }
        }

        //System.out.println("bigLines "+bigLines);
        return result;
    }

    /**
     * Zjisti, kde jsou radky - dava malou toleranci tam, kde muye byt maly
     * bordel (tecky vznikle skanovanim) jaka je tolerance, tolik cenrejch tecek
     * jeste v radku muze bejt
     *
     * @param horizontal
     * @param tolerance
     * @return
     */
    private static List<Integer> findStraightLinesWithTolerance(int horizontal, int tolerance) {
        List<Integer> result = new ArrayList();
        for (int i = 0; i < pixels.length; i++) {
            int count = 0;
            for (int j = 0; j < horizontal && j < pixels[i].length; j++) {
                if (pixels[i][j] != -1) {
                    count++;
                }
            }
            if (count < tolerance) {
                result.add(i);
            }
        }
        return result;
    }

}
