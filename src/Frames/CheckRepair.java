/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Utils.Util;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import pkg1990_plus.checkSeparate.Panel;

/**
 *
 * @author Martin
 */
//Yobrayuje HashMapy s obshaem jeden obrazek a dva texty
public class CheckRepair extends JFrame implements KeyListener {

    List<HashMap> parts;

    Panel p;

    JTextPane original, repaired;
    JButton btn;
    GridLayout main, bottom;

    int index;

    public CheckRepair() {
        parts = new ArrayList();
        declarations();
        buildFrame();
        this.setVisible(true);

    }

    public CheckRepair(List<HashMap> parts) {
        this.parts = parts;

        declarations();
        buildFrame();
        this.setVisible(true);
    }

    private void declarations() {
        index = 0;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 900);
        this.setBackground(Color.white);
        p = new Panel();
        btn = new JButton("handler");
        btn.addKeyListener(this);
        original = new JTextPane();
        repaired = new JTextPane();
        original.setBackground(Color.white);
        repaired.setBackground(Color.white);

        bottom = new GridLayout(0, 1);
        bottom.setHgap(10);
        this.setLayout(null);
        p.setBackground(Color.red);

        StyledDocument doc = original.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        doc = repaired.getStyledDocument();
        center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

    }

    private void buildFrame() {
        Rectangle panelPosition = new Rectangle(0, 0, this.getWidth(), this.getHeight() / 10 * 5);
        Rectangle originalPosition = new Rectangle(0, this.getHeight() / 10 * 5, (int) (this.getWidth() / 1.9), this.getHeight() / 10 * 3);
        Rectangle repairedPosition = new Rectangle((int) (this.getWidth() / 1.9), this.getHeight() / 10 * 5, (int) (this.getWidth() / 1.9), this.getHeight() / 10 * 3);

        Rectangle buttonPosition = new Rectangle(this.getWidth() / 10 * 4, this.getHeight() / 10 * 8, 100, 40);
        p.setBounds(panelPosition);
        original.setBounds(originalPosition);
        repaired.setBounds(repairedPosition);
        btn.setBounds(buttonPosition);
        original.setText("original");
        repaired.setText("repaired");
        this.getContentPane().add(btn);
        this.getContentPane().add(p);
        this.getContentPane().add(original);
        this.getContentPane().add(repaired);

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("HEJ");
            if (index + 1 != parts.size()) {
                moveRight();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            System.out.println("HEJ");
            if (index > 0) {
                moveLeft();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void moveRight() {
        index++;
        BufferedImage img = Util.readImage((String) parts.get(index).get("image"));
        String rep = (String) parts.get(index).get("repaired");
        System.out.println("");
        String orig = Util.readTXT((String) parts.get(index).get("original"));

        img = Util.scaleToWidth(img, BufferedImage.TYPE_3BYTE_BGR, p.getWidth()-20);

        p.setPic(img);
        repaired.setText(rep);
        original.setText(orig);
        System.out.println("right");

    }

    private void moveLeft() {
        index--;
        BufferedImage img = Util.readImage((String) parts.get(index).get("image"));
        String rep = (String) parts.get(index).get("repaired");
        System.out.println("");
        String orig = Util.readTXT((String) parts.get(index).get("original")).replaceAll("﻿________________\n", "").replace("﻿________________","");
        img = Util.scaleToWidth(img, BufferedImage.TYPE_3BYTE_BGR, p.getWidth()-20);

        p.setPic(img);
        repaired.setText(rep);
        original.setText(orig);
        System.out.println("left");
    }

}
