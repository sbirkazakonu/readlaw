/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Repairing.Law;
import Utils.Util;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import pkg1990_plus.checkSeparate.Panel;

/**
 *
 * @author Martin
 */
public class Repair extends JFrame {

    int FRAME_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
    int FRAME_HEIGHT = 800;//(int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();

    Panel p;
    public JTextPane finalText, revisionText;

    Container left, right, leftTop;
    GridLayout leftLayout, rightLayout, top;

    JButton btn;

    List<Law> laws;

    int index;

    public Repair() {
        System.out.println("dsfds");
        createFrame();
        System.out.println("sdsd");
    }

    public Repair(String folderPath) {

        createFrame();

        getLaws(folderPath);

        addListeners();
    }

    private void createFrame() {

        declarations();
        settings();
        layouts();
        this.setVisible(true);
    }

    private void declarations() {
        index = 0;
        laws = new ArrayList();
        left = new Container();
        right = new Container();
        leftTop = new Container();

        leftLayout = new GridLayout(0, 1);
        rightLayout = new GridLayout(0, 1);
        top = new GridLayout(1, 0);

        finalText = new JTextPane();
        revisionText = new JTextPane();

        p = new Panel();

        btn = new JButton("Test");
        leftTop.setLayout(top);
        left.setLayout(leftLayout);
        right.setLayout(rightLayout);

    }

    private void settings() {
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        left.setBounds(0, FRAME_HEIGHT / 10 * 3, FRAME_WIDTH / 10 * 9 - 20, FRAME_HEIGHT / 10 * 7 - 30);
        right.setBounds(FRAME_WIDTH / 10 * 9, 0, FRAME_WIDTH / 10, FRAME_HEIGHT / 10);
        leftTop.setBounds(0, 0, FRAME_WIDTH / 10 * 9 - 20, FRAME_HEIGHT / 10 * 3 - 10);
        leftLayout.setHgap(10);
        rightLayout.setVgap(10);

        p.setBackground(new Color(150, 150, 150));
        Font f = new Font(Font.SANS_SERIF, 18, 20);
        finalText.setFont(f);

    }

    private void layouts() {
        JScrollPane scrollFrame = new JScrollPane(revisionText);
        scrollFrame.setViewportView(revisionText);
        JScrollPane scrollText = new JScrollPane(finalText);
        scrollText.setViewportView(finalText);
        this.add(leftTop);
        this.add(left);
        this.add(right);
        leftTop.add(p);
        left.add(scrollText);
        right.add(btn);

    }

    private void addListeners() {
        btn.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    if (index + 1 < laws.size()) {
                        index++;
                        finalText.setStyledDocument(laws.get(index).repair.getStyledDocument());

                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    if (index > 0) {
                        index--;
                        finalText.setStyledDocument(laws.get(index).repair.getStyledDocument());

                    }
                }
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    laws.get(index).saveLaw(finalText.getText());
                    if (index + 1 < laws.size()) {
                        index++;
                        finalText.setStyledDocument(laws.get(index).repair.getStyledDocument());

                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        finalText.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // Přidá nové rpavidlo
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_B) {
                    e.consume();
                    RepairRule r = new RepairRule(finalText.getSelectedText(), finalText, RepairRule.PATH_RULES);

                }
// zamenu pismenek prida                
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_G) {
                    e.consume();
                    RepairRule r = new RepairRule(finalText.getSelectedText(), finalText, RepairRule.PATH_CHARS);
                }
                // zamenu pismenek prida                
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    //Util.saveTXT(laws.get(index).txtPath, finalText.getText());
                    //Util.saveTXT(laws.get(index).txtPath, finalText.getText());
                    laws.get(index).saveLaw(finalText.getText());
                    if (index + 1 < laws.size()) {
                        index++;
                        finalText.setStyledDocument(laws.get(index).repair.getStyledDocument());
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

        });
    }

    private void getLaws(String pathInput) {
        final String path = pathInput;
        Runnable r = () -> {
            List<String> paths = Util.readAllFilePathsSubfolders(path, new ArrayList());
            List<Law> result = new ArrayList();
            for (String s : paths) {
                
                if (s.contains("zakon_") && !s.contains("/final/")) {
                    File f = new File(s.replace("zakon_","final/zakon_"));
                    if (!f.exists()) {
                        System.out.println("path: "+ s);
                        laws.add(new Law(Util.readTXT(s), s));
                        continue;
                    }
                    String finalPath = s.replaceAll("/zakon_", "/final/zakon_");
                    if (("" + paths).contains(finalPath)) {
                        System.out.println("TRUE: " + s);
                        continue;
                    }
                    laws.add(new Law(Util.readTXT(s), s));
                    //System.out.println("LAWS: " + laws);
                }
            }
        };
        Thread t = new Thread(r);
        t.start();

    }

}
