/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;


import Utils.Util;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;

/**
 *
 * @author Martin
 */
public class RepairRule extends JFrame implements KeyListener {

    final static String PATH_RULES = "rules.txt";
    final static String PATH_CHARS = "char_pairs.txt";

    JTextPane origin, repaired, inputFinish;
    JButton finished;

    String path;

    public RepairRule() {
        declarations();

    }

    public RepairRule(String s) {
        declarations();
        addListeners();
        origin.setText(s);
        repaired.setText(s);

    }

    public RepairRule(String s, JTextPane p) {
        declarations();
        addListeners();
        origin.setText(s);
        repaired.setText(s);
        inputFinish = p;
    }

    public RepairRule(String s, JTextPane p, String path) {
        declarations();
        addListeners();
        origin.setText(s);
        repaired.setText(s);
        inputFinish = p;
        this.path = path;
    }

    private void declarations() {
        this.setSize(300, 200);
        this.setMinimumSize(new Dimension(300, 100));

        Container top = new Container();
        GridLayout main = new GridLayout(0, 1);
        main.setVgap(10);
        main.setHgap(10);
        this.setLayout(main);
        GridLayout topLayout = new GridLayout(1, 0);
        topLayout.setHgap(10);
        topLayout.setVgap(10);
        top.setLayout(topLayout);

        origin = new JTextPane();
        repaired = new JTextPane();
        finished = new JButton("Hotovo");
        JPanel containerPanel = new JPanel();
        containerPanel.setBorder(BorderFactory.createEmptyBorder(2, 25, 2, 25));
        containerPanel.setLayout(new BorderLayout());
        //panel to test

        containerPanel.add(finished, BorderLayout.CENTER);

        top.add(origin);
        top.add(repaired);

        this.getContentPane().add(top);
        this.getContentPane().add(containerPanel);
        this.pack();
        this.setVisible(true);
        repaired.requestFocus();

    }

    private void addListeners() {
        repaired.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
            addRule(origin.getText(), repaired.getText());
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void addRule(String orig, String rep) {

        File f = new File(path);
        String allRules = f.exists() ? Util.readTXT(path) : "";
        String rules = allRules + orig + " -> " + rep;
        Util.saveTXT(path, rules);
        //System.out.println("rules: "+ rules );
        //System.out.println("path: "+ path );
        this.dispose();

    }

}
