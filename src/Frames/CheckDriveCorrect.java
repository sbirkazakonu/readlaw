/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Utils.Util;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import pkg1990_plus.checkSeparate.Panel;

/**
 *
 * @author Martin
 */
public class CheckDriveCorrect extends JFrame implements KeyListener {

    Panel p;

    JTextPane drive, check;

    Container top, bottom, right;

    GridLayout topL, bottomL, rightL;

    JButton btn;

    List<HashMap> parts;

    int index;

    boolean paths;

    public CheckDriveCorrect() {
        creatFrame();
        paths = true;

        getParts();
        //System.out.println("LOADING FINISHED");
    }

    public CheckDriveCorrect(List<HashMap> inputParts) {
        creatFrame();
        if (((String)inputParts.get(0).get("drive")).startsWith("/Users/")) {
            paths = true;
        }else{
            paths = false;
        }
        parts = inputParts;
    }

    public CheckDriveCorrect(List<HashMap> inputParts, boolean paths) {
        creatFrame();
        parts = inputParts;
        this.paths = paths;
    }

    private void creatFrame() {
        declarations();
        settings();
        layouts();
        this.setVisible(true);
    }

    private void declarations() {
        p = new Panel();

        top = new Container();
        bottom = new Container();
        right = new Container();

        drive = new JTextPane();
        check = new JTextPane();
        btn = new JButton("Handler");

        topL = new GridLayout(1, 0);
        bottomL = new GridLayout(1, 0);
        rightL = new GridLayout(1, 0);

        parts = new ArrayList();

        index = 0;

    }

    private void settings() {
        this.setSize(1000, 800);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        p.setBackground(new Color(100, 100, 100));

        top.setLayout(topL);
        bottom.setLayout(bottomL);
        right.setLayout(rightL);
        btn.addKeyListener(this);

        bottomL.setHgap(10);

    }

    private void layouts() {
        top.setBounds(0, 0, this.getWidth() / 10 * 9 - 20, this.getHeight() / 2 - 10);
        bottom.setBounds(0, this.getHeight() / 2 + 10, this.getWidth() / 10 * 9 - 20, this.getHeight() / 2);
        right.setBounds(this.getWidth() / 10 * 9, 0, this.getWidth() / 10, this.getHeight());
        this.add(top);
        this.add(bottom);
        this.add(right);

        top.add(p);
        bottom.add(drive);
        bottom.add(check);
        right.add(btn);

    }

    private void getParts() {
        Runnable r = () -> {
            String folderPath = "/Users/Martin/Documents/zakony/PDF/1990";
            List<String> allPaths = Util.readAllFilePathsSubfolders(folderPath, new ArrayList());
            for (String path : allPaths) {

                if (path.contains("/page/")
                        && path.contains(".txt")
                        && !path.contains("/check/")) {
                    //System.out.println("path: " + path);
                    //Users/Martin/Documents/zakony/PDF/1990/49_1990/page/check
                    String check = path.replace("/page/", "/page/check/");
                    File f = new File(check);
                    if (f.exists()) {
                        String orig = Util.readTXT(path);
                        String second = Util.readTXT(check);
                        while ((orig.endsWith("\\s") || orig.endsWith("\n"))
                                && orig.length() > 0) {
                            //System.out.println("asdasda");
                            orig = orig.substring(0, orig.length() - 1);
                        }
                        while ((second.endsWith("\\s") || second.endsWith("\n"))
                                && second.length() > 0) {
                            //System.out.println("asdasda");
                            second = second.substring(0, second.length() - 1);
                        }
                        if (Util.readTXT("/Users/Martin/Documents/zakony/PDF/repaired.txt").contains(path.replaceAll(".txt", ""))) {
                            //System.out.println("continued");
                            continue;
                        }
                        if (!orig.equals(second)) {
                            if (orig.length() > 5
                                    && second.length() > 5
                                    && orig.substring(0, orig.length() - 5).equals(second.substring(0, second.length() - 5))) {
                                System.out.println("OKKK");
                            }
                            System.out.println("path: " + path);
                            HashMap h = new HashMap();
                            h.put("image", path.replace(".txt", ""));
                            h.put("drive", path);
                            h.put("check", check);
                            parts.add(h);
                            System.out.println("added");
                        }
                    }
                }

            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_RIGHT) {
            String correct = Util.readTXT((String) parts.get(index).get("check"));
            drive.setText(correct);
        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_LEFT) {
            String correct = Util.readTXT((String) parts.get(index).get("drive"));
            check.setText(correct);
        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {

            Util.saveTXT((String) parts.get(index).get("check"), check.getText());
            Util.saveTXT((String) parts.get(index).get("drive"), drive.getText());

            moveRight();
            System.out.println("PROGRESS: " + index + " / " + parts.size());
        }
        if (!e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_RIGHT) {
            moveRight();
            System.out.println("check: " + check.getText().length() + " " + drive.getText().length());;

        }
        if (!e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_LEFT) {

            moveLeft();
            System.out.println("check: " + check.getText().length() + " " + drive.getText().length());;

        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void moveLeft() {
        if (index > 0) {
            index--;
            HashMap cur = parts.get(index);
            BufferedImage img = Util.readImage((String) cur.get("image"));
            img = Util.scaleToWidth(img, BufferedImage.TYPE_3BYTE_BGR, p.getWidth() - 20);
            p.setPic(img);
            drive.setText(Util.readTXT((String) cur.get("drive")));
            check.setText(Util.readTXT((String) cur.get("check")));

        }
    }

    private void moveRight() {

        if (index + 1 != parts.size()) {
            index++;
            HashMap cur = parts.get(index);
            BufferedImage img = Util.readImage((String) cur.get("image"));
            img = Util.scaleToWidth(img, BufferedImage.TYPE_3BYTE_BGR, p.getWidth() - 20);
            p.setPic(img);
            
            if (paths) {
                drive.setText(Util.readTXT((String) cur.get("drive")));
                check.setText(Util.readTXT((String) cur.get("check")).replaceAll("\\\\r\\\\n", "\n"));
            } else {
                drive.setText((String) cur.get("drive"));
                check.setText(((String) cur.get("check")).replaceAll("\\\\r\\\\n", "\n"));
            }
            System.out.println("PATH: " + cur.get("drive"));
        }
    }

}
