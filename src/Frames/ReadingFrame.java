/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Objects.Rule;
import Objects.Panel;
import Objects.Part;
import Repairing.Dictionary;
import Repairing.LawUtil;
import Utils.Util;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Martin
 */
public class ReadingFrame extends JFrame {

    final String PATH_DICT = "slovnik.txt";

    Panel p;
    JButton btnSave, btnNew, btnTest;
    JTextPane text, finish, tree;
    public JTextPane lawName, platnost;

    //CompleteLaw complete;
    List<Part> parts;
    int index, maxIndex, numberLaw, numberYear;
    String fullText, path;
    String words[];
    List<Rule> rules;

    public ReadingFrame() {
        //
        preDeclarations();
        declarations();
        addKeyListeners();
        addActionListeners();
        index = 0;
        maxIndex = 0;
    }

    public ReadingFrame(List<Part> parts) {

        this.parts = parts;
        preDeclarations();
        declarations();
        addKeyListeners();
        addActionListeners();

        index = 0;
        maxIndex = 0;
        startReadThread();
    }

    private void preDeclarations() {
        //complete = new CompleteLaw();
        Dictionary.readWords();
        words = Dictionary.words;
        //System.out.println(words.length);
        this.setLayout(null);
        this.setSize(1400, 800);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        p = new Panel();

        text = new JTextPane();
        tree = new JTextPane();
        finish = new JTextPane();
        lawName = new JTextPane();
        platnost = new JTextPane();

        btnSave = new JButton("Save");
        btnNew = new JButton("Add new");
        btnTest = new JButton("Test");

        p.setBackground(new Color(100, 100, 100));
    }

    private void declarations() {

        rules = Rule.readRules(Util.PATH_RULES);
        fullText = "";

        GridLayout topGrid = new GridLayout(1, 1);
        topGrid.setVgap(5);
        topGrid.setHgap(5);
        GridLayout middleGrid = new GridLayout(0, 1);
        middleGrid.setVgap(0);
        middleGrid.setHgap(10);
        GridLayout bottomGrid = new GridLayout(1, 0);
        bottomGrid.setVgap(30);

        Container top = new Container();
        top.setLayout(topGrid);

        Container middle = new Container();
        middle.setLayout(middleGrid);

        Container bottom = new Container();
        bottom.setLayout(bottomGrid);

        Font f = new Font(Font.SANS_SERIF, 10, 16);

        text.setFont(f);
        finish.setFont(f);
        //text.setContentType( "text/html" );
        //
        JScrollPane scrollFrame = new JScrollPane(p);
        scrollFrame.setViewportView(p);

        JScrollPane sp = new JScrollPane(finish);
        //JScrollPane sp2 = new JScrollPane(text);
        top.add(btnNew);
        top.add(lawName);
        top.add(platnost);
        top.add(tree);
        middle.add(scrollFrame);
        middle.add(sp);
        //middle.add(sp2);
        bottom.add(btnSave);
        bottom.add(btnTest);

        this.getContentPane().add(top);
        this.getContentPane().add(middle);
        this.getContentPane().add(bottom);

        top.setBounds(100, 0, this.getWidth() - 200, (int) (0.08 * this.getHeight()));
        middle.setBounds(0, (int) (0.12 * this.getHeight()), this.getWidth(), (int) (0.78 * this.getHeight()));
        bottom.setBounds(100, (int) (0.9 * this.getHeight()), this.getWidth() - 200, (int) (0.06 * this.getHeight()));

        this.setVisible(true);

        btnSave.requestFocus();
    }

    public void addKeyListeners() {

        Action left = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveLeft();
            }

        };
        btnSave.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "left");
        btnSave.getActionMap().put("left", left);

        Action right = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveRight();
            }

        };
        btnSave.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "right");
        btnSave.getActionMap().put("right", right);

        btnSave.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                //Uloží opravený text
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    //System.out.println("asd");
                    parts.get(index).getNextTitle();
                    Util.saveTXT(parts.get(index).txtPath, finish.getText());
                    parts.get(index).repairText = finish.getText();
                    finish.requestFocus();
                    if (index + 1 == parts.size()) {
                        dispose();
                    }
                    saveRepairedPath(parts.get(index).pngPath);
                    moveRight();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        text.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    System.out.println("asd");
                    Util.saveTXT(parts.get(index).tagPath, text.getText());
                    finish.requestFocus();
                    moveRight();
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        finish.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    //System.out.println("asd");
                    parts.get(index).getNextTitle();
                    Util.saveTXT(parts.get(index).txtPath, finish.getText());
                    parts.get(index).repairText = finish.getText();
                    finish.requestFocus();
                    moveRight();
                }
//Uloží opravený text
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    //System.out.println("asd");
                    parts.get(index).getNextTitle();
                    Util.saveTXT(parts.get(index).txtPath, finish.getText());
                    parts.get(index).repairText = finish.getText();
                    finish.requestFocus();
                    if (index + 1 == parts.size()) {
                        
                        dispose();
                    }
                    saveRepairedPath(parts.get(index).pngPath);

                    moveRight();

                }
// Přidá nové rpavidlo
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_B) {
                    e.consume();
                    RepairRule r = new RepairRule(finish.getSelectedText(), finish, RepairRule.PATH_RULES);

                }
// zamenu pismenek prida                
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_G) {
                    e.consume();
                    RepairRule r = new RepairRule(finish.getSelectedText(), finish, RepairRule.PATH_CHARS);
                }

// Přidání slovíčka do slovníku
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_D) {
                    e.consume();
                    Util.saveTXT(PATH_DICT, Util.readTXT(PATH_DICT) + finish.getSelectedText());
                    words = Arrays.copyOf(words, words.length + 1);
                    words[words.length - 1] = finish.getSelectedText();
                }
// Nová aplikace pravidel - refresh                
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_R) {
                    finish = Rule.applyRules(finish, rules);
                    Dictionary.setColoredText(finish.getText(), finish);
                }
                if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_F) {
                    finish.replaceSelection("<foot>" + finish.getSelectedText() + "</foot>");
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

        });

    }

    private void addActionListeners() {

        btnNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }

        });
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String complete = "";
                for (Part p : parts) {
                    complete = complete + "\n" + p.repairText;
                }
                Util.saveTXT(path + "/complete.txt", complete.replaceAll("\n\n\n", "\n\n").replaceAll("\n\n\n", "\n\n").replaceAll("\n\n\n", "\n\n"));
            }
        });

    }

    private void moveLeft() {

        if (index > 0) {
            parts.get(index).text = text.getText();
            index--;
            BufferedImage im = Util.scaleToWidth(parts.get(index).img,
                    BufferedImage.TYPE_3BYTE_BGR, p.getWidth() - 140);
            p.setPic(im);
            p.setPreferredSize(new Dimension(im.getWidth() + 40, im.getHeight() + 100));
            p.revalidate();
            text.setText(parts.get(index).text);
            if (parts.get(index).ready) {
                finish.setStyledDocument(parts.get(index).repaired.getStyledDocument());
            }
        }
        btnSave.requestFocus();
    }

    private void moveRight() {

        if (parts.size() > 1 + index) {
            parts.get(index).text = text.getText();
            index++;
            BufferedImage im = Util.scaleToWidth(parts.get(index).img,
                    BufferedImage.TYPE_3BYTE_BGR, p.getWidth() - 140);
            p.setPic(im);
            p.setPreferredSize(new Dimension(im.getWidth() + 40, im.getHeight() + 100));
            p.revalidate();
            text.setText(parts.get(index).text);
            if (parts.get(index).ready) {
                finish.setStyledDocument(parts.get(index).repaired.getStyledDocument());
            }
            System.out.println("path: " + parts.get(index).pngPath);
            System.out.println("Index: " + index + " Size: " + parts.size());
        }
        btnSave.requestFocus();
    }

    private void beginLaw(String path) {

        //Priprava nové obrazovky
        System.out.println("BEGIN LAW");
        parts = Part.readParts(path);
        lawName.setText(LawUtil.getLawName(path));
        numberLaw = LawUtil.prepareNumber(path);
        numberYear = LawUtil.prepareYear(path);

        text.setText(parts.get(index).text);
        BufferedImage im = Util.scaleToWidth(parts.get(index).img,
                BufferedImage.TYPE_3BYTE_BGR, p.getWidth() - 40);
        p.setPic(im);
        platnost.setText(LawUtil.getPlatnost(parts.get(index).text));
        //complete = new CompleteLaw(lawName.getText(), platnost.getText(), numberLaw, numberYear);
    }

    public JTextPane getLawName() {
        return lawName;
    }

    public JTextPane getPlatnost() {
        return platnost;
    }

    private void startReadThread() {
        Runnable r = new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < parts.size(); i++) {
                    parts.get(i).createRepaired();
                    //System.out.println("parts: " + parts.get(i).txtPath );
                }

            }

        };
        Thread th = new Thread(r);
        th.start();
    }

    private void saveRepairedPath(final String path) {
        Runnable r = () -> {
            String savePath = "/Users/Martin/Documents/zakony/PDF/repaired.txt";
            File f = new File(savePath);
            String old = "";
            if (f.exists()) {
                old = Util.readTXT(savePath);
            }
            if (!old.contains(path)) {
                String finalText = old + "\n" + path;
                Util.saveTXT(savePath, finalText);
            }

        };
        Thread t = new Thread(r);
        t.start();

    }

}
