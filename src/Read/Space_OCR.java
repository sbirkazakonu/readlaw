/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class Space_OCR {

    public static String spaceOCR(String path) {
        String cm[] = {"curl", "--form", "file=@" + path, "--form", "apikey=03d4f75b1688957", "--form", "language=ce", "https://api.ocr.space/Parse/Image"};
        Process p = null;
        //System.out.println("start proc");
        //System.out.println("path" + path);
        try {

            p = Runtime.getRuntime().exec(cm);
        } catch (IOException ex) {
            Logger.getLogger(Space_OCR.class.getName()).log(Level.SEVERE, null, ex);

        }
        //System.out.println("End proc");
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        BufferedReader err = null;
        try {
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + System.getProperty("line.separator"));
                
            }
            while ((line = err.readLine()) != null) {
                //System.out.println("ERROR PROCESS: " + line);
            }

        } catch (IOException ex) {
            Logger.getLogger(Space_OCR.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(Space_OCR.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //System.out.println("sb: " + sb.toString());
        String result = extractText(sb.toString());
        return result;

    }

    private static String extractText(String toString) {
        //System.out.println("toString: "+ toString);
        String result = toString.substring(
                toString.indexOf("\"ParsedText\":\"") + "\"ParsedText\":\"".length(), toString.indexOf("\",\"ErrorMessage\":"));
        return result;
    }

}
