/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Read;

import Objects.Part;
import Repairing.Law;
import Frames.ReadingFrame;
import Frames.Repair;
import Utils.Util;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Martin
 */
public class FinishLaw {

    static String path;
    static List<Law> laws;

    public static void finish(String folderPath) {
        FinishLaw.path = folderPath;
        //regularParts(folderPath);
        completeLaw(folderPath);
        deleteLaws();
        System.exit(0);
        System.out.println("---");
        Repair r = new Repair(folderPath);
    }

    private static void regularParts(String folderPath) {
        List<String> files = Util.readAllFilePathsSubfolders(folderPath, new ArrayList());
        List<Part> parts = getRepairFiles(files);
        ReadingFrame f = new ReadingFrame(parts);
    }

    public static void completeLaw(String folderPath) {
        List<String> paths = Util.readAllFilePathsSubfolders(folderPath, new ArrayList());
        String newLaws = findNewLaws(paths);
        String footers = findFooters(paths);
        String priloha = findPriloha(paths);
        System.out.println("priloha: " + priloha);
        paths = eliminateWrongPaths(paths);
        paths = newSort(paths);

        String complete = "";
        int index = 0;
        String prevPath = paths.get(0);
        String footerText = "";
        for (String s : paths) {
            

            if (!prevPath.substring(0, prevPath.lastIndexOf("/")).equals(s.substring(0, s.lastIndexOf("/")))) {

                index = 0;
            }

            String text = Util.readTXT(s).replace("﻿________________\n\n", "");
            if (newLaws.contains(s.replace(".txt", "")) && complete.length() > 2) {
                String addit = footerText.length() > 5 ? "\nPoznámky pod čarou:\n" : "";
                complete = complete + addit + footerText;
                String savePath = prevPath.substring(0, prevPath.lastIndexOf("/page/") + 1) + "zakon_" + index + ".txt";
                saveNewLaw(savePath, complete);
                index++;
                complete = "";
                footerText = "";
            }
            if (priloha.contains(s.replace(".txt", ""))) {
                System.out.println("priloha: " + s);
                complete = complete + s + "\n";
                continue;
            }

            if (footers.contains(s.replace(".txt", ""))) {
                footerText += text;
                continue;
            }
            //System.out.println("=====" + s + "======");
            if (s.contains("_priloha")) {
                complete = complete + s + "\n";
                continue;
            }
            if (text.contains("Částka") && text.contains("Sbírka zákonů č.") && text.contains("Strana ")) {
                continue;
            }
            //System.out.println(text + " - path: " + s);
            prevPath = s;
            complete = complete + text;
        }
        System.out.println("!!!!");

    }

    public static List<String> newSort(List<String> paths) {
        //List<HashMap> hashList = new ArrayList();
        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                //System.out.println("o1: "+o1);
                String pom1 = o1.substring(o1.lastIndexOf("/PDF/") + "/PDF/".length(), o1.lastIndexOf("/page/"));

                pom1 = pom1.substring(0, pom1.lastIndexOf("/"));

                String pom2 = o2.substring(o2.lastIndexOf("/PDF/") + "/PDF/".length(), o2.lastIndexOf("/page/"));
                pom2 = pom2.substring(0, pom2.lastIndexOf("/"));
                int year1 = Integer.parseInt(pom1);
                int year2 = Integer.parseInt(pom2);
                if (year1 != year2) {
                    return new Integer(year1).compareTo(year2);
                }

                pom1 = o1.substring(0, o1.lastIndexOf("/page/"));
                pom1 = pom1.substring(pom1.lastIndexOf("/") + 1, pom1.lastIndexOf("_"));
                pom2 = o2.substring(0, o2.lastIndexOf("/page/"));;
                pom2 = pom2.substring(pom2.lastIndexOf("/") + 1, pom2.lastIndexOf("_"));
                int nr1 = Integer.parseInt(pom1);
                int nr2 = Integer.parseInt(pom2);
                if (nr1 != nr2) {
                    return new Integer(nr1).compareTo(nr2);
                }
                pom1 = o1.substring(o1.lastIndexOf("/page") + "/page".length(), o1.lastIndexOf("_"));
                pom2 = o2.substring(o2.lastIndexOf("/page") + "/page".length(), o2.lastIndexOf("_"));
                //System.out.println("pom1: "+ pom1);
                //System.out.println("o1: "+o1);
                int page1 = Integer.parseInt(pom1);
                int page2 = Integer.parseInt(pom2);
                if (page1 != page2) {
                    return new Integer(page1).compareTo(page2);
                }
                pom1 = o1.substring(o1.lastIndexOf("_") + "_".length(), o1.lastIndexOf(".png"));
                pom2 = o2.substring(o2.lastIndexOf("_") + "_".length(), o2.lastIndexOf(".png"));

                int row1 = Integer.parseInt(pom1);
                int row2 = Integer.parseInt(pom2);
                return new Integer(row1).compareTo(row2);
            }
        };

        Collections.sort(paths, comp);

        return paths;
    }

    private static List<Part> getParts(List<String> files) {
        List<Part> parts = new ArrayList();
        for (String s : files) {
            if (s.contains(".txt")) {
                if (s.substring(s.lastIndexOf("/")).contains("_")) {
                    Part p = new Part(s.replace(".txt", ""), s);
                    parts.add(p);
                }
            }
        }
        return parts;
    }

    public static List<Part> getRepairFiles(List<String> files) {
        List<Part> result = new ArrayList();
        System.out.println("START");
        List<String> broken = getBrokenFiles(files);
        System.out.println("cont");
        //List<String> doubleLine = getPartsWithDoubleLine();
        List<String> uppercase = getPartsWithUppercaseLetters();
        spaceOCR(broken);
        return result;
    }

    private static List<String> getBrokenFiles(List<String> files) {
        String repaired = Util.readTXT("/Users/Martin/Documents/zakony/PDF/repaired.txt");
        //List<Part> result = new ArrayList();
        List<String> res = new ArrayList();
        String footnotes[] = {"*)", "')", ".)", "\")", "“)", "”)", "ě)", "°)", "^)", "?)"};
        for (String s : files) {
            System.out.println("s: " + s);
            //uz je opraveno -> continue
            if (s.contains("_priloha") || repaired.contains(s) || repaired.contains(s.replace(".txt", ""))) {
                //System.out.println("skipped: " + s);
                continue;
            }

            if (s.contains("repair.txt")
                    || s.contains("footers.txt")) {
                List<String> paths = Arrays.asList(Util.readTXT(s).split("\n"));
                for (String p : paths) {
                    if (p.length() > 5 && !repaired.contains(p)) {
                        res.add(p);
                        System.out.println("====");
                        //System.out.println("FOOT_REP" + Space_OCR.spaceOCR(p));
                        //result.add(new Part(p, p + ".txt"));
                        System.out.println("\n---------------repaired - footers: " + p);
                        continue;
                    }
                }
            }

            if ((s.contains(".txt")
                    && s.contains("/page/")) && !s.contains("/check/")) {
                String text = Util.readTXT(s);
                //obshuje dvoutečku
                if (text.contains("::") || text.contains(" : ")) {
                    res.add(s.replaceAll(".txt", ""));
                    //result.add(new Part(s.replaceAll(".txt", ""), s));
                    System.out.println("\n------------------ :: : " + s);
                    continue;
                }
                for (String ft : footnotes) {
                    if (text.contains(ft) && !repaired.contains(s.replaceAll(".txt", ""))) {
                        res.add(s.replaceAll(".txt", ""));
                        //result.add(new Part(s.replaceAll(".txt", ""), s));
                        BufferedImage im = Util.readImage(s.replaceAll(".txt", ""));
                        String outputPath= s.replaceAll(".txt", "").replace("/zakony/", "/zakony/foots/");
                        outputPath = outputPath.substring(0,outputPath.lastIndexOf("/foots/")+"/foots/".length());
                        outputPath += s.substring(s.lastIndexOf("/")+1);
                        Util.saveImage(im, outputPath.replace(".txt", ""));
                        Util.saveTXT(outputPath, text);
                        
                        System.out.println("\n------------------FOOT: " + s);
                        continue;
                    }
                }
                if (!s.contains("priloha")
                        && s.endsWith(".png")
                        && s.substring(s.lastIndexOf("/")).contains("_")
                        && getMinimumLetterCount(s) > Util.readTXT(s + ".txt").length()) {
                    res.add(s);
                    //result.add(new Part(s, s + ".txt"));
                    System.out.println("\nlowLetterCount: " + s);
                    continue;
                }

            }

        }
        return res;
    }

    private static String findNewLaws(List<String> paths) {
        String result = "";
        for (String path : paths) {
            if (path.contains("newLaw.txt")) {
                result = result + "\n" + Util.readTXT(path);
            }
        }
        return result;
    }

    private static void getLaws(String pathInput) {
        final String path = pathInput;
        Runnable r = new Runnable() {

            @Override
            public void run() {
                List<String> paths = Util.readAllFilePathsSubfolders(path, new ArrayList());
                List<Law> result = new ArrayList();
                for (String s : paths) {
                    if (s.contains("zakon_")) {
                        result.add(new Law(Util.readTXT(s)));
                    }
                }
            }
        };
        Thread t = new Thread(r);
        t.start();

    }

    /**
     * vymaže cesty k souborům, které jsou nepotřebné
     *
     * @param paths celkový soubor s cestami k souborům
     * @return
     */
    private static List<String> eliminateWrongPaths(List<String> paths) {
        List<String> result = new ArrayList();
        for (String path : paths) {
            if (path.contains("priloha") || path.contains("DS_") || path.contains("/check/") || path.contains("_rep.")) {
                continue;
            }
            if (path.contains(".txt") && path.contains("/page/")) {
                result.add(path);
            }

        }
        return result;
    }

    private static String findFooters(List<String> paths) {
        String result = "";
        for (String s : paths) {
            if (s.contains("footers.txt")) {
                result += Util.readTXT(s);
            }
        }
        return result;
    }

    private static String findPriloha(List<String> paths) {
        String result = "";
        for (String s : paths) {
            if (s.contains("/priloha.txt")) {
                result += Util.readTXT(s);
            }
        }
        return result;
    }

    private static void saveNewLaw(String savePath, String complete) {
        File f = new File(savePath);
        if (true) {
            Util.saveTXT(savePath, complete);
        }

    }

    public static void deleteLaws() {
        List<String> paths = Util.readAllFilePathsSubfolders("/Users/Martin/Documents/zakony/PDF/1990", new ArrayList());
        for (String s : paths) {
            if (s.contains("zakon_")) {
                File f = new File(s);
                f.delete();
            }
            if ((s.contains("zakon_") && s.contains("/final/"))) {

            }

        }
    }

    private static int getMinimumLetterCount(String p) {
        BufferedImage img = Util.readImage(p);
        int pixels[][] = Util.getPixelArray(img);
        img = null;
        System.gc();
        int start = 0;
        boolean check = false;
        for (int i = 0; i < pixels[0].length; i++) {
            for (int j = 0; j < pixels.length; j++) {
                if (pixels[j][i] != -1) {
                    start = i;
                    check = true;
                    break;
                }
            }
            if (check) {
                break;
            }
        }
        int end = 0;
        check = false;
        for (int i = pixels[0].length - 1; i >= 0; i--) {
            //System.out.println("---");
            for (int j = 0; j < pixels.length; j++) {
                if (pixels[j][i] != -1) {
                    end = i;
                    check = true;
                    break;
                }
            }
            if (check) {
                break;
            }
        }
        pixels = null;
        System.gc();
        return (end - start) / 25;
    }

    public static List<String> getPartsWithDoubleLine() {
        List<String> paths = Util.readFilesInSubfolderCondition("/Users/Martin/Documents/zakony/PDF/1990/", ".txt", new ArrayList());
        List<String> result = new ArrayList();
        List<String> rtn = new ArrayList();
        for (String s : paths) {
            if (s.contains("/page/") && !s.contains("/check") && !s.contains("priloha")) {
                result.add(s);
                //System.out.println("s: "+ s);
            }
        }
        int c = 0;
        for (String s : result) {
            String text = Util.readTXT(s);
            text = text.replace("________________\n", "");
            text = text.replace("________________", "");
            while (text.startsWith("\n")) {
                text = text.substring(1);
            }
            while (text.endsWith("\n")) {
                text = text.substring(0, text.length() - 1);
            }

            int count = Util.occurenceNumber(text, "\n");
            if (count > 1 && text.length() > 1) {
                rtn.add(s);
                c++;
            }
        }
        return rtn;
    }

    public static List<String> getPartsWithUppercaseLetters() {
        List<String> paths = Util.readFilesInSubfolderCondition("/Users/Martin/Documents/zakony/PDF/1990/", ".txt", new ArrayList());
        List<String> result = new ArrayList();
        for (String s : paths) {
            if (s.contains("/page/")
                    && !s.contains("/check")
                    && !s.contains("priloha")
                    && !s.contains("double")) {
                result.add(s);
                //System.out.println("s: "+ s);
            }
        }
        int index = 0;
        List<String> fin = new ArrayList();
        for (String path : result) {
            String text = Util.readTXT(path);
            String split[] = text.split("\\s");
            boolean check = false;
            for (String s : split) {
                while (s.length() > 0 && !Character.isLetter(s.toCharArray()[0])) {
                    s = s.substring(1);
                }
                while (s.length() > 0 && !Character.isLetter(s.toCharArray()[s.toCharArray().length - 1])) {
                    s = s.substring(0, s.length() - 1);
                }
                if (s.toLowerCase().contains("q")
                        || s.toLowerCase().contains(". .")
                        || s.toLowerCase().contains(" .")
                        || s.toLowerCase().contains("111")
                        || s.toLowerCase().contains("IlUl")) {

                    //System.out.println("====================");
                    //System.out.println("path: " + path);
                    //System.out.println("text: " + text);
                    //System.out.println("path: "+path);
                    //System.out.println(":: " + Space_OCR.spaceOCR(path));
                    //System.out.println("STARTED SPACE OCR");
                    //generateRepair(text,Space_OCR.spaceOCR(path.replaceAll(".txt","")));
                    fin.add(path);
                    check = true;
                }
                String allowed[] = {"CSc", "JUDr", "MPa", "kW", "DrSc", "MUDr", "PhDr", "MVDr"};
                boolean allow = true;
                for (String all : allowed) {
                    if (s.equals(all)) {
                        allow = false;
                    }
                }
                boolean b = Util.isUpperCase(s);
                b = Util.isLowerCase(s);
                if ((!Util.isLowerCase(s)
                        && !Util.isUpperCase(s)
                        && !Util.isLowerCase(s.substring(1))
                        && allow)) {
                    fin.add(path);
                    //System.out.println("STARTED SPACE OCR");
                    //generateRepair(text,Space_OCR.spaceOCR(path.replaceAll(".txt","")));

                    check = true;
                }

            }
            if (check) {
                index++;
                if (index > 10) {
                    //break;
                }
            }
            //System.out.println("=========================");
        }
        System.out.println("index: " + index);

        return fin;
    }

    private static void generateRepair(String text, String OCR) {
        System.out.println("text: " + text);
        System.out.println("OCR: " + OCR);
    }

    public static void spaceOCR(List<String> paths) {
        int index = 0;
        for (String path : paths) {
            path = path.replaceAll(".txt", "");
            //System.out.println("p: "+path.substring(0,path.lastIndexOf("/")));
            File f = new File(path.substring(0, path.lastIndexOf("/") + 1).replaceAll("/page/", "/space/"));
            if (!f.exists()) {
                f.mkdir();
            }
            System.out.println("OCR path: " + path);

            File fil = new File(path.replace("/page/", "/space/") + ".txt");
            if (fil.exists()) {
                System.out.println("ALREADY DONE: " + path);
                continue;
            }

            String ocr = Space_OCR.spaceOCR(path);
            ocr = ocr.replaceAll("- \\\\r\\\\n", "");
            Util.saveTXT(path.replace("/page/", "/space/") + ".txt", ocr);
            index++;
            System.out.println("index: " + index);
            if (index > 50) {
                System.exit(1);
                break;

            }
        }
    }

    public static void singleSpaceOCR(String path) {
        int index = 0;

        path = path.replaceAll(".txt", "");
        //System.out.println("p: "+path.substring(0,path.lastIndexOf("/")));
        File f = new File(path.substring(0, path.lastIndexOf("/") + 1).replaceAll("/page/", "/space/"));
        if (!f.exists()) {
            f.mkdir();
        }

        File fil = new File(path.replace("/page/", "/space/") + ".txt");
        if (fil.exists()) {
            System.out.println("ALREADY DONE: " + path);
            return;
        }
        System.out.println("OCR path: " + path);
        String ocr = Space_OCR.spaceOCR(path);
        ocr = ocr.replaceAll("- \\\\r\\\\n", "");
        Util.saveTXT(path.replace("/page/", "/space/") + ".txt", ocr);

    }

}
