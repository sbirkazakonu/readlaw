/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Read;

import static Read.ToString.path;
import Utils.Util;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class ToText {

    public ToText() {

    }

    /**
     *
     * @param inputPath
     *
     * @param index googleOcr index
     * @param outputFolder FOLDER!!!
     */
    public boolean readAndMove(String inputPath, String index, String outputFolder) {
        outputFolder = outputFolder.replaceAll("/page/", "/page/check");
        File outputFold = new File(outputFolder);
        if (!outputFold.exists()) {
            outputFold.mkdir();
        }
        path = "/Users/Martin/googleocr" + index + "/in/" + inputPath.substring(inputPath.lastIndexOf("/"));
        //System.out.println("path: " + path);
        Path input = Paths.get(inputPath, "");
        Path output = Paths.get(path, "");
        try {
            Files.copy(input, output, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            //Logger.getLogger(ToString.class.getName()).log(Level.SEVERE, null, ex);
            emptyFolders(index);
            System.out.println("repeat: -" + index + "-" + inputPath);
            return false;
        }
        execCommand(index);
        String addit = inputPath.substring(inputPath.lastIndexOf("/"));
        //System.out.println("addit: "+ addit);
        //System.out.println("outputFodler " + outputFolder);
        moveBack(outputFolder + addit, index);
        emptyFolders(index);
        //System.out.println("TRANSLATED in Thread " + index + ": " + inputPath);
        return true;
    }

    /**
     * vyprazdni slozky in a out
     */
    private void emptyFolders(String index) {
        List<String> in = Util.readAllFilePaths("/Users/Martin/googleocr" + index + "/in");
        List<String> out = Util.readAllFilePaths("/Users/Martin/googleocr" + index + "/out");
        for (String s : in) {
            File f = new File(s);
            f.delete();
        }
        for (String s : out) {
            File f = new File(s);
            f.delete();
        }

    }

    private void execCommand(String index) {

        Runtime rt = Runtime.getRuntime();
        Process p;
        try {
            p = rt.exec("/usr/local/bin/node /Users/Martin/googleocr" + index + "/index.js");
            p.waitFor();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ToString.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean moveBack(String inputPath, String index) {
        //System.out.println("inputPath "+ inputPath);
        path = "/Users/Martin/googleocr" + index + "/in/" + inputPath.substring(inputPath.lastIndexOf("/"));
        //System.out.println("path: " + path);
        Path input = Paths.get(path.replace("/in/", "/out/") + ".txt", "");
        Path output = Paths.get(inputPath + ".txt", "");
        try {
            //System.out.println("input: "+ input.toAbsolutePath());
            //System.out.println("output: "+ output.toAbsolutePath());
            Files.copy(input, output, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            //Logger.getLogger(ToString.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("NOT DONE: -" + index + "-" + inputPath);
            emptyFolders(index);
            //System.out.println("repeat: " + inputPath);
            return false;
        }
        return true;
    }

    /**
     *
     * @param inputPath
     */
    public boolean readAndMoveBack(String inputPath, String index) {
        path = "/Users/Martin/googleocr" + index + "/in/" + inputPath.substring(inputPath.lastIndexOf("/"));
        //System.out.println("path: " + path);
        Path input = Paths.get(inputPath, "");
        Path output = Paths.get(path, "");
        try {
            Files.copy(input, output, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            System.out.println("repeat");
            Logger.getLogger(ToString.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        execCommand(index);

        boolean res = moveBack(inputPath, index);

        emptyFolders(index);
        return res;
        //System.out.println("TRANSLATED: " + inputPath);
    }

}
