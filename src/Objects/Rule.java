/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import Utils.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextPane;

/**
 *
 * @a static JTextPane applyRules(JTextPane finish, List<Rule> rules) { throw
 * new UnsupportedOperationException("Not supported yet."); //To change body of
 * generated methods, choose Tools | Templates. } uthor Martin
 */
public class Rule {

    public static JTextPane applyRules(JTextPane finish, List<Rule> rules) {
        rules = Rule.readRules(Util.PATH_RULES);
        for (int i = rules.size() - 2; i < rules.size(); i++) {
            Rule r = rules.get(i);
            finish.setText(finish.getText().replace(r.oldString, r.newString));
        }
        return finish;
    }

    public static String applyRules(String input, List<Rule> rules) {
        rules = Rule.readRules(Util.PATH_RULES);
        for (int i = rules.size() - 2; i < rules.size(); i++) {
            Rule r = rules.get(i);
            input = input.replace(r.oldString, r.newString);
        }
        return input;
    }

    public String oldString, newString;

    public Rule(String oldString, String newString) {
        this.oldString = oldString;
        this.newString = newString;

    }

    public static List<Rule> readRules(String path) {
        List<Rule> rls = new ArrayList();
        File f = new File(path);
        if (!f.exists()) {
            System.out.println("path no exist: " + path);
            return rls;
        }
        String rules[] = Util.readTXT(path).split("\n");

        for (String s : rules) {
            String pom[] = s.split(Util.RULES_REGEX);
            if (pom.length > 1) {
                rls.add(new Rule(pom[0], pom[1]));
            }

        }
        return rls;
    }

    public static void saveRule(String path) {

    }

}
