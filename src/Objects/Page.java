/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import Utils.Util;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import pkg1990_plus.checkSeparate.FrameUtils;
import org.apache.commons.lang3.SerializationUtils;

/**
 *
 * @author Martin
 */
public class Page {

    public BufferedImage img, imgLines, imgRect;

    public boolean save;

    public String path;
    public int center;

    public List<Rectangle> rects, newLaw, additRepair, footer, priloha;

    public int rectIndex;

    public Page(String path) {
        save = true;
        this.img = Util.readImage(path);
        imgLines = Util.readImage(path);
        newLaw = new ArrayList();
        additRepair = new ArrayList();
        footer = new ArrayList();
        priloha = new ArrayList();
        center = img.getWidth() / 2;
        this.rects = FrameUtils.getRectangles(img);
        addLines();
        this.path = path;
        paintRectangles();

    }

    public Page(BufferedImage img, List<Line> lines) {
        this.img = img;
        //this.lines = lines;
        //paintLines();
    }

    public Page(BufferedImage img, List<Line> lines, String path) {
        this.img = img;
        //this.lines = lines;
        this.path = path;

        //paintLines();
    }

    public void paintRectangles() {
        //System.out.println("Paint: " + rects.size());
        imgLines = Util.readImage(path);
        for (Rectangle r : rects) {
            //System.out.println("r: " + r);
            int c = (int) (Math.random() * (double) 16000000);
            for (int i = r.x; i < r.x + r.width; i++) {
                imgLines.setRGB(i, r.y, c);
                imgLines.setRGB(i, r.y + 1, c);
                imgLines.setRGB(i, r.y + 2, c);
                imgLines.setRGB(i, r.y + 3, c);
                imgLines.setRGB(i, r.y + 4, c);
                imgLines.setRGB(i, r.y + r.height - 1, c);
                imgLines.setRGB(i, r.y + r.height - 2, c);
                imgLines.setRGB(i, r.y + r.height - 3, c);
                imgLines.setRGB(i, r.y + r.height - 4, c);
                imgLines.setRGB(i, r.y + r.height - 5, c);
            }
            for (int i = r.y; i < r.y + r.height; i++) {
                imgLines.setRGB(r.x, i, c);
                imgLines.setRGB(r.x + 1, i, c);
                imgLines.setRGB(r.x + 2, i, c);
                imgLines.setRGB(r.x + 3, i, c);
                imgLines.setRGB(r.x + 4, i, c);
                imgLines.setRGB(r.x + r.width - 1, i, c);
                imgLines.setRGB(r.x + r.width - 2, i, c);
                imgLines.setRGB(r.x + r.width - 3, i, c);
                imgLines.setRGB(r.x + r.width - 4, i, c);
                imgLines.setRGB(r.x + r.width - 5, i, c);
            }
        }
        //FrameUtils.heapStats();
    }

    public void paintRectangle(Rectangle r) {
        imgRect = Util.readImage(path);
        int c = (int) (Math.random() * (double) 16000000);
        for (int i = r.x; i < r.x + r.width; i++) {
            imgRect.setRGB(i, r.y, c);
            imgRect.setRGB(i, r.y + 1, c);
            imgRect.setRGB(i, r.y + 2, c);
            imgRect.setRGB(i, r.y + 3, c);
            imgRect.setRGB(i, r.y + 4, c);
            imgRect.setRGB(i, r.y + r.height - 1, c);
            imgRect.setRGB(i, r.y + r.height - 2, c);
            imgRect.setRGB(i, r.y + r.height - 3, c);
            imgRect.setRGB(i, r.y + r.height - 4, c);
            imgRect.setRGB(i, r.y + r.height - 5, c);
        }
        for (int i = r.y; i < r.y + r.height; i++) {
            imgRect.setRGB(r.x, i, c);
            imgRect.setRGB(r.x + 1, i, c);
            imgRect.setRGB(r.x + 2, i, c);
            imgRect.setRGB(r.x + 3, i, c);
            imgRect.setRGB(r.x + 4, i, c);
            imgRect.setRGB(r.x + r.width - 1, i, c);
            imgRect.setRGB(r.x + r.width - 2, i, c);
            imgRect.setRGB(r.x + r.width - 3, i, c);
            imgRect.setRGB(r.x + r.width - 4, i, c);
            imgRect.setRGB(r.x + r.width - 5, i, c);
        }

        FrameUtils.heapStats();
    }

    public void addRectangle(int x, int y, boolean isNewLaw) {

        Rectangle split = null;
        int index = 0;
        //System.out.println("rects.size(): " + rects.size());
        if (rects.isEmpty()) {
            createFirstRect(y);
            return;
        }
        for (Rectangle r : rects) {
            int minX = r.x;
            int maxX = r.x + r.width;
            int minY = r.y;
            int maxY = r.y + r.height;

            if (x > minX && x < maxX && y > minY && y < maxY) {
                split = r;
                index = rects.indexOf(r);
                //System.out.println(x + " " + y + " all rectangles:" + r);
            }

        }
        if (split == null) {
            fillBlank(x, y);
            return;
        }
        y = findAccurateY(split, y);
        //System.out.println("split: " + split);
        Rectangle up = new Rectangle(split.x, split.y, split.width, y - split.y);
        Rectangle down = new Rectangle(split.x, y, split.width, split.y + split.height - y);
        //System.out.println("up " + up);
        //System.out.println("down " + down);
        rects.set(index, up);
        if (isNewLaw) {
            newLaw.add(down);
        }
        rects.add(index + 1, down);
        //System.out.println(rects.indexOf(up) + " - " + rects.indexOf(down));
    }

    public void saveParts() {

        Runnable run = new Runnable() {

            @Override
            public void run() {
                System.out.println("SAVE " + save);
                //Uklada se? Pokud ne, jeho nazev se ulozi do textaku
                if (!save) {
                    File f = new File(path.substring(0, path.lastIndexOf("/page/") + 1) + "/deleted.txt");
                    if (f.exists()) {
                        System.out.println(f.getAbsolutePath());
                        String s = Util.readTXT(f.getAbsolutePath());
                        Util.saveTXT(f.getAbsolutePath(), s + "/n" + path);
                    } else {
                        System.out.println("AA");
                        Util.saveTXT(f.getAbsolutePath(), path);
                    }
                    return;
                }

                int index = 0;
                for (Rectangle r : rects) {
                    if (!checkIfPart(r)) {
                        continue;
                    }
                    BufferedImage sImg = img.getSubimage(r.x, r.y, r.width, r.height);
                    checkIfSpecial(index, footer, r, "/footers.txt");
                    checkIfSpecial(index, newLaw, r, "/newLaw.txt");
                    checkIfSpecial(index, additRepair, r, "/repair.txt");
                    checkIfSpecial(index, priloha, r, "/priloha.txt");

                    //System.out.println("path: " + path);
                    Util.saveImage(sImg, path.substring(0, path.lastIndexOf(".")) + "_" + index + ".png");
                    index++;
                }
            }
        };
        Thread t = new Thread(run);
        t.start();

    }

    private void checkIfSpecial(int index, List<Rectangle> list, Rectangle r, String fileName) {
        for (Rectangle test : list) {
            if (test.x == r.x
                    && test.y == r.y
                    && test.width == r.width
                    && test.height == r.height) {
                System.out.println("======" + fileName + "======");
                saveLawName(index, fileName);
            }
        }
    }

    private void saveLawName(int index, String fileName) {
        String additPath = path.substring(0, path.lastIndexOf("/page/")) + fileName;
        File f = new File(additPath);
        if (f.exists()) {
            String text = Util.readTXT(additPath);
            Util.saveTXT(additPath,
                    text + "\n" + path.substring(0, path.lastIndexOf(".")) + "_" + index + ".png");
        } else {
            Util.saveTXT(additPath,
                    path.substring(0, path.lastIndexOf(".")) + "_" + index + ".png");
        }
    }

    private void addLines() {
        //left
        List<Point> indexes = new ArrayList();
        for (Rectangle r : rects) {
            //System.out.println("Border " + ((img.getWidth() / 3) * 2));
            if (r.width < ((img.getWidth() / 3) * 2)) {
                indexes.addAll(getAdditLines(r));
            }
        }
        for (Point p : indexes) {
            addRectangle(p.x, p.y, false);
        }

        //right
    }

    private List<Point> getAdditLines(Rectangle r) {
        int pixels[][] = Util.getPixelArray(img);
        int count = 0;
        int space = 8;
        boolean reset = false;
        List<Point> addit = new ArrayList();
        for (int i = r.y; i < r.y + r.height; i++) {
            for (int j = r.x; j < r.x + r.width; j++) {
                if (pixels[i][j] != -1) {
                    if (count > space) {
                        addit.add(new Point(r.x + r.width / 2, i - count / 2));
                    }
                    count = 0;
                    break;
                }
            }
            count++;
        }
        return addit;
    }

    public BufferedImage paintOneRect() {
        paintRectangle(rects.get(rectIndex));
        return imgRect;
    }

    private boolean checkIfPart(Rectangle input) {

        int pixels[][] = Util.getPixelArray(img);
        int count = 0;
        for (int i = input.x; i < input.x + input.width; i++) {
            for (int j = input.y; j < input.y + input.height; j++) {
                if (pixels[j][i] != -1) {
                    count++;
                }
            }
            if (count > 15) {
                return true;
            }
        }

        return false;
    }

    public void savePNG() {
        Runnable r = () -> {
            int index = (int) (Math.random() * 10000);
            System.out.println("index:" + index);
            Util.saveImage(img, path.replaceAll(".png", "_priloha" + index + ".png"));
        };
        Thread t = new Thread(r);
        t.start();

    }

    private void createFirstRect(int y) {
        Rectangle up = new Rectangle(0, 0, img.getWidth(), y);
        Rectangle down = new Rectangle(0, y, img.getWidth(), img.getHeight() - y);
        rects.add(up);
        rects.add(down);
    }

    private int findAccurateY(Rectangle r, int y) {
        int pixels[][] = Util.getPixelArray(img);
        for (int i = 0; true; i++) {
            boolean checkUp = true;
            boolean checkDown = true;

            for (int j = r.x; j < r.x + r.width; j++) {
                if (pixels[y + i][j] != -1) {
                    checkDown = false;
                    break;
                }
            }
            if (checkDown) {
                return y + i;
            }
            for (int j = r.x; j < r.x + r.width; j++) {
                if (pixels[y - i][j] != -1) {
                    checkUp = false;
                    break;
                }
            }
            if (checkUp) {
                return y - i;
            }
        }
    }

    private void fillBlank(int x, int y) {
        int closeTop = img.getHeight();
        int closeBottom = img.getHeight();
        Rectangle bottom = null;
        Rectangle top = null;

        for (Rectangle r : rects) {
            if (r.y < y) {
                continue;
            }
            if (r.y - y < closeBottom && r.y - y > 0) {
                closeBottom = r.y - y;
                bottom = r;

            }

        }
        for (Rectangle r : rects) {
            if (r.y + r.height > y) {
                continue;
            }
            if (y - r.y - r.height < closeTop) {
                closeTop = y - r.y - r.height;
                top = r;
            }
        }

        int rectX = 0;
        int rectY = 0;
        int rectWidth = img.getWidth();
        int rectHeight = 0;
        if (top == null) {
            rectY = 0;
            rectHeight = bottom.y;
            Rectangle result = new Rectangle(rectX, rectY, rectWidth, rectHeight);
            rects.add(0, result);
            return;
        }
        int insertIndex = rects.indexOf(top) + 1;
        if (bottom == null) {
            rectY = top.y + top.height;
            rectHeight = img.getHeight() - top.y - top.height;
            Rectangle result = new Rectangle(rectX, rectY, rectWidth, rectHeight);
            rects.add(insertIndex, result);
            return;
        }

        rectY = top.y + top.height;
        rectHeight = bottom.y - rectY;
        Rectangle result = new Rectangle(rectX, rectY, rectWidth, rectHeight);
        System.out.println("ADDED NEW BLANK: " + result);
        rects.add(insertIndex, result);
        sortRectangles();

    }

    private void sortRectangles() {
        List<Rectangle> result = new ArrayList();
        List<Rectangle> wide = new ArrayList();
        for (Rectangle r : rects) {
            if (r.width > img.getWidth() * 2 / 3) {
                wide.add(r);
            }
        }
        Comparator<Rectangle> comp = new Comparator<Rectangle>() {
            @Override
            public int compare(Rectangle o1, Rectangle o2) {
                return new Integer(o1.y).compareTo(o2.y);
            }
        };
        Collections.sort(wide, comp);
        for (int i = 0; i < wide.size(); i++) {
            int begin = 0;
            int end = 0;
            //List<Rectangle> half = new ArrayList();
            if (i == 0) {
                end = wide.get(i).y;
                System.out.println("if 1");

            } else if (i == wide.size()) {
                begin = wide.get(i).y + wide.get(i).height;
                end = img.getHeight();
                System.out.println("if 2");

            } else {
                begin = wide.get(i - 1).y + wide.get(i - 1).height;
                end = wide.get(i).y;
                System.out.println("if 3");
            }
            //mezi jendotlivymi sirokymi rectangli najdu tz polovicni
            List<Rectangle> right = new ArrayList();
            System.out.println("begin: " + begin + " end: " + end);
            for (Rectangle r : rects) {
                if (r.y <= end && r.y >= begin && r.width < img.getWidth() * 2 / 3) {
                    if (r.x < 50) {
                        result.add(r);
                    } else {
                        right.add(r);
                    }
                }
            }
            System.out.println("ADD RIGHT: " + right.size());
            result.addAll(right);
            result.add(wide.get(i));
        }
        rects = result;
    }

}
