/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import java.awt.Point;

/**
 *
 * @author Martin
 */
public class Line {

    Point begin;
    Point end;

    public Line(Point x, Point y) {
        this.begin = x;
        this.end = y;

    }

    @Override
    public String toString() {
        String result = "Line: ["+this.begin.x+";"+this.begin.y + "] to ["+this.end.x+";"+this.end.y + "]";
        return result;

    }
}
