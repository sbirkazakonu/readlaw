/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import Repairing.Dictionary;
import java.awt.image.BufferedImage;
import java.io.File;
import Utils.Util;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.swing.JTextPane;

/**
 *
 * @author Martin
 */
public class Part {

    final String PART_CAST = "část ";
    final String PART_HLAVA = "hlava ";
    final String PART_ODDIL = "oddíl ";
    final String PART_DIL = "díl ";
    final String KEY_LEVEL = "level_";

    public BufferedImage img;
    public String text, repairText, tagText, pngPath, txtPath, repairPath, tagPath;
    public String paraTitle;
    public HashMap thisSection;
    public HashMap nextSection;

    public JTextPane repaired;

    public boolean ready;

    public Part(BufferedImage img) {
        this.img = img;

    }

    public Part(BufferedImage readImage, String string) {
        this.img = readImage;
        this.text = string;
    }

    public Part(String pngPath, String txtPath) {
        this.img = Util.readImage(pngPath);
        this.text = Util.readTXT(txtPath);
        this.txtPath = txtPath;
        this.repairPath = txtPath.replace(".txt", "_rep.txt");
        this.tagPath = txtPath.replace(".txt", "_tag.txt");
        //System.out.println("TXTPATH " + txtPath);
        //System.out.println("TEXT: " + this.text);
        //System.out.println("------------");
        getRepairText();
        //getTaggedText();
        thisSection = new HashMap();
        nextSection = new HashMap();
        this.pngPath = pngPath;


    }

    public Part(BufferedImage readImage, String text, String pngPath, String txtPath) {
        this.img = readImage;
        this.text = text;
        this.txtPath = txtPath;
        this.repairPath = txtPath.replace(".txt", "_rep.txt");
        this.tagPath = txtPath.replace(".txt", "_tag.txt");
        getRepairText();
        getTaggedText();
        thisSection = new HashMap();
        nextSection = new HashMap();
        this.pngPath = pngPath;
    }

    private String applyRules() {
        List<Rule> rules = Rule.readRules(Util.PATH_RULES);
        for (Rule r : rules) {
            this.repairText = repairText.replace(r.oldString, r.newString);
        }
        return this.repairText;

    }

    private void getRepairText() {
        File f = new File(repairPath);
        if (f.exists()) {
            repairText = Util.readTXT(repairPath);
            return;
        } else {
            repairText = text;
        }
        this.repairText = applyRules();
        //začínat bude písmenem
        for (int i = 0; i < repairText.length(); i++) {
            if (repairText.charAt(i) == 32
                    || repairText.charAt(i) == 10 // ||repairText.charAt(i) == 65279
                    ) {

            } else {
                //System.out.println();
                //System.out.println("ahoj "+ i+ " -"+ ((int)repairText.charAt(i)) + "-");
                repairText = repairText.substring(i);
                break;
            }
        }
        //řádek nezačíná mezerou
        repairText = repairText.replace("\n ", "\n");

        //pokud začíná paragrafem, odřádkuje ho
        if (repairText.length() > 0 && repairText.charAt(0) == '§') {
            int spaceIndex = Util.ordinalIndexOf(repairText, ' ', 1);
            int lineIndex = Util.ordinalIndexOf(repairText, '\n', 0);
            if (spaceIndex < lineIndex && spaceIndex >= 0) {
                repairText = repairText.substring(0, spaceIndex)
                        + "\n" + repairText.substring(spaceIndex + 1);
            }
        }
        // Text vždy končí tečkou pak jsou zpravidla nadpisy. Poslední tečka je odřádkováná
        int lastDotIndex = repairText.lastIndexOf(".");
        //System.out.println("lastIndexOf dot " + lastDotIndex);
        if (lastDotIndex > 0 && lastDotIndex < repairText.length() - 5) {
            String mesice[] = {"led", "úno", "bře", "dub", "kvě", "čer", "srp", "zář", "říj", "lis", "pro"};

            boolean month = true;
            for (String m : mesice) {
                if (repairText.substring(lastDotIndex, lastDotIndex + 5).contains(m)) {
                    month = false;
                }
            }
//            if (month) {
//                repairText = repairText.substring(0, lastDotIndex + 1)
//                        + "\n" + repairText.substring(lastDotIndex + 1);
//            }

        }
        //pokud je za poslední tečkou ČÁST, DÍL nebo podobné chujoviny odřádkuje hezky
        String end = repairText.substring(lastDotIndex + 1);
        if (end.toLowerCase().contains("část ")) {
            String pom = end.substring(end.toLowerCase().lastIndexOf("část"));
            int endIndex = Util.ordinalIndexOf(pom, ' ', 1) >= 0 ? Util.ordinalIndexOf(pom, ' ', 1) : 0;
            if (endIndex > Util.ordinalIndexOf(pom, '\n', 0) && Util.ordinalIndexOf(pom, '\n', 0) >= 0) {
                endIndex = Util.ordinalIndexOf(pom, '\n', 0);
            }
            pom = pom.substring(0, endIndex);
            repairText = repairText.replace(pom, pom + "\n");
        } else if (end.toLowerCase().contains("hlava ")) {
            String pom = end.substring(end.toLowerCase().lastIndexOf("hlava"));
            pom = pom.substring(0, Util.ordinalIndexOf(pom, ' ', 1) >= 0 ? Util.ordinalIndexOf(pom, ' ', 1) : 0);
            repairText = repairText.replace(pom, pom + "\n");
        }


        //řádek nezačíná mezerou 
        repairText = repairText.replace("\n ", "\n");
        repairText = repairText.replace("\n\n\n", "\n\n");
        repairText = repairText.replace("  ", " ");

    }

    public static List<Part> readParts(String folderPath) {
        List<String> files = new ArrayList();
        List<Part> result = new ArrayList();
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_PARA));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_TITLE));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_OTHERS));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_FOOTNOTE));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_PARA.replace("/", "\\") + "\\"));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_TITLE.replace("/", "\\")));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_OTHERS.replace("/", "\\")));
        files.addAll(Util.readAllFilePaths(folderPath + Util.FOLDER_FOOTNOTE.replace("/", "\\")));

        System.out.println("fiels.siye" + files.size());
        System.out.println(folderPath + Util.FOLDER_FOOTNOTE.replace("/", "\\"));
        System.out.println("===================");
        List<String> pom = new ArrayList();
        for (String p : files) {
            if (!p.contains(".txt")) {
                pom.add(p);
            }
        }
        files = pom;
        files = Util.sortByLastNumber(files);
        for (String p : files) {
            result.add(
                    new Part(Util.readImage(p), Util.readTXT(p + ".txt"),
                            p, p + ".txt")
            );

        }
        return result;

    }

    private void getTaggedText() {
        File f = new File(tagPath);
        if (f.exists()) {
            tagText = Util.readTXT(tagPath);
        } else {
            tagText = text;
        }
    }

    public void createRepaired() {
        repaired = new JTextPane();
        Dictionary.setColoredText(repairText, repaired);

        ready = true;
    }

    public void getParaTitle() {
        String split[] = repairText.split("\n");
        String possibleTitle = "";

        for (int i = 0; i < split.length; i++) {
            if (split[i].contains("§")) {
                if (i + 1 < split.length) {
                    //System.out.println("JO" + split[i] + " - " + split[i + 1]);
                    possibleTitle = split[i + 1];
                    break;
                }
            }
            if (i > 2) {
                return;
            }
        }
        String breakers[] = {"(1)", "[", "]", "{", "}", "a)", ".", ",", ";", ":"/*,"","","","","",""*/};
        int count = 0;
        for (String b : breakers) {
            if (possibleTitle.contains(b)) {
                return;
            }
        }
        String pom = possibleTitle.replaceAll("\\s", "");

        if (pom.length() > 0 && pom.substring(pom.length() - 1).equals(".")) {
            return;
        }
        paraTitle = possibleTitle;
        //Dictionary.addWord(repaired, "\n\n" + possibleTitle, Color.green);
    }

    /**
     * Nejden a konci nadpisy a ulozi je spravne
     *
     * @return
     */
    public void getNextTitle() {
        copyThisToNext();
        int end = repairText.lastIndexOf(".");
        String part = repairText.substring(end >= 0 ? end + 1 : 0);

        //text = part;
        String split[] = part.split("\n");
        for (String s : split) {
            if (s.toLowerCase().contains(PART_CAST)) {
//                this.nextSection.put("text", s);
//                this.nextSection.put("level_" + "0", PART_CAST);
//                getTitle - nekdy
                createNext(s, PART_CAST);
            }
            if (s.toLowerCase().contains(PART_HLAVA)) {
//                this.nextSection.put("text", s);
//                this.nextSection.put("level_" + "1", PART_HLAVA);

            }
            if (s.toLowerCase().contains(PART_ODDIL)) {
                createNext(s, PART_ODDIL);
            }
        }
    }

    /**
     * Musi prekopirovat vsechny data z this to next Predchozi i nasledujici
     * pokracuji
     */
    private void copyThisToNext() {
        Object keys[] = thisSection
                .keySet()
                .toArray();
        for (int i = 0; i < keys.length; i++) {
            String k = (String) keys[i];
            nextSection.put(k, thisSection.get(k));
            //System.out.println("key: " + key.toArray()[i]);
        }
        //this.nextSection.put("text", this.thisSection.get("text"));
    }

    private void createNext(String line, String part) {
        int partLevel = levelOfPart(part);
        System.out.println("PartLevel " + partLevel);
        System.out.println(thisSection);
        if (partLevel == -1) {
            if (isNewPart()) {
                //vytvori prvni part
                //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + nextSection);
                this.nextSection.put("level_0_text", line);
                this.nextSection.put("level_0_type", part);
                //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + nextSection);
                return;
            } else {
                //prida dalsi part - yjisti nejvyssi a prida o jednu vys
                int topLevel = getTopLevel();
                topLevel++;
                //System.out.println(topLevel + " HOVNO " + part);
                this.nextSection.put("level_" + topLevel + "_text", line);
                this.nextSection.put("level_" + topLevel + "_type", part);
                return;
            }

        }
        //delete levels above

        nextSection.put(KEY_LEVEL + partLevel + "_text", line);
        // change na tom level
    }

    /*
     HashMap design -    level_1 - Cast
     level_2 - Hlava
     level_3 - 
     */
    private String getLevel(HashMap<String, Object> hash) {

        Set key = hash.keySet();
        for (int i = 0; i < key.size(); i++) {
            String k = (String) key.toArray()[i];
            int level = 0;
            int finLevel = 0;
            if (k.contains(KEY_LEVEL)) {
                level = Integer.parseInt(k.substring(k.lastIndexOf(KEY_LEVEL) + KEY_LEVEL.length()));
                if (level > finLevel) {
                    finLevel = level;
                }
            }

            System.out.println("key: " + key.toArray()[i]);
        }
        return "";
    }

    private int levelOfPart(String part) {
        Object keys[] = thisSection
                .keySet()
                .toArray();
        int level = -1;
        for (int i = 0; i < keys.length; i++) {
            String key = (String) keys[i];
            if (thisSection.get(key).equals(part)) {
                System.out.println("Value: " + thisSection
                        .get(key)
                        .toString());
                level = Integer.parseInt(key
                        .split("_")[1]);
            }
        }
        System.out.println("level: " + level);
        return level;
    }

    private boolean isNewPart() {
        System.out.println("ThisSection " + thisSection);
        if (thisSection.keySet().toArray().length == 0 && nextSection.keySet().toArray().length == 0) {
            System.out.println("KeySet: " + Arrays.toString(thisSection.keySet().toArray()));
            return true;
        }
        return false;
    }

    private int getTopLevel() {

        Set key = nextSection.keySet();
        int level = 0;
        int finLevel = 0;

        for (int i = 0; i < key.size(); i++) {
            String k = (String) key.toArray()[i];

            if (k.contains(KEY_LEVEL)) {
                level = Integer.parseInt(k.split("_")[1]);
                if (level > finLevel) {
                    finLevel = level;
                }
            }

            System.out.println("key: " + key.toArray()[i]);
        }
        return finLevel;
    }
}
