/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repairing;

import Read.FinishLaw;
import Utils.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
public class Footnotes {

    final static String footnotes[] = {"*)", "')", ".)", "\")", "“)", "”)", "ě)", "°)", "^)", "?)"};

    public static List<HashMap> correct() {
        // Get current time
        long start = System.currentTimeMillis();
        float elapsStart = start / 1000F;
        System.out.println("Start: " + elapsStart);
        List<String> paths = getRelevantPaths();

        long elapsedTimeMillis = System.currentTimeMillis() - start;
        float elapsedTimeSec = elapsedTimeMillis / 1000F;
        System.out.println("TIME: " + elapsedTimeSec);
        List<HashMap> input = createHashInput(paths);
        elapsedTimeMillis = System.currentTimeMillis() - start;
        elapsedTimeSec = elapsedTimeMillis / 1000F;
        System.out.println("TIME: " + elapsedTimeSec);
        for (HashMap h : input) {
            HashMap has = repair(h);
        }

        return input;
    }

    private static List<String> getRelevantPaths() {
        String conds[] = {"/page/page", ".png.txt"};
        List<String> paths = Util.readFilesInSubfolderCondition("/Users/Martin/Documents/zakony/PDF/1990/", conds, new ArrayList());
        System.out.println("paths.size: " + paths.size());
        paths = Footnotes.getPossibleFootnotes(paths);
        System.out.println("paths.size 2: " + paths.size());
        List<String> pth = new ArrayList();
        for (String path : paths) {
            if (path.substring(path.lastIndexOf("/")).contains("_")) {
                pth.add(path);
            }
            if (pth.size() > 250) {
                break;
            }
        }
        paths = pth;
        System.out.println("paths.siye: " + paths.size());
        return paths;
    }

    public static List<String> getPossibleFootnotes(List<String> files) {
        System.out.println("files.size: " + files.size());
        List<String> res = new ArrayList();
        int index = 0;
        for (String s : files) {
            //System.out.println("s: " + s);
            if (s.endsWith(".png")) {
                s += ".txt";
            }
            String text = Util.readTXT(s);
            //obshuje dvoutečku
            if (text.contains("::") || text.contains(" : ")) {
                res.add(s.replaceAll(".txt", ""));
                //result.add(new Part(s.replaceAll(".txt", ""), s));
                //System.out.println("\n------------------ :: : " + s);
                continue;
            }

            if (isFootnote(text)) {
                //System.out.println("-------");
                res.add(s.replaceAll(".txt", ""));
                //break;
            }

            if (res.size() > 50) {
                System.out.println("BROKEN");
                break;
            }
        }
        res = Util.removeDuplicate(res);
        return res;
    }

    private static boolean isFootnote(String text) {
        int index = 0;
        String origText = text;
        boolean result = false;
        for (String f : footnotes) {

            text = origText;
            while (text.contains(f)) {
                text = text.substring(0, text.lastIndexOf(f) + f.length());
                String snippet = text.substring(text.length() > 60 ? text.length() - 60 : 0);
                if (text.endsWith("Zb.)")
                        || text.endsWith("Sb.)")
                        || text.endsWith("apod.)")
                        || text.endsWith(" atd.)")
                        || text.endsWith(" nasl.)")) {
                    //System.out.println("text: "+text);
                } else if ((f.equals("“)"))
                        && (snippet.contains("(dále jen „"))
                        && (!snippet.substring(snippet.indexOf("(dále jen „") + "(dále jen „".length(), snippet.length()).contains("(")
                        || !snippet.substring(snippet.indexOf("(dále jen „") + "(dále jen „".length(), snippet.length()).contains(")"))) {
                    //nic se neděje -vraci se případně false a npŕidá se do footnote lisut
                } else if ((f.equals("“)"))
                        && (snippet.contains("(ďalej len „"))
                        && (!snippet.substring(snippet.indexOf("(ďalej len „") + "(ďalej len „".length(), snippet.length()).contains("(")
                        || !snippet.substring(snippet.indexOf("(ďalej len „") + "(ďalej len „".length(), snippet.length()).contains("("))) {
                    //nic se neděje -vraci se případně false a npŕidá se do footnote lisut

                } else if (f.equals("“)") && firstNonLetter(snippet)) {
                    //System.out.println("snip: " + snippet);
                } else {
                    //ani jedna z výjimek, je footnote pravděpodobně
                    return true;
                }
                text = text.substring(0, text.lastIndexOf(f));
                index++;
            }
        }

        //mrknout, jestli kousek před tím je spodní uvozovky, před tím je něco jako dále jen a před tím obrácená závorka
        return result;
    }
    static int match = 0;
    static int noMatch = 0;

    public static HashMap repair(HashMap input) {
        String text = Util.readTXT(input.get("drive") + "").replace("<foot>", "").replaceAll("</foot>", "");
        String origText = text;
        //Získá slovo co je před footnotem a za ním
        String before = "";
        String after = "";
//        int match = 0;
//        int noMatch = 0;
        for (String foot : footnotes) {
            text = origText;
            while (text.contains(foot)) {
                //System.out.println("FULL TEXT: " + text);
                before = getWordBefore(text, foot);
                after = getWordAfter(text, foot);
                int index = text.lastIndexOf(foot);
                String space = Util.readTXT(input.get("check") + "").replaceAll("\\\\r\\\\n", "");
                //System.out.println("TEXT: " + text);
                String footNumber = getFootnoteFromSpace(space, index);
                if (footNumber.length() > 0) {
                    //text = replaceFootnote(text, footNumber);
                }
                text = text.substring(0, text.lastIndexOf(foot));
            }

        }

        System.out.println(match + "====================================" + noMatch);
        return input;
    }

    private static List<HashMap> createHashInput(List<String> paths) {
        List<HashMap> input = new ArrayList();
        int index = 0;
        for (String s : paths) {
            FinishLaw.singleSpaceOCR(s.replace(".txt", "").replaceAll("/check/", "/"));
            s = s + ".txt";
            HashMap h = new HashMap();
//            String regularPath = s.replaceAll("/space/", "/page/");
            h.put("image", s.replace(".txt", "").replaceAll("/check/", "/"));

            h.put("drive", s);
            h.put("check", s.replace("/page/", "/space/"));
            input.add(h);
            index++;

            if (index > 150) {
                break;
            }
        }
        return input;
    }

    private static boolean firstNonLetter(String text) {
        //System.out.println("tex non lettert: "+ text);
        if (text.endsWith("“)")) {
            text = text.substring(0, text.length() - "“)".length());
        }
        if (text.contains("„")) {
            text = text.substring(text.lastIndexOf("„") + "„".length());
        } else {
            return false;
        }
        if (text.length() > 25) {
            return false;
        }
        //System.out.println("NON LET: " + text);
        for (char c : text.toCharArray()) {
            if (Character.isLetter(c) || Character.isSpaceChar(c)) {
                //return false;
            } else {
                return false;
            }
        }

        //System.out.println("tex non lettert: "+ text);
        return true;

    }

    private static String getWordBefore(String text, String foot) {
        //System.out.println("text 1: " + text);
        text = text.substring(0, text.lastIndexOf(foot));
        //System.out.println("text 2: " + text);
        if (text.contains(" ")) {
            text = text.substring(text.lastIndexOf(" ") + 1);
        }
        //System.out.println("text 3: " + text);
        return text;

    }

    private static String getWordAfter(String text, String foot) {
//        System.out.println("foot: " + foot);
        //System.out.println("text 1: " + text);
        text = text.substring(text.lastIndexOf(foot) + foot.length());
        //System.out.println("text 2: " + text);
        while (text.length() > 0
                && !Character.isLetter(text.toCharArray()[0])) {
            text = text.substring(1);
        }
        //System.out.println("text 3: " + text);

        if (text.contains(" ")) {
            text = text.substring(0, text.indexOf(" ") + 1);
        }
        //System.out.println("text 4: " + text);
        return text;
    }

    private static String getFootnoteFromSpace(String space, int index) {
        String result = "";
        while (space.contains(")")) {
            space = space.substring(0, space.lastIndexOf(")") + 1);
            if (space.lastIndexOf(")") - 7 < index && space.lastIndexOf(")") + 7 > index) {
            } else {
                space = space.substring(0, space.lastIndexOf(")"));
                continue;
            }
            String spacepart = space.substring(space.lastIndexOf(")") - 7 >= 0 ? space.lastIndexOf(")") - 7 : 0, space.lastIndexOf(")") + 7 < space.length() ? space.lastIndexOf(")") + 7 : space.length());
            if (Util.occurenceNumber(spacepart, ")") == 1) {
                spacepart = spacepart.substring(0, spacepart.length() - 1);
                System.out.println("spacePart: "+spacepart);
                for (int i = spacepart.length()-1; i >= 0; i--) {
                    
                    char c = spacepart.charAt(i);
                    System.out.println("c: "+ c);
                    if (Character.isDigit(c)) {
                        
                        result = c + result;
                    }
                }
                System.out.println("result: "+result);
                return result;
            }

            space = space.substring(0, space.lastIndexOf(")"));
        }
        System.out.println("result: " + result);
        return result;

    }

    private static String replaceFootnote(String text, String footNumber) {

        return "";
    }

}
