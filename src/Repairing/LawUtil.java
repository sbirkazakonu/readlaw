/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repairing;

import Utils.Util;
import java.io.File;

/**
 *
 * @author Martin
 */
public class LawUtil {

    /**
     * Z metadat ziska nazev zakon
     *
     * @param s
     * @return
     */
    public static String getLawName(String s) {
        //System.out.println(s);
        String result = "";
        File txt = new File(s + "/metadata.txt");
        File docx = new File(s + "/metadata.docx");
        if (txt.exists()) {
            result = Util.readTXT(s + "/metadata.txt");
        } else if (docx.exists()) {
            result = Util.readTXT(s + "/metadata.docx");
        }

        result = result.replace("_", "/");
        result = result.replace("------", "");
        result = result.replace("\n", "");
        //System.out.println(result);
        return result;
    }

    /**
     * Platnost zákona, odvíjí se od vydání sbírky, ta je na první straně
     *
     * @param text Text prnví stránky (titulka částky)
     * @return
     */
    public static String getPlatnost(String text) {
        String result = text.substring(text.lastIndexOf("Vydána dne") + "Vydána dne".length());
        for (int i = 0; i < result.length() - 4; i++) {
            if (Character.isDigit(result.charAt(i))
                    && Character.isDigit(result.charAt(i + 1))
                    && Character.isDigit(result.charAt(i + 2))
                    && Character.isDigit(result.charAt(i + 3))) {
                result = result.substring(0, i + 4);
                break;
            }
        }
        return result;
    }

    public static int prepareNumber(String path) {

        if (path.contains("/")) {
            path = path.substring(path.lastIndexOf("/") + 1);
        } else if (path.contains("\\")) {
            path = path.substring(path.lastIndexOf("\\") + 1);
        }

        path = path.substring(0, path.lastIndexOf("_"));
        //System.out.println("Number "+path);
        return Integer.parseInt(path);
    }

    public static int prepareYear(String path) {
        if (path.contains("/")) {
            path = path.substring(path.lastIndexOf("/"));
        } else if (path.contains("\\")) {
            path = path.substring(path.lastIndexOf("\\"));
        }

        path = path.substring(path.lastIndexOf("_") + 1);
        path = path.replaceAll(" Sb.", "");
        System.out.println("Year: " + path);
        return Integer.parseInt(path);
    }

}
