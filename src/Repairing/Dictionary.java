/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repairing;

import Objects.Rule;
import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import Utils.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.text.SimpleAttributeSet;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Martin
 */
public class Dictionary {

    public static String words[];
    public static String dict;
    final static String PATH_DICT = "slovnik.txt";
    final static String PATH_CHARS = "char_pairs.txt";
    final static String PATH_RULES = "rules.txt";

    static boolean isCzech;

    public static void readWords() {
        dict = Util.readTXT(PATH_DICT);
        words = dict.split("\n");
    }

    public static void setColoredText(String str, JTextPane finish) {
        //sehnat jednotlivy výslova
        isCzech = testlanguage(str);

        finish.setText("");
        str = Rule.applyRules(str, Rule.readRules(PATH_RULES));
        str.replaceAll("\n \n", "\n");

        String lines[] = str.split("\n");

        lines = deleteEmptyLines(lines);
        lines = connectRelevantLines(lines);

        //vybrat ktery nejsou ve slovniku
        for (int i = 0; i < lines.length; i++) {
            //Testování celého řádku 
            HashMap h = checkRow(lines[i]);
            if (lines[i].contains("  ")) {
                System.out.println("DOUBLE SPACE");
            }
            if (((boolean) h.get("add"))) {
                if ((boolean) h.get("append")) {
                    addRow(finish, lines[i], h);
                    addWord(finish, "\n");
                }
                continue;
            }

            // Testování jednotlivých slov
            String[] wrds = lines[i].split(" ");
            for (String word : wrds) {
                addWord(finish, word + " ");
            }
            // nový řádek, protože byly odstraněny...
            addWord(finish, "\n");
        }
        StyledDocument doc = finish.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

    }

    private static void addWord(JTextPane finish, String word) {
        StyledDocument doc = finish.getStyledDocument();
        Style style = finish.addStyle("I'm a Style", null);
        HashMap<String, Object> h = isInDictionary(word);
        if (h.get("color") == Color.red || h.get("color") == Color.green) {
            StyleConstants.setBackground(style, (Color) h.get("color"));
        } else {
            StyleConstants.setForeground(style, (Color) h.get("color"));
        }
        try {
            doc.insertString(doc.getLength(), (String) h.get("word"), style);
        } catch (BadLocationException e) {
        }

    }

    private static HashMap<String, Object> isInDictionary(String input) {
        boolean check = true;
        boolean small = getCase(input);
        //System.out.println("words-size: "+ words.length);
        HashMap<String, Object> result = new HashMap();
        if (input.equals(": ") || input.equals(":") || input.contains("::")) {
            result.put("word", input);
            result.put("color", Color.GREEN);
            return result;
        }
        if (input.startsWith(":")) {
            result.put("word", input);
            result.put("color", Color.GREEN);
            return result;
        }
        String modInput = cleanInput(input, true, true);
        if (autoSkip(modInput)) {
            result.put("word", input);
            result.put("color", Color.black);
            //System.out.println("==========================");
            return result;
        }

        for (String dict : words) {

            if (modInput.equals(dict)) {
                result.put("word", input);
                result.put("color", Color.black);
                return result;
            }
        }
        result.put("word", input);
        if (!isCzech) {
            result.put("color", Color.red);
            return result;
        }

//záměna podobného písmenka pokud vede k úspěchu, dostane to modrou
        List<Rule> pairs = Rule.readRules(PATH_CHARS);
        //System.out.println(pairs.size());
        if (input.length() < 3 || modInput.length() < 3) {
            result.put("color", Color.red);
            return result;
        }
// Proletí vsechny pravidla 
        for (Rule r : pairs) {
// Provede zamenu na kazdy pozici, kde se objevuje old string            
            for (int i = 0; i < occurenceNumber(input, r.oldString); i++) {
                modInput = changeInput(input, i, r.oldString, r.newString);
// Zkouskne, jeslti to je ve slovniku 
                String testInput = cleanInput(modInput, true, true);
                //String testInput = modInput.toLowerCase();

                for (String s : words) {
                    if (s.equals(testInput)) {
                        result.put("color", Color.blue);
                        result.put("word", modInput);
                        //System.out.println("testInput: " + testInput);
                        return result;
                    }
                }
            }
            //System.out.println("=======================");
        }

        result.put("color", Color.red);
        return result;

    }

    /**
     * je spis malz nebo velký
     *
     * @param input
     * @return
     */
    private static boolean getCase(String input) {
        int smallCount = 0;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isLowerCase(input.charAt(0))) {
                smallCount++;
            }
            if (smallCount > input.length() / 2) {
                return true;
            }
        }
        return smallCount > input.length() / 2;

    }

    private static int occurenceNumber(String input, String oldString) {
        int count = 0;
        for (int i = 0; i < input.length() - oldString.length(); i++) {
            //System.out.println("Test: -"+input.substring(i, i+oldString.length())+"-");
            if (input.substring(i, i + oldString.length()).equals(oldString)) {
                count++;
            }
        }
        //System.out.println("===================");
        return count;

    }

    private static String changeInput(String input, int i, String oldString, String newString) {
        int a = Util.ordinalIndexOf(input, oldString, i);
        String result = input.substring(0, a) + newString + input.substring(a + newString.length(), input.length());
        //System.out.println(input+"----"+result);
        return result;
    }

    private static boolean autoSkip(String input) {
        //---- je (1), (2)...etc ----//
        for (int i = 0; i < 30; i++) {
            if (input.equals("(" + i + ")")) {
                return true;
            }
        }
        //-----<foot></foot>
        if (input.contains("<foot>") || input.contains("</foot>")) {
            return true;
        }
        //---- jsou pouze čísla,...etc ----//
        boolean check = true;
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                continue;
            } else {
                check = false;
                break;
            }
        }
        if (check) {
            return true;
        }
        //---- č. Čl. ...etc ----//
        if (input.equals("č") || input.equals("čl")) {
            return true;
        }
        //---- 19/1990 cisla-lomitko-cisla ...etc ----//
        check = false;
        for (char c : input.toCharArray()) {
            if ((c + "").equals("/") && !check) {
                //System.out.println("NUMER: " + input);
                check = true;
            } else if ((c + "").equals("/") && check) {
                check = false;
                break;
            }
            if (!Character.isDigit(c) && !(c + "").equals("/")) {
                check = false;
                break;
            }
        }
        if (check) {
            return true;
        }
        //---- Sb. ...etc ----//
        //System.out.println("input: "+input);
        if (input.contains("sb.") && input.length() < 7) {
            return true;
        }
        //---- a) b) c) ...etc ----//
        for (int i = 97; i < 123; i++) {
            if (input.equals(((char) i) + ")")) {
                //System.out.println("input: " + input);
                return true;
            }
        }

        return false;
    }

    private static String cleanInput(String input, boolean start, boolean end) {
        String bannedChars[] = {"“.", " ", ".", ",", "\"", ":", ";", "(", ")", "„", "“", "-", ""};
        for (String s : bannedChars) {
            if (end && input.endsWith(s)) {
                input = input.substring(0, input.length() - s.length());
            }
            if (start && input.startsWith(s)) {
                input = input.substring(s.length(), input.length());
            }
        }
        if (input.endsWith("-li")) {

            input = input.replace("-li", "");
        }
        input = input.toLowerCase();
        return input;
    }

    private static HashMap checkRow(String s) {
        HashMap result = new HashMap();
        if (s.contains("~") && s.length() < 3) {
            System.out.println("ADDED");
            result.put("color", Color.green);
            result.put("add", true);
            result.put("append", false);
            return result;
        }
        int count = StringUtils.countMatches(s, " ") + StringUtils.countMatches(s, ".") + StringUtils.countMatches(s, "*");
        int VAcount = StringUtils.countMatches(s.toLowerCase(), "v") + StringUtils.countMatches(s.toLowerCase(), "a");
        if (s.length() < 11 && s.length() > 4 && count > 1 && VAcount > 2) {
            result.put("color", Color.green);
            result.put("add", true);
            result.put("append", true);
            //System.out.println("HHHHH");
            return result;
        }
        if (s.length() < 4 && !s.startsWith("§") && !Util.onlyNumbers(s)) {
            result.put("color", Color.green);
            result.put("add", true);
            result.put("append", true);
            return result;
        }
        if (s.length() < 10 && !s.startsWith("§")
                && !s.startsWith("Čl.")
                && !s.startsWith("Článek")) {
            result.put("color", Color.green);
            result.put("add", true);
            result.put("append", true);
            return result;
        }

        result.put("add", false);
        return result;

    }

    private static void addRow(JTextPane finish, String s, HashMap h) {
        StyledDocument doc = finish.getStyledDocument();
        Style style = finish.addStyle("I'm a Style", null);
        StyleConstants.setBackground(style, (Color) h.get("color"));
        try {
            doc.insertString(doc.getLength(), s, style);
        } catch (BadLocationException e) {
        }

    }

    private static String[] connectRelevantLines(String[] lines) {
        List<Integer> remove = new ArrayList();
        //System.out.println("start lines.length: " + lines.length);
        for (int i = 1; i < lines.length; i++) {
            String[] prev = lines[i - 1].split(" ");
            String[] next = lines[i].split(" ");
            String prevString = cleanInput(prev[prev.length - 1], true, false);
            String nextString = cleanInput(next[0], false, true);
            boolean prevB = isInDict(prevString);
            boolean nextB = isInDict(nextString);
            boolean merged = isInDict(prevString + nextString);
            if (autoSkip(next[0]) || autoSkip(prev[prev.length - 1])) {
                continue;
            }

            if (!prevB
                    && !nextB
                    && merged) {
                lines[i - 1] = lines[i - 1] + lines[i];
                remove.add(i);
                //System.out.println("prevString= "+ prevString +nextString + " =nextString" );
                //System.out.println("REMOVE");
            }
            if (((!prevB && nextB) || (prevB && !nextB)) && merged) {
                lines[i - 1] = lines[i - 1] + lines[i];
                remove.add(i);
            }
        }
        for (int i = remove.size() - 1; i >= 0; i--) {
            lines = ArrayUtils.removeElement(lines, lines[remove.get(i)]);
        }
        //System.out.println("end lines.length: " + lines.length);

        return lines;
    }

    /**
     * Simple TRUE/FALSE method to check, if word is in dictionary
     *
     * @param input Input word
     * @return
     */
    private static boolean isInDict(String input) {
        for (String dict : words) {
            if (dict.equals(input)) {
                return true;
            }
        }
        return false;
    }

    private static boolean testlanguage(String s) {
        int wrong = 0;
        int good = 0;
        int tested = 0;
        boolean check = false;
        for (String word : s.split("\\s")) {
            check = false;
            word = cleanInput(word, true, true);
            for (String dict : words) {
                if (dict.equals(word)) {
                    //System.out.println("word: " + word);
                    check = true;
                    break;
                }
            }
            tested++;
            if (check) {

                good++;
            } else {
                wrong++;
            }
            if (tested > 50) {
                if (good > 30) {
                    //System.out.println("tested: "+ tested+ " - " + good);
                    //System.out.println("czech");
                    return true;
                } else {
                    return false;
                }

            }

        }
        double d = (double) good / (double) tested;
        //System.out.println("d: " + d);
        return d > 0.5;

    }

    private static String[] deleteEmptyLines(String[] lines) {
        for (String line : lines) {
            if (line.equals(" ") || line.length() == 0) {
                lines = ArrayUtils.removeElement(lines, line);
            }
        }
        return lines;
    }

}
