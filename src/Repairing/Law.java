/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repairing;

import Utils.Util;
import java.io.File;
import javax.swing.JTextPane;

/**
 *
 * @author Martin
 */
public class Law {

    String text;
    public JTextPane repair;
    String txtPath;

    boolean ready;

    public Law(String text) {
        this.text = text;
        repair = new JTextPane();
        ready = false;

        getRepair();
        System.out.println("----------");
    }

    public Law(String text, String path) {
        this.text = text;
        this.txtPath = path;
        repair = new JTextPane();
        ready = false;

        getRepair();
        System.out.println("----------");
    }

    private void getRepair() {
        appplyRules();
        Dictionary.setColoredText(text, repair);
    }

    private void appplyRules() {

    }

    public void saveLaw(String text) {
        String path = txtPath;
        String savePath = path.substring(0, path.lastIndexOf("/zakon_") + 1) + "/final/";
        String addit = path.substring(path.lastIndexOf("/"));

        File f = new File(path.substring(0, path.lastIndexOf("/zakon_") + 1) + "/final/");
        if (!f.exists()) {
            f.mkdir();
        }
        System.out.println(path.substring(0, path.lastIndexOf("/zakon_") + 1) + "/final/" + addit);
        Util.saveTXT(path, text);
        Util.saveTXT(path.substring(0, path.lastIndexOf("/zakon_") + 1) + "/final/" + addit, text);
    }

}
