/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repairing;

import java.util.HashMap;

/**
 *
 * @author Martin
 */
class RepairFootnote {

    final static String FOOTER_SEPARATOR = "Poznámky pod čarou:";
    final static String[] footers = {"*)", "“)", "°"};

    /**
     * Hlavni metoda, pokusi se orpavit poznamky pod carou
     *
     * @param str
     * @return
     */
    static String repair(String str) {
        if (!str.contains(FOOTER_SEPARATOR)) {
            return str;
        }
        String newFoot = newFooter(str);
        return newFoot;
    }

    private static String newFooter(String str) {
        int result = 0;
        String footer = str.substring(str.lastIndexOf(FOOTER_SEPARATOR));
        String oldFooter = footer;
        String text = str.substring(0, str.lastIndexOf(FOOTER_SEPARATOR));
        String newFooter = "";
        int index = 1;
        for (int i = 2; i < footer.length(); i++) {
            HashMap foot = new HashMap();
            if (!(footer.charAt(i) == ')')) {
                continue;
            }
            String footPart = footer.substring(i - 4, i + 1);
            foot = isFootnote(footPart);

            if ((boolean) foot.get("is") && toNextNewLine(footer, i) > 5) {
                if (footPart.contains("\n")) {
                    footer = footer.substring(0, i - (index + "").length()) + "\n" + index + footer.substring(i);
                    i++;
                    index++;
                } else {
                    footer = footer.substring(0, i - (index + "").length()) + "\n" + index + footer.substring(i);
                    i++;
                    index++;
                }
            }
        }
        footer = footer.replace("\n\n", "\n");
        return text + footer + "\n" + oldFooter;

    }

    private static HashMap isFootnote(String test) {
        HashMap result = new HashMap();
        result.put("is", false);
        for (int i = test.length() - 2; i >= 0; i--) {
            if (test.charAt(i) == (char) 10
                    || test.charAt(i) == (char) 32
                    || (test.charAt(i) == '.' && !test.contains("Sb.)"))) {
                result.put("is", true);
                System.out.println("test:" + test);

                System.out.println("test return: " + test.substring(i + 1, test.length()));
                result.put("word", test.substring(i + 1, test.length()));
                return result;
            }
            if (Character.isLetter(test.charAt(i))) {

                return result;
            }
        }
        return result;
    }

    private static int toNextNewLine(String footer, int index) {
        for (int i = index;i <footer.length();i++) {
            if (footer.charAt(i) == (int) 10)  {
                return i-index;
            }
        }
        return 1000;
    }

}
