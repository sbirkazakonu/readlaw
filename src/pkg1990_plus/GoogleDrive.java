/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus;

import Read.ToString;
import Read.ToText;
import Utils.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
class GoogleDrive {

    public static void start() {
        List<String> paths = getPaths();
        while (paths.size() > 0) {
            System.out.println("paths.size(): " + paths.size());
            if (paths.size() < 10) {
                System.out.println("!!!!");
                System.out.println("paths: " + paths);
                Thread t = runSingleThread(paths, "");
                t.start();
                try {
                    t.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GoogleDrive.class.getName()).log(Level.SEVERE, null, ex);
                }
                paths = getPaths();
                return;
            }
            int numberOfThreads = 10;
            int count = paths.size() / numberOfThreads;
            Thread t[] = new Thread[numberOfThreads];
            for (int i = 0; i < t.length; i++) {
                int start = i > 0 ? i * count + 1 : 0;
                int end = i != t.length - 1 ? (i + 1) * count : paths.size();
                List<String> list = paths.subList(start, end);
                t[i] = runSingleThread(list, "" + i);
            }

            for (Thread one : t) {
                one.start();
            }
            for (Thread one : t) {
                try {
                    one.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GoogleDrive.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            paths = getPaths();
            System.out.println("----------------------- NEW ROUND TRANSLATE ----------------");
        }
        System.out.println("FINISHED TRANSLATE");

    }

    private static List<String> getPaths() {
        List<String> paths = Util.readAllFilePathsSubfolders("/Users/Martin/Documents/zakony/PDF", new ArrayList());
        List<String> result = new ArrayList();
        for (String s : paths) {
            String pom = s.substring(s.lastIndexOf("/") + 1);
            if (pom.contains("_")
                    && !pom.contains("Store")
                    && !pom.contains(".txt")
                    && !s.contains("/split/")
                    && !pom.contains(".docx")
                    && !isTranslated(paths, s)) {
                result.add(s);
            }

        }

        return result;
    }

    private static Thread runSingleThread(List<String> files, String index) {
        ToText texter = new ToText();
        Runnable r = () -> {
            int progress = 0;
            for (String s : files) {
                progress++;
                synchronized (texter) {
                    texter.readAndMoveBack(s, index);
                }
                System.out.println("PROGRESS THREAD " + index + ": " + progress + "/" + files.size());
            }
        };
        Thread t = new Thread(r);
        return t;
    }

    private static boolean isTranslated(List<String> paths, String s) {
        if (s.contains(".txt")) {
            return false;
        }
        s = s + ".txt";
        for (String path : paths) {
            if (path.equals(s)) {
                return true;
            }
        }
        return false;
    }

}
