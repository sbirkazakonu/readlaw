/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus;

import Frames.CheckDriveCorrect;
import Objects.Part;
import Read.FinishLaw;
import Repairing.Dictionary;
import Frames.ReadingFrame;
import Frames.Repair;
import Read.Space_OCR;
import Utils.Util;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import pkg1990_plus.checkSeparate.Check;

/**
 *
 * @author Martin
 */
public class HandlerFrame extends JFrame {

    JButton checkSeparate, drive, repairBroken, repairLaws, doubleCheck, testing;

    Container center;

    GridLayout main;

    public HandlerFrame() {
        createFrame();
        addListeners();
    }

    private void declarations() {
        checkSeparate = new JButton("checkSeparate");
        drive = new JButton("Google Drive");
        repairBroken = new JButton("repairBroken");
        repairLaws = new JButton("repairLaws");
        doubleCheck = new JButton("Double check");
        testing = new JButton("Testin double new Line");
        main = new GridLayout(1, 0);
        center = new Container();

    }

    private void addListeners() {
        drive.addActionListener((ActionEvent e) -> {
            Runnable r = () -> {
                GoogleDrive.start();
                System.out.println("CHECKING");
                DriveCheck.startCheck();
            };
            Thread t = new Thread(r);
            t.start();
        });
        checkSeparate.addActionListener((ActionEvent e) -> {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty(
                    "com.apple.mrj.application.apple.menu.about.name", "Main");
            Check ch = new Check();
        });
        repairBroken.addActionListener((ActionEvent e) -> {
            String folderPath = "/Users/Martin/Documents/zakony/PDF/1990/";
            List<String> files = Util.readFilesInSubfolderCondition(folderPath, ".txt",new ArrayList());
            System.out.println("All Files: "+ files.size());
            files = unFinishedFiles(files);
            System.out.println("reduced finished files: "+files.size());
            List<Part> parts = FinishLaw.getRepairFiles(files);
            parts = cleanParts(parts);
            ReadingFrame f = new ReadingFrame(parts);

        });
        repairLaws.addActionListener((ActionEvent e) -> {
            Dictionary.readWords();
            String folderPath = "/Users/Martin/Documents/zakony/PDF/1990/";
            FinishLaw.completeLaw(folderPath);
            Repair r = new Repair(folderPath);
        });
        doubleCheck.addActionListener((ActionEvent e) -> {
            CheckDriveCorrect c = new CheckDriveCorrect();

        });
        testing.addActionListener((ActionEvent e) -> {
            List<String> paths = FinishLaw.getPartsWithDoubleLine();
            spaceOCR(paths);
        });
    }

    private void createFrame() {
        declarations();
        settings();
        layouts();
        this.setVisible(true);
    }

    private void settings() {
        this.setSize(530, 80);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("MainHandler");
        this.setLayout(null);

        center.setBounds(20, 10, this.getWidth() - 60, this.getHeight() - 40);

        center.setLayout(main);

        this.add(center);

        main.setHgap(15);
        main.setVgap(15);

    }

    private void layouts() {

        center.add(checkSeparate);
        center.add(drive);
        center.add(doubleCheck);
        center.add(repairBroken);
        center.add(repairLaws);
        center.add(testing);

    }

//    private void checkDiff() {
//        List<String> paths = Util.readAllFilePathsSubfolders("/Users/Martin/Documents/zakony/PDF/", new ArrayList());
//        int index = 0;
//        List<Part> parts = new ArrayList();
//        for (String s : paths) {
//            if (s.contains("/check/") && !s.contains("DS_")) {
//                String check = Util.readTXT(s);
//                String orig = Util.readTXT(s.replaceAll("/check", ""));
//                String pngPath = s.replaceAll("/check", "").replaceAll(".txt", "");
//                String txtPath = s.replaceAll("/check", "");
//
//                if (!check.equals(orig)) {
//                    parts.add(new Part(pngPath, txtPath));
//                }
//
//            }
//        }
//        ReadingFrame f = new ReadingFrame(parts);
//    }

    private List<Part> cleanParts(List<Part> parts) {
        for (Part p : parts) {
            if ((p.repairText.startsWith("Částka") || p.repairText.startsWith("Strana"))
                    && p.repairText.contains("Sbírka zákonů")
                    && p.repairText.length() < 80) {
                p.repairText = "";
            }
        }
        return parts;
    }

    private List<String> unFinishedFiles(List<String> files) {
        System.out.println(files.size());
        List<String> pom = new ArrayList();
        for (String s : files) {
            if (s.contains("/check/")) {
                continue;
            }
            pom.add(s);
        }
        files = pom;
        System.out.println(files.size());

        pom = null;
        List<String> finished = getFinished(files);
        return finished;
    }

    private List<String> getFinished(List<String> files) {
        List<String> result = new ArrayList();
        for (String path : files) {
            //System.out.println("path: " + path);
            String pom = path.substring(0, path.lastIndexOf("/page/")+1);
            List<String> laws = Util.readAllFilePathsCondition(pom, "zakon_");
            List<String> lawsFinal = Util.readAllFilePathsCondition(pom+"final/", "zakon_");
            //System.out.println("pom: "+ pom);
            
            //System.out.println("laws: "+ laws.size());
            //System.out.println("lawsFinal: " + lawsFinal.size());
            if (laws.size()>0
                    && lawsFinal.size()>0
                    && laws.size() == lawsFinal.size()) {
                //pom = pom.substring(pom.lastIndexOf("/") + 1);
                continue;
            }
            pom = pom.substring(pom.lastIndexOf("/") + 1);
            result.add(path);
            //System.out.println("NOT FINISHED pom: " + pom);
        }
        return result;
    }

    private void spaceOCR(List<String> paths) {
        int index = 0;
        for (String path: paths) {
            path=path.replaceAll(".txt", "");
            //System.out.println("p: "+path.substring(0,path.lastIndexOf("/")));
            File f= new File(path.substring(0,path.lastIndexOf("/")+1).replaceAll("/page/", "/space/"));
            if (!f.exists()) f.mkdir();
            System.out.println("OCR path: "+path);
            
            File fil = new File(path.replace("/page/", "/space/") + ".txt");
            if (fil.exists()) continue;
            
            String ocr = Space_OCR.spaceOCR(path);
            ocr = ocr.replaceAll("- \\\\r\\\\n", "");
            Util.saveTXT(path.replace("/page/", "/space/") + ".txt", ocr);
            index++;
            System.out.println("index: "+index );
            if (index > 50) {
                System.exit(1);
                break;
                
            }
        }
    }
}
