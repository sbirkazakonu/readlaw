package pkg1990_plus.checkSeparate;

import Utils.Util;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JFileChooser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Martin
 */
public class FrameUtils {

    static String selectFolder() {
        JFileChooser chooser = new JFileChooser("/Users/Martin/Documents/zakony/PDF/");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int choose = chooser.showOpenDialog(null);
        if (choose == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile().getPath();
        } else {
            return "";
        }
    }

    static List<String> getPagesPaths(String path) {
        List<String> all = Util.readAllFilePathsSubfolders(path, new ArrayList());
        List<String> result = new ArrayList();
        String test = "/page/";
        for (String s : all) {
            if (s.contains(".png")
                    && !s.substring(s.lastIndexOf(test)).contains("_")) {
                result.add(s);
            }

        }
        result = sort(result);
        for (String s: result ) {
            System.out.println("s: "+ s);
        }
        return result;

    }

    public static List<Rectangle> getRectangles(BufferedImage img) {
        Rectangle r = getCenter(img);
        int height = 44;
        int width = 40;
        boolean verti[] = getVertical(img, r.x);
        int pixels[][] = Util.getPixelArray(img);
        List<Point> vertical = convertToDimension(verti);
        vertical = eliminateWrong(vertical, img, r.x);
        vertical = streamLine(vertical, img);
        List<Rectangle> rects = new ArrayList();
        for (int i = 0; i < vertical.size(); i++) {
            if (i == 0) {
                Rectangle tmp = new Rectangle(0, 0, img.getWidth(), vertical.get(i).x > 5 ? vertical.get(i).x : 5);
                if (!(tmp.x + tmp.height < 180)) {
                    rects.add(tmp);
                }
            }
            if (i != 0) {
                Rectangle tmp = new Rectangle(0, vertical.get(i - 1).y, img.getWidth(), vertical.get(i).x - vertical.get(i - 1).y);

                rects.add(tmp);
            }
            Rectangle temp = new Rectangle(0, vertical.get(i).x,
                    r.x, vertical.get(i).y - vertical.get(i).x);
            Rectangle temp2 = new Rectangle(r.x, vertical.get(i).x,
                    img.getWidth() - r.x, vertical.get(i).y - vertical.get(i).x);
            rects.add(temp);
            rects.add(temp2);

            if (i == vertical.size() - 1) {
                Rectangle tmp = new Rectangle(0, vertical.get(i).y, img.getWidth(), img.getHeight() - vertical.get(i).y);
                rects.add(tmp);
            }
        }

        //mezi blbostma
        return rects;
    }

    public static Rectangle getCenter(BufferedImage img) {
        int horiz = img.getWidth() / 2;
        return new Rectangle(horiz, 0, 3, img.getHeight());
    }

    private static boolean[] getVertical(BufferedImage img, int horiz) {
        int height = 44;
        int width = 40;
        int pixels[][] = Util.getPixelArray(img);
        boolean test[] = new boolean[img.getHeight() / height];
        for (int i = 0; i < test.length; i++) {
            test[i] = true;
            int bestCount = height * width;
            for (int j = 0; j < width; j++) {
                int count = 0;
                for (int k = 0; k < height; k++) {
                    for (int l = 0; l < width; l++) {
                        if (pixels[i * height + k][horiz - j + l] != -1) {
                            count++;
                        }
                    }
                }
                if (count < bestCount) {
                    bestCount = count;
                }

            }
            if (bestCount > 5) {
                test[i] = false;
            }

        }

        test = eliminateWhiteSpaces(img, test);
        test = getOnlyLong(test);
        //System.out.println(Arrays.toString(test));
        return test;
    }

    private static boolean[] getOnlyLong(boolean[] test) {
        for (int i = 0; i < test.length; i++) {
            if (i < test.length - 1 && test[i] && test[i + 1]) {

            } else if (i > 0 && test[i - 1] && test[i]) {
            } else {
                test[i] = false;
            }
        }
        return test;
    }

    private static boolean[] eliminateWhiteSpaces(BufferedImage img, boolean[] test) {
        int height = 44;
        int width = 40;
        int pixels[][] = Util.getPixelArray(img);
        boolean result[] = new boolean[test.length];
        int count = 0;
        for (int i = 0; i < result.length; i++) {
            result[i] = true;
            for (int j = 0; j < height; j++) {
                for (int k = 0; k < img.getWidth(); k++) {
                    if (pixels[j + i * height][i] != -1) {
                        count++;
                    }
                    if (count > 20) {
                        result[i] = false;
                        break;
                    }
                }
                if (!result[i]) {
                    break;
                }
            }
        }
        for (int i = 1; i < result.length; i++) {
            if (!result[i] && result[i - 1]) {
                result[i] = false;
            }
            if (result[i] && !result[i - 1]) {
                result[i] = false;
            }
        }
        for (int i = 0; i < test.length; i++) {
            if (test[i] && !result[i]) {
                test[i] = false;
            }
        }
        return test;
    }

    private static List<Point> convertToDimension(boolean[] verti) {
        int height = 44;
        int width = 40;
        List<Point> result = new ArrayList();
        Point p = new Point(0, 0);
        for (int i = 0; i < verti.length - 1; i++) {
            if (verti[i] && !verti[i + 1] || i == verti.length - 2) {
                p.y = i * height;
                result.add(p);
            } else if (!verti[i] && verti[i + 1]) {
                p = new Point(0, 0);
                p.x = (i + 1) * height;
            }

        }
        //System.out.println("result: " + result);
        return result;
    }

    private static List<Point> streamLine(List<Point> vertical, BufferedImage img) {
        int pixels[][] = Util.getPixelArray(img);
        for (int i = 0; i < vertical.size(); i++) {
            Point p = vertical.get(i);
            for (int k = 0; true; k++) {
                boolean up = true;
                boolean down = true;
                for (int j = 0; j < pixels[0].length; j++) {
                    if (p.x + k == pixels[0].length) {
                        break;
                    }
                    if (pixels[p.x + k][j] != -1) {
                        down = false;
                    }
                    if (p.x - k == 0) {
                        break;
                    }
                    if (pixels[p.x - k][j] != -1) {
                        up = false;
                    }

                    if (!down && !up) {
                        break;
                    }
                }
                if (down) {
                    vertical.get(i).x = p.x + k;
                    break;
                }
                if (up) {
                    vertical.get(i).x = p.x - k;
                    break;
                }
            }
            for (int k = 0; true; k++) {
                boolean up = true;
                boolean down = true;
                for (int j = 0; j < pixels[0].length; j++) {
                    if (p.y + k == pixels[0].length - 1) {
                        break;
                    }
                    if (pixels[p.y + k][j] != -1) {
                        down = false;
                    }
                    if (p.y - k == 0) {
                        break;
                    }
                    if (pixels[p.y - k][j] != -1) {
                        up = false;
                    }

                    if (!down && !up) {
                        break;
                    }
                }
                if (down) {
                    vertical.get(i).y = p.y + k;
                    break;
                }
                if (up) {
                    vertical.get(i).y = p.y - k;
                    break;
                }
            }
        }

        return vertical;
    }

    private static List<Point> eliminateWrong(List<Point> vertical, BufferedImage img, int hor) {
        int pixels[][] = Util.getPixelArray(img);
        List<Point> result = new ArrayList();
        for (Point p : vertical) {
            int count = 0;
            for (int i = p.x; i < p.y; i++) {
                for (int j = hor; j < pixels[0].length; j++) {
                    if (pixels[i][j] != -1) {
                        count++;
                    }
                }

            }
            if (count > (p.y - p.x)) {
                result.add(p);
            }
        }
        return result;
    }

    public static void heapStats() {
        int mb = 1024 * 1024;

        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();

        System.out.println("##### Heap utilization statistics [MB] #####");

        //Print used memory
        System.out.println("Used Memory:"
                + (runtime.totalMemory() - runtime.freeMemory()) / mb);

        //Print free memory
        System.out.println("Free Memory:"
                + runtime.freeMemory() / mb);

        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb);

        //Print Maximum available memory
        System.out.println("Max Memory:" + runtime.maxMemory() / mb);

    }

    private static List<String> sort(List<String> result) {

        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                //System.out.println("o1: "+o1);
                String pom1 = o1.substring(o1.lastIndexOf("/PDF/") + "/PDF/".length(), o1.lastIndexOf("/page/"));

                pom1 = pom1.substring(0, pom1.lastIndexOf("/"));

                String pom2 = o2.substring(o2.lastIndexOf("/PDF/") + "/PDF/".length(), o2.lastIndexOf("/page/"));
                pom2 = pom2.substring(0, pom2.lastIndexOf("/"));
                int year1 = Integer.parseInt(pom1);
                int year2 = Integer.parseInt(pom2);
                //System.out.println("pom1: " + pom1);
                //System.out.println("pom2: " + pom2);
                if (year1 != year2) {
                    return new Integer(year1).compareTo(year2);
                }
                pom1 = o1.substring(0, o1.lastIndexOf("/page/"));
                pom1 = pom1.substring(pom1.lastIndexOf("/") + 1, pom1.lastIndexOf("_"));
                pom2 = o2.substring(0, o2.lastIndexOf("/page/"));;
                pom2 = pom2.substring(pom2.lastIndexOf("/") + 1, pom2.lastIndexOf("_"));
                int nr1 = Integer.parseInt(pom1);
                int nr2 = Integer.parseInt(pom2);
                //System.out.println("pom1: " + pom1);
                //System.out.println("pom2: " + pom2);
                if (nr1 != nr2) {
                    return new Integer(nr1).compareTo(nr2);
                }
                pom1 = o1.substring(o1.lastIndexOf("/page") + "/page".length(), o1.lastIndexOf(".png"));
                pom2 = o2.substring(o2.lastIndexOf("/page") + "/page".length(), o2.lastIndexOf(".png"));
                //System.out.println("pom1: "+ pom1);
                //System.out.println("o1: "+o1);
                int page1 = Integer.parseInt(pom1);
                int page2 = Integer.parseInt(pom2);
                //System.out.println("pom1: " + pom1);
                //System.out.println("pom2: " + pom2);

                return new Integer(page1).compareTo(page2);

            }
        };

        Collections.sort(result, comp);
        return result;
    }
}
