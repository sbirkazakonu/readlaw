/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus.checkSeparate;

import Objects.Line;
import Objects.Page;
import Utils.Util;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Martin
 */
public class Panel extends JPanel {

    List<String> paths;
    Page imgs[];
    int index;
    int color = 0;
    BufferedImage pic, pix2;
    boolean deleteMode, modifyMode, mergeMode, newLawMode, footer, prilohaMode;
    boolean additionalRepairMode;

    public Panel() {
        paths = new ArrayList();
        this.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                BufferedImage scaledImg = Util.scale(imgs[index].imgLines, BufferedImage.TYPE_3BYTE_BGR, getHeight());
                double scaleFactor = (double) scaledImg.getWidth() / (double) imgs[index].img.getWidth();

                int originalX = (int) (x / scaleFactor);
                int originalY = (int) (y / scaleFactor) - 55;
                if (footer) {
                    //find that rect
                    for (Rectangle rect : imgs[index].rects) {
                        if (rect.contains(new Point(originalX, originalY))) {
                            imgs[index].footer
                                    .add(rect);
                            break;
                        }
                    }
                    System.out.println("FOOTER CLICKED");

                } else if (prilohaMode) {
                    for (Rectangle rect : imgs[index].rects) {
                        if (rect.contains(new Point(originalX, originalY))) {
                            imgs[index].priloha
                                    .add(rect);
                            break;
                        }
                    }
                    System.out.println("PRILOHA CLICKED");

                } else if (additionalRepairMode) {
                    //find that rect
                    for (Rectangle rect : imgs[index].rects) {
                        if (rect.contains(new Point(originalX, originalY))) {
                            imgs[index].additRepair
                                    .add(rect);
                            break;
                        }
                    }
                    System.out.println("REPAIR CLICKED");

                } else if (deleteMode) {
                    //imgs[index].deleteClicked(x,y);
                    deleteClicked(originalX, originalY);
                    imgs[index].paintRectangles();
                    refresh();

                } else if (mergeMode) {
                    mergeRectangles(originalX, originalY);
                    imgs[index].paintRectangles();
                    refresh();
                } else {
                    //System.out.println("rectNumber" + imgs[index].rects.size());

                    imgs[index].addRectangle(originalX, originalY, newLawMode);
                    imgs[index].paintRectangles();
                    refresh();
                    //System.out.println(originalX + ", " + originalY);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

    }

    public Panel(Color c) {
        this.setBackground(c);
    }

    public void setPic(BufferedImage pic) {
        //this.setBorder(new EmptyBorder(10, 10, 10, 10));
        this.pic = pic;
        repaint();
    }

    public void setBg() {
        color = color + 10000;
        this.setBackground(new Color(color));
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(pic, 10, 10, this);
        if (pix2 != null) {
            g.drawImage(pix2, (int) (this.getWidth() * 0.6), 10, this);

        }

    }

    void setPic(BufferedImage pic, BufferedImage pic2) {
        this.setBorder(new EmptyBorder(10, 10, 10, 10));
        this.pic = pic;
        this.pix2 = pic2;
        repaint();
    }

    public void setPathsList(List<String> list) {
        imgs = new Page[list.size()];
        this.paths = list;
        index = getStartNumber();
        System.out.println("START INDEX: " + index + "/" + paths.size());
        loadFirst();

    }

    public void moveLeft() {
        if (index > 0 && imgs[index - 1] != null) {
            index--;
            BufferedImage scaled = Util.scale(imgs[index].imgLines, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
            this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
            this.revalidate();
            this.setPic(scaled);
            readNext();
            System.out.println("Current Path: " + imgs[index].path);
        }
    }

    public void moveRight() {

        if (index <= imgs.length - 1 && imgs[index + 1] != null) {
            index++;
            BufferedImage scaled = Util.scale(imgs[index].imgLines, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
            this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
            this.revalidate();
            this.setPic(scaled);
            checkCastkaChange();
            readNext();
            System.out.println("Current Path: " + imgs[index].path);
        }

    }

    public void refresh() {
        BufferedImage scaled = Util.scale(imgs[index].imgLines, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
        this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
        this.revalidate();
        this.setPic(scaled);
    }

    private void loadFirst() {

        Runnable r = new Runnable() {

            @Override
            public void run() {
                for (int i = index; i < index + 5 && i < paths.size(); i++) {
                    //BufferedImage imgs = Util.readImage(paths.get(i));
                    Page p = new Page(paths.get(i));
                    //System.out.println("Read: " + i + " " + paths.get(i));
                    imgs[i] = p;
                }
                System.out.println("FirstLoad: Done!");
            }
        };
        Thread t0 = new Thread(r);
        t0.start();

    }

    private void readNext() {
        Runnable r = new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < imgs.length; i++) {
                    if (imgs[i] == null && i < index + 5 && i > index - 5) {
                        //System.out.println("i: " + i + index);
                        Page p = new Page(paths.get(i));
                        //System.out.println("Read: " + i + " " + paths.get(i));
                        imgs[i] = p;
                    }
                    if (i < index + 5 && i > index - 5) {
                    } else {
                        imgs[i] = null;
                    }
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    public Page getCurrent() {
        return imgs[index];
    }

    public void setCurrent(Page pag) {
        imgs[index] = pag;
    }

    public void setPureImg() {
        BufferedImage scaled = Util.scale(imgs[index].img, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
        this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
        this.revalidate();
        this.setPic(scaled);

    }

    public void setImgWithRects() {
        BufferedImage scaled = Util.scale(imgs[index].imgLines, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
        this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
        this.revalidate();
        this.setPic(scaled);
    }

    public void showOneRectangle() {
        imgs[index].imgRect = imgs[index].paintOneRect();
        BufferedImage scaled = Util.scale(imgs[index].imgRect, BufferedImage.TYPE_3BYTE_BGR, this.getHeight());
        this.setPreferredSize(new Dimension(scaled.getWidth() - 20, scaled.getHeight()));
        this.revalidate();
        this.setPic(scaled);

    }

    private int getStartNumber() {
        boolean check = true;

        for (int i = 0; i < paths.size(); i++) {
            String path = paths.get(i);
            //System.out.println("Path: " + path);
            String deletePath = path.substring(0, path.lastIndexOf("/page/") + 1) + "deleted.txt";
            //System.out.println("deletePath: " + deletePath);
            File f = new File(deletePath);
            if (f.exists()) {
                String text = Util.readTXT(deletePath);
                //System.out.println("text: " + text);
                if (text.contains(path)) {
                    continue;
                }

            }

            List<String> pths = Util.readAllFilePaths(path.substring(0, path.lastIndexOf("/")));
            String pr = path.substring(path.lastIndexOf("/")).replace(".png", "") + "_";
            int count = 0;
            for (String s : pths) {
                if (s.contains(pr)) {
                    count++;
                }
                if (count > 1) {
                    break;
                }
                String pom = s.substring(s.lastIndexOf("/"));
                //System.out.println("TEST: "+ pom);
            }
            if (count < 1) {
                //System.out.println("path: " + path);
                //System.out.println("RETURNED");
                return i;
            }
        }
        return 0;

    }

    private void deleteClicked(int x, int y) {
        Page page = imgs[index];
        List<Rectangle> list = page.rects;
        Point click = new Point(x, y);
        for (Iterator<Rectangle> iter = list.listIterator(); iter.hasNext();) {
            Rectangle r = iter.next();
            if (r.contains(click)) {
                System.out.println("REMOVED");
                iter.remove();
            }
        }
        page.rects = list;
    }

    private void mergeRectangles(int originalX, int originalY) {
        Point point = new Point(originalX, originalY);
        Rectangle clicked = null;
        int clickedIndex = 0;
        int i = 0;
        for (Rectangle r : imgs[index].rects) {
            if (r.contains(point)) {
                clicked = r;
                clickedIndex = i;
                break;
            }
            i++;
        }
        if (clicked == null) {
            System.out.println("NOT MERGED - NULL Pointer");
            return;
        }
        int middle = clicked.y + clicked.height / 2;
        boolean isUp = middle < point.y;
        if (isUp) {
            Rectangle next = imgs[index].rects.get(clickedIndex + 1);
            deleteClicked(next.x + 10, next.y + 10);
            clicked.height = clicked.height + next.height;
            System.out.println("isUP " + isUp);
        } else {
            Rectangle next = imgs[index].rects.get(clickedIndex - 1);
            deleteClicked(next.x + 10, next.y + 10);
            clicked.y = next.y;
            clicked.height = clicked.height + next.height;
            System.out.println("isUP " + isUp);
        }

    }

    /**
     * zkontroluje, jestli se změnila od minulého bodu částka sbírky, pokud ano,
     * vytvoří finální zákony
     */
    private void checkCastkaChange() {

    }

}
