/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus.checkSeparate;

import Objects.Page;
import Utils.Util;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

/**
 *
 * @author Martin
 */
public class Check extends JFrame implements KeyListener {

    final int FRAME_HEIGHT = 800;
    final int FRAME_WIDTH = 800;

    final String keyText = "<html>⌘ + Enter = save<br>⌘ + s = mergeMode<br>"
            + "⌘ + m = modifyMode<br>"
            + "⌘ + d = deleteMode<br>"
            + "⌘ + shift + w = no Save!<br>"
            + "⌘ + f = Footer!<br>"
            + "⌘ + b = Additional check!<br>"
            + "⌘ + a = Priloha jedna část!<br>"
            + "ModifyMode + BACK_SPACE = delete curretn Rectangle<br>"
            + "</html>";
    int move = 10;

    //List<BufferedImage> pages;
    List<String> paths;

    Panel p, empty, empty2, empty3;
    JButton handler, save, change;

    JScrollPane scrollMain;

    JMenuBar menuBar;
    JMenu fileMenu;
    JMenuItem select;

    JLabel keys;

    JTextPane done;

    Container left, right, rightTop, rightBottom;

    //boolean modifyMode;
    public Check() {
        declarations();
        setttings();
        layouts();
        components();
        actionListeners();
        startFolder("/Users/Martin/Documents/zakony/PDF/1990/");
        this.setVisible(true);
    }

    private void declarations() {
        menuBar = new JMenuBar();

        p = new Panel();
        empty = new Panel();
        empty2 = new Panel();
        empty3 = new Panel();

        left = new Container();
        right = new Container();
        rightTop = new Container();
        rightBottom = new Container();

        handler = new JButton("Handler");
        save = new JButton("Save");
        change = new JButton("Change");

        fileMenu = new JMenu("Top");
        select = new JMenuItem("Load Folder");

        done = new JTextPane();

        keys = new JLabel("", SwingConstants.CENTER);

        scrollMain = new JScrollPane(p);

    }

    private void setttings() {
        //Frame settings
        this.setSize(this.FRAME_WIDTH, this.FRAME_HEIGHT);
        this.setJMenuBar(menuBar);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setJMenuBar(menuBar);
        this.setTitle("Check Separation");

        //Panel settings
        p.setBackground(new Color(100, 100, 100));
        //Menu settings
        menuBar.add(fileMenu);
        fileMenu.add(select);

        scrollMain.setViewportView(p);
        handler.requestFocus();

        done.setContentType("text/html");

        keys.setText(keyText);
    }

    private void keyListeners() {

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (p.modifyMode && e.getKeyCode() == KeyEvent.VK_LEFT) {
            p.imgs[p.index].rectIndex--;
            System.out.println("modify left");
            p.showOneRectangle();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            p.moveLeft();
            setDone();
            System.out.println("left");
        }

        if (p.modifyMode && e.getKeyCode() == KeyEvent.VK_RIGHT) {
            p.imgs[p.index].rectIndex++;
            System.out.println("modify right");
            p.showOneRectangle();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            p.moveRight();
            setDone();
            System.out.println("right");
        }
        // zmensonvani, zvetsovani 
        if (p.modifyMode && e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_UP) {
            if (move < 100) {
                move = move + 2;
            }

        } else if (p.modifyMode && e.getKeyCode() == KeyEvent.VK_UP) {
            Page page = p.imgs[p.index];
            Rectangle r = page.rects.get(page.rectIndex);
            r.height = r.height - move;
            p.showOneRectangle();
        }
        if (p.modifyMode && e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (move > 1) {
                move = move - 2;
            }

        } else if (p.modifyMode && e.getKeyCode() == KeyEvent.VK_DOWN) {
            Page page = p.imgs[p.index];
            Rectangle r = page.rects.get(page.rectIndex);
            r.height = r.height + move < page.img.getHeight() ? r.height + move : r.height;
            p.showOneRectangle();
        }
        if (p.modifyMode && e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_UP) {
            Page page = p.imgs[p.index];
            Rectangle r = page.rects.get(page.rectIndex);
            r.y = r.y - move > 0 ? r.y - move : r.y;
            p.showOneRectangle();
        }
        if (p.modifyMode && e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_DOWN) {
            Page page = p.imgs[p.index];
            Rectangle r = page.rects.get(page.rectIndex);
            r.y = r.y + r.height + move < page.img.getHeight() ? r.y + move : r.y;
            p.showOneRectangle();
        }
        //smazat
        if (p.modifyMode && e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            Page page = p.imgs[p.index];
            List<Rectangle> list = page.rects;
            Rectangle currentRect = page.rects.get(page.rectIndex);
            for (Iterator<Rectangle> iter = list.listIterator(); iter.hasNext();) {
                Rectangle r = iter.next();
                if (r.x == currentRect.x
                        && r.y == currentRect.y
                        && r.width == currentRect.width
                        && r.height == currentRect.height) {
                    iter.remove();
                }
            }
            page.rects = list;
        }
        if (!p.modifyMode && e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            p.imgs[p.index].rects = new ArrayList();
            p.imgs[p.index].paintRectangles();
            p.refresh();
            System.out.println("==");
        }

        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {

            p.imgs[p.index].saveParts();
            p.deleteMode = false;
            p.modifyMode = false;
            p.additionalRepairMode = false;
            p.footer = false;
            p.newLawMode = false;
            p.mergeMode = false;
            p.prilohaMode = false;
            p.moveRight();
            setDone();

        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_P) {
            p.imgs[p.index].savePNG();
            p.moveRight();
            setDone();
        }

        if (e.isMetaDown() && e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_W) {
            p.imgs[p.index].save = !p.imgs[p.index].save;
            System.out.println("QUIT");

            done.setText(" SAVE =  " + p.imgs[p.index].save);
            setDone();
            p.imgs[p.index].saveParts();
            p.moveRight();
        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_D) {

            p.deleteMode = !p.deleteMode;
            done.setText(done.getText() + " delteMode " + p.deleteMode);
            setDone();

        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_A) {

            p.prilohaMode = !p.prilohaMode;
            done.setText(done.getText() + " prilohaMode " + p.prilohaMode);
            setDone();

        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_M) {
            if (p.modifyMode) {
                p.imgs[p.index].rectIndex = 0;
                move = 10;
                p.imgs[p.index].paintRectangles();
                p.modifyMode = false;
                
                p.setImgWithRects();

            } else {
                p.setPureImg();
                p.modifyMode = true;
            }
            setDone();

        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_S) {
            //mergeRectangle
            p.mergeMode = !p.mergeMode;
            if (p.mergeMode) {

                p.imgs[p.index].rectIndex = 0;
                move = 10;
                p.imgs[p.index].paintRectangles();
                p.modifyMode = false;
                p.prilohaMode = false;
                p.deleteMode = false;
                p.additionalRepairMode = false;
                p.newLawMode = false;
            }
            setDone();
        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_N) {
            p.newLawMode = !p.newLawMode;
            if (p.newLawMode) {
                p.modifyMode = false;
                p.prilohaMode = false;
                p.deleteMode = false;
                p.additionalRepairMode = false;
                p.mergeMode = false;
            }
            setDone();
        }

        //Na dalsi kontrolu
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_B) {
            p.additionalRepairMode = !p.additionalRepairMode;
            setDone();
        }
        //Footer
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_F) {
            p.footer = !p.footer;
            setDone();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void layouts() {
        this.setLayout(null);
        left.setBounds(0, 0, (int) (0.68 * FRAME_WIDTH), (int) ((int) FRAME_HEIGHT * 0.95));
        right.setBounds((int) (0.69 * FRAME_WIDTH), 0, (int) (0.3 * FRAME_WIDTH), (int) ((int) FRAME_HEIGHT * 0.95));

        GridLayout leftLayout = new GridLayout(1, 0);
        left.setLayout(leftLayout);

        GridLayout rightLayout = new GridLayout(0, 1);
        rightLayout.setVgap(20);
        right.setLayout(rightLayout);

        GridLayout rightBtm = new GridLayout(0, 1);
        rightBottom.setLayout(rightBtm);
        GridLayout rightTp = new GridLayout(0, 1);
        rightTop.setLayout(rightTp);

    }

    private void components() {
        left.add(scrollMain);
        right.add(rightTop);
        right.add(rightBottom);
        rightTop.add(keys);
        rightTop.add(done);
        rightBottom.add(empty);
        rightBottom.add(empty2);
        rightBottom.add(handler);
        rightBottom.add(save);
        rightBottom.add(change);

        this.add(left);
        this.add(right);
    }

    private void actionListeners() {
        select.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = FrameUtils.selectFolder();
                startFolder(path);
            }
        });

        handler.addKeyListener(this);
    }

    private void startFolder(String path) {
        paths = FrameUtils.getPagesPaths(path);
        p.setPathsList(paths);
        handler.requestFocus();
    }

    private void setDone() {
        String path = p.imgs[p.index].path;
        String deleted = path.substring(0, path.lastIndexOf("/page/"))+"/deleted.txt";
        List<String> paths = Util.readAllFilePaths(path.substring(0, path.lastIndexOf("/")));
        String pr = path.substring(path.lastIndexOf("/")).replace(".png", "") + "_";
        //System.out.println("pr: "+pr);
        int count = 0;
        for (String s : paths) {
            if (s.contains(pr)) {
                count++;
            }
            if (count >= 1 || Util.readTXT(deleted).contains(path)) {
                done.setText("<html>Done!<br>"
                        + "modify: " + p.modifyMode
                        + "<br>delete: " + p.deleteMode
                        + "<br>merge: " + p.mergeMode
                        + "<br>repair: " + p.additionalRepairMode
                        + "<br>footer: " + p.footer
                        + "<br>priloha: " + p.prilohaMode
                        + "<br>newLaw: " + p.newLawMode + "</html>");
                done.setBackground(Color.green);
                return;
            }
        }
        done.setText("<html>Not done!<br>"
                + "modify: " + p.modifyMode
                + "<br>delete: " + p.deleteMode
                + "<br>merge: " + p.mergeMode
                + "<br>repair: " + p.additionalRepairMode
                + "<br>footer: " + p.footer
                + "<br>priloha: " + p.prilohaMode
                + "<br>newLaw: " + p.newLawMode + "</html>");
        done.setBackground(Color.red);

    }

}
