/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus;

import Read.ToString;
import Read.ToText;
import Utils.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class DriveCheck {

    public static void startCheck() {
        List<String> paths = getPaths();

        while (paths.size() > 0) {

            System.out.println("paths.size(): " + paths.size());
            if (paths.size() < 10) {
                System.out.println("!!!");
                Thread t = runSingleThread(paths, "");
                t.start();

                return;
            }
            int numberOfThreads = 10;
            int count = paths.size() / numberOfThreads;
            Thread t[] = new Thread[numberOfThreads];
            for (int i = 0; i < t.length; i++) {
                int start = i > 0 ? i * count + 1 : 0;
                int end = i != t.length - 1 ? (i + 1) * count : paths.size();
                List<String> list = paths.subList(start, end);
                t[i] = runSingleThread(list, "" + i);
            }
            for (Thread one : t) {
                synchronized (one) {
                    one.start();
                }
                System.out.println("thread start");
            }
            try {
                for (Thread one : t) {
                    one.join();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(DriveCheck.class.getName()).log(Level.SEVERE, null, ex);
            }

            paths = getPaths();
            System.out.println("----------------------- NEW ROUND CHECK ----------------");
        }
        System.out.println("FINISHED CHECK");

    }

    private static List<String> getPaths() {
        List<String> paths = Util.readAllFilePathsSubfolders("/Users/Martin/Documents/zakony/PDF", new ArrayList());
        List<String> result = new ArrayList();
        for (String s : paths) {
            String pom = s.substring(s.lastIndexOf("/") + 1);
            if (pom.contains("_")
                    && !pom.contains("Store")
                    && !pom.contains(".txt")
                    && !pom.contains("col_")
                    && !pom.contains(".docx")
                    && !s.contains("/split/")
                    && isTranslated(paths, s)
                    && !isChecked(paths, s)) {
                result.add(s);
            }
            if (result.size() > 5000) {
                break;
            }

        }

        return result;
    }

    private static boolean isTranslated(List<String> paths, String s) {
        if (s.contains(".txt")) {
            return false;
        }
        s = s + ".txt";
        for (String path : paths) {
            if (path.equals(s)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isChecked(List<String> paths, String s) {
        String path = s.replace("/page/", "/page/check/") + ".txt";
        File f = new File(path);
//        System.out.println("f.exists:" + f.exists());
//        System.out.println("path: "+ path);
        return f.exists();
    }

    private static Thread runSingleThread(List<String> files, String index) {
        ToText texter = new ToText();
        Runnable r = () -> {
            synchronized (texter) {
                int progress = 0;
                for (String s : files) {
                    texter.readAndMove(s, index, s.substring(0, s.lastIndexOf("/") + 1));
                    progress++;
                    System.out.println("PROGRESS THREAD " + index + ": " + progress + "/" + files.size());
                }
            }
        };
        Thread t = new Thread(r);
        return t;
    }

}
