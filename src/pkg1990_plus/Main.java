/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1990_plus;

import Frames.CheckRepair;
import ManualFootnotes.MainFoot;
import Read.FinishLaw;
import RepairDoubleLines.RepairDoubleLines;
import RepairUpperCaseWords.RepairUpperCase;
import Utils.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import pkg1990_plus.checkSeparate.Check;

/**
 *
 * @author Martin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MainFoot.main(null);

        //testRepairDoubleLine();

        //List<HashMap> input = Footnotes.correct();
        //CheckDriveCorrect c = new CheckDriveCorrect(input);
        //HandlerFrame han = new HandlerFrame();
    }

    private static void checkAll() {
        List<String> files = Util.readAllFilePaths("/Users/Martin/Documents/zakony/PDF/1990/13_1990/page");
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty(
                "com.apple.mrj.application.apple.menu.about.name", "Main");
        Check ch = new Check();
    }

    private static void deleteAll() {
        List<String> paths = Util.readAllFilePathsSubfolders("/Users/Martin/Documents/zakony/PDF/", new ArrayList());
        for (String path : paths) {
            String pom = path.substring(path.lastIndexOf("/"));
            if (pom.contains("png.txt") || pom.contains("_")) {
                File f = new File(path);
                f.delete();
            }
        }
        System.exit(0);
    }

    // Check repair test - oteviram tam, kde jsou nejaky podezrely velky pismena
    //prozenu Space OCR a mrknu jaky to je
    private static void testUpperCaseRepair() {
        List<String> paths = FinishLaw.getPartsWithDoubleLine();
        List<HashMap> input = new ArrayList();
        int index = 0;
        Collections.shuffle(paths);
        for (String s : paths) {
            if (index > 20) {
                break;
            }
            String repaired = RepairUpperCase.repairUppercase(s, s.replace(".png.txt", ".png"));
            HashMap h = new HashMap();
            h.put("image", s.replace(".png.txt", ".png"));
            h.put("repaired", repaired);
            h.put("original", s);
            input.add(h);
            index++;
        }
        CheckRepair f = new CheckRepair(input);
    }

    private static void testRepairDoubleLine() {
        List<String> paths = FinishLaw.getPartsWithDoubleLine();
        List<HashMap> hash= new ArrayList();
        for (String path: paths) {
            HashMap h = new HashMap();
            h.put("original",path);
            h.put("image",path.replace(".png.txt", ".png"));

            String rep = RepairDoubleLines.repair(path.replace(".png.txt", ".png"));
            h.put("repaired",rep);
            hash.add(h);

            if (hash.size()>5) {
                //break;
            }
            
        }
        Collections.shuffle(hash);
        CheckRepair rep = new CheckRepair(hash);
    }

}
