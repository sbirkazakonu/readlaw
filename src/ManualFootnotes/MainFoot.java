/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import Utils.Util;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
public class MainFoot {

    public static int count;
    public static double widthHeight;

    public static void main(String[] args) {
        //testPomerWidthHeight();
        //testSeparate();
        testTextFinderTwo();

    }

    public static void repairConnectedLetters() {
        //LetterLearn l = new LetterLearn();
        List<String> paths = Util.readAllFilePathsCondition("/Users/Martin/Documents/zakony/test/", ".png");
        TextFinder finder = new TextFinder();
        int index = 0;
        for (String path : paths) {
            System.out.println("path: " + path);
            finder.separateConnectedLetters(Util.readImage(path));
            BufferedImage img = finder.paintAll();
            String outputPath = "/Users/Martin/Documents/zakony/output/";
            Util.saveImage(finder.lowerPart, outputPath + index + "_a.png");
            Util.saveImage(finder.topPart, outputPath + index + "_b.png");
            Util.saveImage(img, outputPath + index + "_c.png");
            //System.out.println("done: " + finder.result);
            index++;
            System.out.println("=============================");
        }
    }

    public static void makeRectangles() {
        count = 0;
        List<String> paths = Util.readAllFilePaths("/Users/Martin/Documents/zakony/foots/");
        int index = 0;
        //
        for (String path : paths) {
            if (path.endsWith(".txt")) {
                continue;
            }
            System.out.println("path: " + path);
            FooterFinder ff = new FooterFinder(path);

            if (!ff.haveFootnote) {
                continue;
            }
            index++;
            System.out.println("path: " + path);
            System.out.println("-------");
            if (index > 355) {
                break;
            }
        }

    }

    public static void textFinder(String path) {
        if (path.contains(".png.txt")) {
            return;
        }
        BufferedImage img = Util.readImage(path);
        if (img.getHeight() < 50) {
            //continue;
        }
        System.out.println("path:" + path);
        TextFinder tF = new TextFinder();
        tF.TextFinderFootnote(img);
        System.out.println("=============================================");
    }

    private static void savePathToDone(String path) {
        String done = "";
        File f = new File("/Users/Martin/Documents/zakony/probe/done.txt");
        if (f.exists()) {
            done += Util.readTXT("/Users/Martin/Documents/zakony/probe/done.txt");
        }
        done += "\n" + path;
        Util.saveTXT("/Users/Martin/Documents/zakony/probe/done.txt", done);

    }

    public static void testTextFinder(String path) {
        TextFinder tF = new TextFinder();
        tF.TextFinderFootnote(Util.readImage(path));
        BufferedImage testImg = tF.paintAll();

        Util.saveImage(testImg, "/Users/Martin/Documents/zakony/test.png");
    }

    public static void testFootFind(String path) {
        FooterFinder f = new FooterFinder(path, true);
        f.paintWholeFootnotes();
        Util.saveImage(f.img, "/Users/Martin/Documents/zakony/test.png");
    }

    public static void testTextFinderTwo() {
        List<String> paths = Util.readFilesInSubfolderCondition("/Users/Martin/Documents/zakony/PDF/1990", ".png", new ArrayList());
        System.out.println("paths: " + paths.size());
        String finished = Util.readTXT("/Users/Martin/Documents/zakony/probe/done.txt");
        String noFoot = Util.readTXT("/Users/Martin/Documents/zakony/probe/nofoot.txt");
        Collections.shuffle(paths);
        for (String path : paths) {
            if (path.endsWith(".txt")
                    || !path.substring(path.lastIndexOf("/")).contains("_")
                    //|| finished.contains(path)
                    || noFoot.contains(path)
                    || path.contains("priloha")
                    || !path.contains("25_1990")
                    || !path.contains("25_1990/page/page15_32.png")
                    ) {
                continue;
            }
            FooterFinder f = new FooterFinder(path, true);
            TextFinder2 tF = new TextFinder2();
            int index = 0;
            for (Rectangle r : f.wholeFootnote) {
                tF.TextFinderFootnote(Util.getSubImage(f.getImg(), r));
                BufferedImage testImg = tF.paintAll();
                Util.saveImage(testImg, "/Users/Martin/Documents/zakony/test_" + index + ".png");
                index++;
                System.out.println("text: " + tF.footText);
            }

        }
    }
    

    private static void testSeparate() {
        //LetterLearn let = new LetterLearn();
        List<String> paths = Util.readFilesInSubfolderCondition("/Users/Martin/Documents/zakony/PDF/1990", ".png", new ArrayList());
        System.out.println("paths: " + paths.size());
        String finished = Util.readTXT("/Users/Martin/Documents/zakony/probe/done.txt");
        String noFoot = Util.readTXT("/Users/Martin/Documents/zakony/probe/nofoot.txt");
        Collections.shuffle(paths);
        for (String path : paths) {
            if (path.endsWith(".txt")
                    || !path.substring(path.lastIndexOf("/")).contains("_")
                    //|| finished.contains(path)
                    || noFoot.contains(path)
                    || path.contains("priloha")
                    || !path.contains("25_1990")
                    || !path.contains("25_1990/page/page15_32.png")) {
                continue;
            }
            System.out.println("=============path: " + path);
            FooterFinder f = new FooterFinder(path);
            for (Rectangle rect : f.wholeFootnote) {
                f.paintRectangle(f.img, rect);
            }
            for (Rectangle rect : f.done) {
                f.paintRectangle(f.img, rect);
            }
            if (f.done.size() > 0) {
                //Util.saveImage(f.img, "/Users/Martin/Documents/zakony/test/a_" + count + ".png");
                count++;
            }
            //savePathToDone(path);

            if (!f.haveFootnote) {
                continue;
            }
        }
    }

    public static void saveWholeFootnote(String path) {
        BufferedImage img = Util.readImage(path);
        FooterFinder f = new FooterFinder(img);
        f.makeBorders();
        for (Rectangle rect : f.wholeFootnote) {
            Util.saveImage(Util.getSubImage(f.img, rect), "/Users/Martin/Documents/zakony/whole/" + MainFoot.count + ".png");
            count++;
        }

    }
}
