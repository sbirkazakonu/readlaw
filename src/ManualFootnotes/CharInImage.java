/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import Utils.Util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
class CharInImage {

    static void test() {
        List<String> paths = Util.readAllFilePathsCondition("/Users/Martin/Documents/zakony/big_foot/", "foot_");
        int index = 0;
        for (String path : paths) {
            Character nrs[] = {'2', '3', '4', '5', '6', '7', '8', '9', '0', '1'};
            boolean result = false;
            BufferedImage img = Util.readImage(path);
            for (char nr : nrs) {
                CharInImage finder = new CharInImage(img, nr);
                result = finder.result.size() > 0 ? true : false;
                if (result) {
                    break;
                }
            }
            if (!result) {
                System.out.println("================PATH: " + path);
            }
            Util.saveImage(img, "/Users/Martin/Documents/zakony/big_foot/z_" + result + "_" + index + ".png");
            index++;

        }

    }

    static void test2() {
        List<String> paths = Util.readAllFilePathsCondition("/Users/Martin/Documents/zakony/big_foot/", "number_false");
        int index = 0;
        char fin = '-';
        for (String path : paths) {
            Character nrs[] = {'2', '3', '4', '5', '6', '7', '8', '9', '0', '1'};
            boolean result = false;
            BufferedImage img = Util.readImage(path);
            if (img.getHeight() < 51) {
                continue;
            }
            for (char nr : nrs) {
                CharInImage finder = new CharInImage(img, nr);
                result = finder.result.size() > 0;
                if (result) {
                    fin = nr;
                    System.out.println("nr: " + nr);
                    break;
                
                }else{
                    System.out.println("false nr:"+nr);
                }
            }
            if (!result) {
                System.out.println("================PATH: " + path);
            }
            Util.saveImage(img, "/Users/Martin/Documents/zakony/big_foot/z_" + result + "_" + index + "_" + fin + ".png");
            index++;

        }

    }

    BufferedImage img;
    int pixels[][];
    int[][] template;
    double sensitivity = 1;

    char character;
    List<HashMap> result;

    boolean colored;

    public CharInImage() {

    }

    public CharInImage(String path) {
        img = Util.readImage(path);
        pixels = Util.getPixelArray(img);
        pixels = convertPixels(pixels);
        String temp = Util.readTXT("/Users/Martin/Documents/zakony/templates/p.txt");
        template = convertTemplateToInt(temp);
        find();

        if (!colored) {
            //System.out.println("path: " + path);
            for (int[] row : pixels) {
                System.out.println(Arrays.toString(row));
            }
            Util.saveImage(img, "/Users/Martin/Documents/zakony/test/Not_col" + Math.round(Math.random() * 10000) + ".png");
        } else {
            Util.saveImage(img, "/Users/Martin/Documents/zakony/test/col" + Math.round(Math.random() * 10000) + ".png");

        }

    }

    public CharInImage(BufferedImage img) {
        this.img = img;
        pixels = Util.getPixelArray(img);
        pixels = convertPixels(pixels);
        String temp = Util.readTXT("/Users/Martin/Documents/zakony/templates/p.txt");
        template = convertTemplateToInt(temp);
        find();

        if (!colored) {
            for (int[] row : pixels) {
                System.out.println(Arrays.toString(row));
            }
            Util.saveImage(img, "/Users/Martin/Documents/zakony/test/Not_col" + Math.round(Math.random() * 10000) + ".png");
        } else {
            Util.saveImage(img, "/Users/Martin/Documents/zakony/test/col" + Math.round(Math.random() * 10000) + ".png");
        }

    }

    public CharInImage(BufferedImage img, char character) {
        this.img = img;
        result = new ArrayList();
        this.character = character;
        //result.put("found", colored);
        pixels = Util.getPixelArray(img);
        pixels = convertPixels(pixels);
        String temp = Util.readTXT("/Users/Martin/Documents/zakony/templates/" + character + ".txt");
        //printDoubleArray(template);
        template = convertTemplateToInt(temp);
        //printDoubleArray(pixels);
        if (Character.isDigit(this.character)) {
            findNumber(img, this.character);
        } else {
            find();
        }
    }

    public List<HashMap> find() {
        for (int i = 0; i < pixels.length - template.length + 1; i++) {
            for (int j = 0; j < pixels[i].length - template[0].length + 1; j++) {
                if (isThereTemplate(i, j)) {
                    colorTemplate(i, j);
                    //
                    colored = true;
                    HashMap addit = new HashMap();
                    addit.put("point", new Point(j, i));
                    addit.put("dimension", new Dimension(template[0].length, template.length));
                    addit.put("found", colored);
                    result.add(addit);
                }

            }
            //System.out.println("result: "+result);
        }

        return result;
    }

    public List<HashMap> find(BufferedImage img, char c) {
        this.img = img;
        result = new ArrayList();
        character = c;
        pixels = Util.getPixelArray(img);
        pixels = convertPixels(pixels);
        String temp = Util.readTXT("/Users/Martin/Documents/zakony/templates/" + c + ".txt");
        template = convertTemplateToInt(temp);
        if (Character.isDigit(this.character)) {
            findNumber(img, this.character);
        } else {
            find();
        }
        return result;
        //return h;
    }

    private boolean isThereTemplate(int y, int x) {
        int badCount = 0;
        int maxBadCount = 8 * template.length * template[0].length / 4;
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                if (pixels[y + i][x + j] != template[i][j]) {
                    int wrongPoints = getNumberOfWrongPoints(i, j);
                    badCount += wrongPoints;
                }
                if (badCount > maxBadCount) {
                    return false;
                }
            }
        }
        return true;
    }

    private static int[][] convertTemplateToInt(String temp) {
        int result[][] = new int[temp.split("\n").length][temp.split("\n")[0].split(";").length];
        String row[] = temp.split("\n");
        for (int i = 0; i < row.length; i++) {
            String cell[] = row[i].split(";");
            for (int j = 0; j < cell.length; j++) {
                int curr = Integer.parseInt(cell[j]);
                result[i][j] = curr < 6 ? 0 : 1;
            }
        }
        //System.out.println("result: " + result.length + " x " + result[0].length);
        result = cropArray(result);
        //System.out.println("result: " + result.length + " x " + result[0].length);
        //System.out.println("======================");
        return result;
    }

    private static int[][] convertPixels(int[][] pixels) {
        for (int i = 0; i < pixels.length; i++) {

            for (int j = 0; j < pixels[0].length; j++) {
                if (pixels[i][j] == -1) {
                    pixels[i][j] = 0;
                } else {
                    pixels[i][j] = 1;
                }
            }
        }
        pixels = cropArray(pixels);
        return pixels;
    }

    private void colorTemplate(int y, int x) {
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                //System.out.println("template: " + template[i][j]);

                if (template[i][j] != 0) {
                    pixels[i + y][j + x] = 2;
                    img.setRGB(j + x, y + i, 7000000);
                    //System.out.println("7667");
                }

            }
        }
        //Util.saveImage(img, "/Users/Martin/Documents/zakony/test/col" + Math.round(Math.random() * 10000) + ".png");
    }

    private int getNumberOfWrongPoints(int y, int x) {
        int result = 0;
        int curr = template[y][x];
        //System.out.println("CURR: " + curr);
        for (int i = -1; i < 2; i++) {

            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                if (y + i < 0
                        || y + i == template.length
                        || x + j < 0) {
                    continue;
                }
                //System.out.println("y+i: " + (y+i));
                if (x + j == template[y + i].length) {
                    continue;
                }
                if (curr == template[y + i][x + j]) {
                    result++;
                }
            }
        }
        //int distanceFromMatch = getDistanceFromMatch(y, x);
        if (result == 8) {
            //System.out.println("RESULT: "+8);
            int distanceFromMatch = getDistanceFromMatch(y, x);
            result = result + distanceFromMatch;
            if (Character.isDigit(character)) {
                result *= 3 * sensitivity;
            } else {
                result *= 2 * sensitivity;
            }

        }
        if (curr == 1) {
            result *= result;
        } else if (curr == 0 && img.getHeight() < 50) {
            result *= 2;
        }

        return result;

    }

    private int getDistanceFromMatch(int y, int x) {
        int curr = template[y][x];
        int result = 100000;
        for (int i = y; i < template.length; i++) {
            if (template[i][x] != curr && i - y < result) {
                result = i - y;
            }
        }
        for (int i = y; i >= 0; i--) {
            if (template[i][x] != curr && Math.abs(i - y) < result) {
                result = y - i;
            }
        }
        for (int i = x; i < template[0].length; i++) {
            if (template[y][i] != curr && Math.abs(i - x) < result) {
                result = i - x;
            }
        }
        for (int i = x; i >= 0; i--) {
            if (template[y][i] != curr && Math.abs(i - x) < result) {
                result = x - i;
            }
        }
        if (result != 0) {
            //System.out.println("RESULT: "+result);
        }

        return result;
    }

    List<HashMap> findNumber(BufferedImage img, char c) {
        this.img = img;
        this.character = c;
        result = new ArrayList();

        pixels = Util.getPixelArray(img);
        pixels = convertPixels(pixels);

        String temp = Util.readTXT("/Users/Martin/Documents/zakony/templates/" + c + ".txt");
        template = convertTemplateToInt(temp);
        int yBegin = getIndexProbableBegin();
        int yEnd = yBegin==0?pixels.length-template.length: yBegin+10;
        img.setRGB(0, yBegin, 500000);
        img.setRGB(1, yBegin, 500000);
        img.setRGB(0, yEnd, 500000);
        img.setRGB(1, yEnd, 500000);
        System.out.println("BEGIN: " + yBegin+ " end: "+yEnd);
        for (int i = yEnd; i >= yBegin; i--) {
            for (int j = 0; j < pixels[i].length - template[0].length + 1; j++) {
                if (isThereNumberTemplate(i, j) && exactNumberCheck(i, j)) {
                    //colorTemplate(i, j);
                    colored = true;
                    HashMap addit = new HashMap();
                    addit.put("point", new Point(j, i));
                    addit.put("dimension", new Dimension(template[0].length, template.length));
                    addit.put("found", colored);
                    result.add(addit);
                }
            }
        }
        return result;
    }

    private boolean isThereNumberTemplate(int y, int x) {
        int badCount = 0;
        int maxBadCount = 8 * template.length * template[0].length / 3;
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                if (pixels[y + i][x + j] != template[i][j]) {
                    int wrongPoints = getNumberOfWrongPoints(i, j);
                    badCount += wrongPoints;
                }
                if (badCount > maxBadCount) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean exactNumberCheck(int y, int x) {

        if (character == '1') {
            int count = 0;
            Rectangle testRect = new Rectangle(x, y + template.length, template[0].length, 7);
            for (int k = testRect.x; k < testRect.x + testRect.width; k++) {
                for (int l = testRect.y; l < testRect.y + testRect.height; l++) {
                    if (k < pixels.length && l < pixels[0].length && pixels[l][k] == 1) {
                        count++;
                    }

                }
                if (count > 15) {
                    return false;
                }
            }
            if (isLetterP(y, x)) {
                return false;
            }

        }
        if (character == '0') {
            if (isLetterP(y, x)) {
                return false;
            }
        }
        return true;
    }

    private boolean isLetterP(int y, int x) {
        CharInImage finder = new CharInImage(img, 'p');
        List<HashMap> result = finder.result;
        if (result.size() == 1) {
            Point p = (Point) result.get(0).get("point");
            Dimension dim = (Dimension) result.get(0).get("dimension");
            System.out.println("dim: " + dim);
            System.out.println("point: " + p);
            System.out.println("x: " + x + " y: " + y);
            if (p.x <= x && p.x + dim.width >= x
                    && p.y <= y && p.y + (dim.height * 2 / 3) >= y) {
                System.out.println("did it");
                return true;
            }
        } else if (result.size() == 0) {
            System.out.println("BRAKE");
            return false;
        }
        return true;
    }

    private int getIndexProbableBegin() {
        BufferedImage im = Util.deepCopy(img);
        CharInImage charInImage = new CharInImage(im, ')');
        List<HashMap> list = charInImage.result;

        if (list.size() > 0) {
            for (HashMap h : list) {
                Point p = (Point) h.get("point");
                Dimension dim = (Dimension) h.get("dimension");
                //System.out.println("----");
                //System.out.println("p: " + p);
                //System.out.println("dim: " + dim);
                if (p.y + dim.height > img.getHeight() - 10 && p.x + dim.width > img.getWidth() - 10) {
                    int result = p.y - 10 < 0 ? 0 : p.y - 10;
                    return result;
                }
            }
        }

        return 0;
    }

    private static int[][] cropArray(int[][] result) {
        boolean check = true;
        for (int i = 0; i < result.length; i++) {
            if (result[i][0] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length][result[0].length - 1];
            for (int i = 0; i < result.length; i++) {
                pom[i] = Arrays.copyOfRange(result[i], 1, result[0].length);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result.length; i++) {
            if (result[i][result[i].length - 1] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length][result[0].length - 1];
            for (int i = 0; i < result.length; i++) {
                pom[i] = Arrays.copyOfRange(result[i], 0, result[0].length - 1);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result[0].length; i++) {
            if (result[0][i] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length - 1][result[0].length];
            for (int i = 1; i < result.length; i++) {
                for (int j = 0; j < result[i].length; j++) {
                    pom[i - 1][j] = result[i][j];
                }
//                pom[i] = Arrays.copyOfRange(result[i], 1, result[0].length);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result[0].length; i++) {
            if (result[result.length - 1][i] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length - 1][result[0].length];
            for (int i = 0; i < result.length - 1; i++) {
                for (int j = 0; j < result[i].length; j++) {
                    pom[i][j] = result[i][j];
                }
//                pom[i] = Arrays.copyOfRange(result[i], 0, result[0].length - 1);
            }
            result = pom;
        }
        return result;
    }

    public void setSensitivity(double d) {
        this.sensitivity = d;
    }

    private void printDoubleArray(int[][] arr) {
        for (int[] row : arr) {
            System.out.println(Arrays.toString(row));
        }
    }

}
