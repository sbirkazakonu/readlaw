/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import Objects.Panel;
import Utils.Util;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;

/**
 *
 * @author Martin
 */
public class LetterLearn extends JFrame implements KeyListener {

    Panel p;
    JTextPane input;

    GridLayout main;

    JButton btn;

    List<String> paths;

    int index;

    public LetterLearn() {
        declarations();
        createFrame();
        putObjects();
        this.setVisible(true);
        index = 0;
        
        paths = Util.readAllFilePathsCondition("/Users/Martin/Documents/zakony/index/", "");
        List<String> pom = paths;
        paths=new ArrayList();
                
        for (String path:pom) {
            BufferedImage im = Util.readImage(path);
            //if (im.getWidth()<14 && im.getHeight()>29) {
                paths.add(path);
            //}
        }
        
        Collections.shuffle(paths);
        System.out.println("paths.size: " + paths.size());
    }

    private void declarations() {
        p = new Panel();
        input = new JTextPane();
        btn = new JButton("handle");
        main = new GridLayout(0, 1);
    }

    private void createFrame() {
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(main);
        btn.addKeyListener(this);
        input.addKeyListener(this);

    }

    private void putObjects() {
        this.add(p);
        this.add(input);
        this.add(btn);
    }

    private void saveTemplate(String text, String template) {
        String mainpath = "/Users/Martin/Documents/zakony/templates/" + text + ".txt";
        File f = new File(mainpath);
        if (f.exists()) {
            String prev = Util.readTXT(mainpath);
            String newTemp = mergeTemplates(template, prev);
            System.out.println(newTemp);
            Util.saveTXT(mainpath, newTemp);
        } else {
            Util.saveTXT(mainpath, template);
        }

    }

    private String mergeTemplates(String template, String prev) {
        String result = "";
        String longer = "";
        String shorter = "";

        if (template.split("\n")[0].split(";").length > prev.split("\n")[0].split(";").length) {
            longer = template;
            shorter = prev;
        } else {
            longer = prev;
            shorter = template;
        }

        for (int i = 0; i < longer.split("\n").length; i++) {
            if (i != 0) {
                result += "\n";
            }
            for (int j = 0; j < longer.split("\n")[i].split(";").length; j++) {
                String temp = longer.split("\n")[i].split(";")[j];
                String addit = "0";
                if (i < shorter.split("\n").length && j < shorter.split("\n")[i].split(";").length) {
                    addit = shorter.split("\n")[i].split(";")[j];
                }
                int currCell = Integer.parseInt(temp) + Integer.parseInt(addit);
                if (j != longer.split("\n")[i].split(";").length - 1) {
                    result += currCell + ";";
                } else {
                    result += currCell;
                }

            }
        }

        return result;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT && e.isMetaDown()) {
            e.consume();
            System.out.println("===");
            if (index < paths.size() - 1) {
                index++;
                p.setPic(Util.readImage(paths.get(index)));
                System.out.println(paths.get(index));
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT && e.isMetaDown()) {
            e.consume();
            System.out.println("===");

            if (index > 0) {
                index--;
                p.setPic(Util.readImage(paths.get(index)));
                System.out.println(paths.get(index));
            }
        }
        if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
            e.consume();
            BufferedImage img = Util.readImage(paths.get(index));
            int pixels[][] = Util.getPixelArray(img);
            String template = "";
            for (int i = 0; i < pixels.length; i++) {
                if (i != 0) {
                    template += "\n";
                }
                for (int j = 0; j < pixels[i].length; j++) {
                    int nr = pixels[i][j] == -1 ? 0 : 1;
                    if (j != pixels[i].length - 1) {
                        template += nr + ";";
                    } else {
                        template += nr;
                    }
                }
            }
            
            saveTemplate(input.getText(), template);
            input.setText("");
            System.out.println("===");
            if (index < paths.size() - 1) {
                index++;
                p.setPic(Util.readImage(paths.get(index)));
                System.out.println(paths.get(index));
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
