/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import MyOCR.OCR;
import Utils.Util;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
public class FooterFinder {

    List<Rectangle> done, allRects;
    List<BufferedImage> parts;
    List<Rectangle> wholeFootnote;
    String path, text;
    BufferedImage img;
    int pixels[][];
    boolean haveFootnote;
    List<HashMap> partPoints;

    public FooterFinder() {

    }

    public FooterFinder(String imgPath, boolean test) {
        path = imgPath;
        img = Util.readImage(path);
        pixels = Util.getPixelArray(img);
        done = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        long start = System.currentTimeMillis();
        makeBorders();
    }

    public FooterFinder(String imgPath) {
        path = imgPath;
        img = Util.readImage(path);
        pixels = Util.getPixelArray(img);
        done = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        long start = System.currentTimeMillis();
        //System.out.println("start");
        makeBorders();

        if (haveFootnote) {
            ImageSeparator imSep = new ImageSeparator(this);
            text = imSep.getText();
            parts = imSep.parts;
        } else {
            savePathToNoFoot();
            System.out.println("NO FOOTNOTE");
        }
    }

    public FooterFinder(BufferedImage img) {
        this.img = img;
        //img = Util.readImage(path);
        pixels = Util.getPixelArray(this.img);
        done = new ArrayList();
        parts = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        makeBorders();

//        if (haveFootnote) {
//            ImageSeparator imSep = new ImageSeparator(this);
//            parts = imSep.getParts();
//        }
    }

    public BufferedImage colorImg() {
        int zavHeight = 41;
        int zavWidth = 11;
        for (int i = 0; i < pixels.length - zavHeight; i++) {
            //System.out.println("i+ " + i + " x j: " + 0);
            for (int j = 0; j < pixels[i].length - zavWidth; j++) {
                //vytvori male subarray 11x41 (rozmery zavorky)
                int arr[][] = new int[zavHeight][zavWidth];
                for (int k = 0; k < zavHeight; k++) {
                    for (int l = 0; l < zavWidth; l++) {
                        arr[k][l] = pixels[i + k][j + l];
                    }
                }

            }
        }
        return img;
    }

    public void makeBorders() {
        //1905,1253
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++) {
                if (pixels[i][j] != -1 && isDone(j, i)) {
                    findBorder(pixels, j, i);
                }
            }
        }
        allRects = done;
        eliminateWrongRectangles();
        addIndexes();

    }

    /**
     * v obrazku se snazi najit znak a jeho ohraniceni, kde zacina a kde konci
     * Idealni vysledek - kazdy pismeno by melo byt ohraniceno
     *
     * @param pixels - obrazek v pixelech
     * @param i - index kde zacit
     * @param j - index kde zacit
     */
    private void findBorder(int[][] pixels, int x, int y) {
        // hashmap - "check", "point"
        List<HashMap> points = new ArrayList();
        HashMap init = new HashMap();
        init.put("check", false);
        init.put("point", new Point(x, y));
        init.put("color", img.getRGB(x, y));
        points.add(init);
        while (isAllChecked(points)) {
            points = addAllPossible(points);
        }
        partPoints.add(getHashPoints(points));
        int x2 = 0, y2 = 0;
        int x1 = 20000, y1 = 20000;
        for (HashMap h : points) {
            int xPoint = ((Point) h.get("point")).x;
            int yPoint = ((Point) h.get("point")).y;
            if (xPoint > x2) {
                x2 = xPoint;
            }
            if (yPoint > y2) {
                y2 = yPoint;
            }
            if (xPoint < x1) {
                x1 = xPoint;
            }
            if (yPoint < y1) {
                y1 = yPoint;
            }
        }
        //System.out.println("---");
        Rectangle rect = new Rectangle(x1, y1, x2 - x1 + 1, y2 - y1 + 1);
        if (!done.contains(rect)) {
            //Util.saveImage(Util.getSubImage(img, rect), "/Users/Martin/Documents/zakony/letters/let_"+Math.round(Math.random()*1000000)+ ".png", true);
            done.add(rect);
        }
    }

    private boolean isAllChecked(List<HashMap> points) {
        for (HashMap h : points) {
            if (!((boolean) h.get("check"))) {
                return true;
            }
        }
        return false;
    }

    private List<HashMap> addAllPossible(List<HashMap> points) {
        List<HashMap> addit = new ArrayList();
        for (HashMap h : points) {
            if (!((boolean) h.get("check"))) {
                Point p = (Point) h.get("point");
                h.put("check", true);
                addit.addAll(addAround(points, p));
                //System.out.println("addit in loop: "+addit.size());
            }
        }
        addit = removeAlreadyChecked(points, addit);
        points.addAll(addit);
        //points = removeDuplicate(points);
        return points;
    }

    private List<HashMap> addAround(List<HashMap> points, Point p) {
        List<HashMap> newPoints = new ArrayList();
        for (int i = -1; i <= 1 && p.y + i < pixels.length; i++) {
            if (p.y + i < 0) {
                continue;
            }
            for (int j = -1; j <= 1 && p.x + j < pixels[p.y + i].length; j++) {

                if (p.x + j > -1 && pixels[p.y + i][p.x + j] != -1
                        && isNotAdded(points, p.x + j, p.y + i)) {
                    HashMap add = new HashMap();
                    add.put("check", false);
                    add.put("point", new Point(p.x + j, p.y + i));
                    add.put("color", img.getRGB(p.x + j, p.y + i));
                    newPoints.add(add);
                }
            }
        }

        return newPoints;

    }

    private List<HashMap> removeDuplicate(List<HashMap> points) {
        List<HashMap> result = new ArrayList();
        for (int i = 0; i < points.size(); i++) {
            Point pt = (Point) points.get(i).get("point");
            boolean check = true;
            for (int j = i + 1; j < points.size(); j++) {
                Point point = (Point) points.get(j).get("point");
                if (point.x == pt.x
                        && point.y == pt.y) {
                    boolean done = (boolean) points.get(i).get("check");
                    boolean doneTwo = (boolean) points.get(j).get("check");
                    if (done) {
                        points.get(j).put("check", true);
                    }
                    check = false;
                }

            }
            if (check) {
                result.add(points.get(i));
            }
        }
        return result;

    }

    private boolean isDone(int x, int y) {

        for (HashMap h : partPoints) {
            List<Point> points = (List<Point>) h.get("list");
            for (Point p : points) {
                if (x == p.x && y == p.y) {
                    return false;
                }
            }
        }
        return true;
    }

    public void paintAllBorders() {
        //System.out.println("done.size: " + allRects.size());
        for (Rectangle rect : allRects) {
            //Util.saveImage(img.getSubimage(rect.x, rect.y, rect.width, rect.height),"/Users/Martin/Documents/zakony/foots/foot_"+Math.round(Math.random()*100000)+".png");
            paintRectangle(rect);
        }
        //System.out.println("paint done");
    }

    public void paintBorders() {
        //System.out.println("done.size: " + done.size());
        for (Rectangle rect : done) {
            //Util.saveImage(img.getSubimage(rect.x, rect.y, rect.width, rect.height),"/Users/Martin/Documents/zakony/foots/foot_"+Math.round(Math.random()*100000)+".png");
            paintRectangle(rect);
        }
        //System.out.println("paint done");
    }

    private void paintRectangle(Rectangle rect) {
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            img.setRGB(i, (int) rect.getY(), 7000000);
            if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                img.setRGB(i, (int) ((int) rect.getY() + rect.getHeight()), 7000000);
            }
        }
        for (int i = startY; i < startY + rect.getHeight() && i < img.getHeight(); i++) {
            img.setRGB((int) rect.getX(), i, 7000000);
            img.setRGB((int) (rect.getX() + rect.getWidth() - 1), i, 7000000);
        }
    }

    public void paintRectangle(BufferedImage img, Rectangle rect) {
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            img.setRGB(i, (int) rect.getY(), 7000000);
            if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                img.setRGB(i, (int) ((int) rect.getY() + rect.getHeight()), 7000000);
            }
        }
        for (int i = startY; i < startY + rect.getHeight() && i < img.getHeight(); i++) {
            img.setRGB((int) rect.getX(), i, 7000000);
            img.setRGB((int) (rect.getX() + rect.getWidth() - 1), i, 7000000);
        }
    }

    private void blankRectangle(Rectangle rect) {
        //16777215
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            for (int j = startY; j < startY + rect.getHeight() && j < img.getHeight(); j++) {
                // if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                img.setRGB(i, j, 16777215);

            }
        }
    }

    private List<HashMap> addNonDuplicate(List<HashMap> points, List<HashMap> addit) {
        //System.out.println("points: " + points.size());
        //System.out.println("addit: " + addit.size());
        for (HashMap h : addit) {
            boolean check = true;
            for (HashMap point : points) {
                boolean dn = (boolean) point.get("check");
                Point add = (Point) h.get("point");
                Point pnt = (Point) point.get("point");
                if (add.x == pnt.x
                        && add.y == add.y
                        && dn) {
                    check = false;
                    break;
                }
            }
            if (check) {
                points.add(h);
            }
        }
        //System.out.println("points: " + points.size());
        //System.out.println("==============");
        return points;
    }

    private boolean isNotAdded(List<HashMap> points, int x, int y) {
        for (HashMap h : points) {
            Point p = (Point) h.get("point");
            if (x == p.x && y == p.y) {
                return false;
            }
        }
        return true;
    }

    private List<HashMap> removeAlreadyChecked(List<HashMap> points, List<HashMap> addit) {
        List<HashMap> result = new ArrayList();
        addit = removeDuplicate(addit);

        return addit;
    }

    private void eliminateWrongRectangles() {
        List<Rectangle> result = new ArrayList();
        List<Rectangle> addit = new ArrayList();
        for (Rectangle rect : done) {
            double pom = (double) rect.height / rect.width;

            if (pom > 3.3 && pom < 4) {
                BufferedImage im = Util.getSubImage(img, rect);
                int pixels[][] = Util.getPixelArray(im);
                int wrong = 0;
                for (int i = 0; i < pixels.length / 9; i++) {
                    for (int j = (int) ((double) pixels[i].length / 12 * 10); j < pixels[i].length; j++) {
                        //testImage.setRGB(rect.x + j, rect.y + i, 500000);

                        if (pixels[i][j] != -1) {
                            wrong++;
                        }
                        if (pixels[pixels.length - i - 1][j] != -1) {
                            wrong++;
                        }
                    }
                }
                for (int i = pixels.length / 3; i < pixels.length / 3 * 2; i++) {
                    for (int j = 0; j < pixels[0].length / 5 * 2; j++) {
                        //testImage.setRGB(rect.x + j, rect.y + i, 500000);
                        if (pixels[i][j] != -1) {
                            wrong++;
                        }
                    }

                }

                if (wrong < 2) {
                    addit.add(rect);
                }
            }
        }
        done = addit;
    }

    private void addIndexes() {
        List<Rectangle> addit = new ArrayList();
        for (Rectangle rect : done) {
            //System.out.println("done.height: " + rect.height);
            List<Rectangle> curr = new ArrayList();
            int h = (rect.y + rect.y + rect.height) / 2;
            int end = 0;
            //zjisti, kde zacina prvni pismenko pred tim (mezi timto indexem a závorkou se bude nachazet index horni)
            for (int i = 0; true && rect.x - i > -1; i++) {
                //img.setRGB(rect.x - i, h, 1000000);
                if (pixels[h][rect.x - i] != -1) {
                    end = rect.x - i;
                    break;
                }
            }
            for (Rectangle all : allRects) {
                //x je na spravnym miste
                int lower = rect.y + rect.height;
                if ((all.x < rect.x && all.x > end)
                        || (all.x + all.width < rect.x && all.x + all.width > end + 4)) {

                } else {
                    continue;
                }
                if (all.y + all.height < h
                        && all.y + all.height > rect.y
                        && all.height > 12) {

                } else {
                    continue;
                }
                BufferedImage readImg = img.getSubimage(all.x, all.y, all.width, all.height);
                curr.add(all);
                if (rect.height <37 && rect.height>33) {
                    //Util.saveImage(img.getSubimage(all.x, all.y, all.width, all.height), "/Users/Martin/Documents/zakony/index/ind_" + Math.round(Math.random() * 100000000) + ".png");
               }

            }
            curr.add(rect);
            if (curr.size() > 1) {
                Rectangle r = mergeWholeFootnote(curr);
                wholeFootnote.add(r);
                BufferedImage readImg = img.getSubimage(r.x, r.y, r.width, r.height);

                //Util.saveImage(readImg, "/Users/Martin/Documents/zakony/index/full_" + MainFoot.count + ".png");
                haveFootnote = true;
            }

        }
        done.addAll(addit);
    }

    private Rectangle mergeWholeFootnote(List<Rectangle> curr) {
        int x1 = 100000, x2 = 0, y1 = 100000, y2 = 0;

        for (Rectangle c : curr) {
            if (c.x < x1) {
                x1 = c.x;
            }
            if (c.x + c.width > x2) {
                x2 = c.x + c.width;
            }
            if (c.y < y1) {
                y1 = c.y;
            }
            if (c.y + c.height > y2) {
                y2 = c.y + c.height;
            }
        }
        Rectangle result = new Rectangle(x1, y1, x2 - x1, y2 - y1);

        return result;

    }

    private void splitImg(Rectangle r) {
        int middle = (2 * r.y + r.height) / 2;
        for (Rectangle all : allRects) {
            if (all.height > 60) {
                //paintRectangle(all);
            }
            //Smaže vše za závorkou
            if (middle > all.y
                    && middle < all.y + all.height
                    && r.x + r.width < all.x
                    || (r.x + r.width < all.x && all.y > r.y && all.y + all.height < r.y + r.height)) {
                blankRectangle(all);
                continue;
            }
            // smaže vše, co začíná až pod závorkou
            if (all.y > r.y + r.height) {
                blankRectangle(all);
                continue;
            }
            // jsou na pomezí a jsou to čárky nad pismenama
            if (all.y < r.y + r.height && all.y + all.height > r.y + r.height && all.width < 11 && all.height < 13) {
                blankRectangle(all);

            }
        }

    }

    void saveAllLetters() {
        int index = 0;
        for (Rectangle rect : allRects) {
            if (rect.height < 25) {
                continue;
            }
//            Util.saveImage(img.getSubimage(rect.x, rect.y, rect.width, rect.height), "/Users/Martin/Documents/zakony/letters/letter_" + MainFoot.count + "_" + rect.width + "x" + rect.height + ".png");
//            MainFoot.count++;
        }
    }

    public List<Rectangle> getDone() {
        return done;
    }

    public List<Rectangle> getAllRects() {
        return allRects;
    }

    public List<BufferedImage> getParts() {
        return parts;
    }

    public List<Rectangle> getWholeFootnote() {
        return wholeFootnote;
    }

    public String getPath() {
        return path;
    }

    public BufferedImage getImg() {
        return img;
    }

    public int[][] getPixels() {
        return pixels;
    }

    public boolean isHaveFootnote() {
        return haveFootnote;
    }

    public static List<BufferedImage> getSingleImageParts(BufferedImage image) {

        return null;
    }

    private HashMap getHashPoints(List<HashMap> points) {
        List<Point> result = new ArrayList();
        for (HashMap h : points) {
            result.add((Point) h.get("point"));
        }
        HashMap res = new HashMap();
        res.put("list", result);
        return res;
    }

    String getText() {
        return text;
    }

    private void savePathToNoFoot() {
        String done = "";
        File f = new File("/Users/Martin/Documents/zakony/probe/nofoot.txt");
        if (f.exists()) {
            done += Util.readTXT("/Users/Martin/Documents/zakony/probe/nofoot.txt");
        }
        done += "\n" + path;
        Util.saveTXT("/Users/Martin/Documents/zakony/probe/nofoot.txt", done);

    }

    public void paintWholeFootnotes() {
        for (Rectangle rect : wholeFootnote) {
            paintRectangle(rect);
        }
    }

    //Yiska part points z obrzku, idelani vytvorit prazdny konstruktor a zavolat tuhle metodu
    public List<HashMap> getPartPoints(BufferedImage img) {
        this.img = img;
        pixels = Util.getPixelArray(img);
        done = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++) {
                if (pixels[i][j] != -1 && isDone(j, i)) {

                    List<HashMap> points = new ArrayList();
                    HashMap init = new HashMap();
                    init.put("check", false);
                    init.put("point", new Point(j, i));
                    init.put("color", img.getRGB(j, i));
                    points.add(init);
                    while (isAllChecked(points)) {
                        points = addAllPossible(points);
                    }
                    partPoints.add(getHashPoints(points));
                }
            }
        }
        return partPoints;
    }

    /**
     * Získá všechny rectangly z předhozenýho obrázku
     */
    public List<Rectangle> getAllRectangles(BufferedImage img) {

        this.img = img;
        pixels = Util.getPixelArray(img);
        done = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        long start = System.currentTimeMillis();
        //System.out.println("start");
        makeBorders();
        return allRects;
    }

    /**
     * Získá všechny rectangly z předhozenýho obrázku
     */
    public List<Rectangle> getAllDoneRectangles(BufferedImage img) {

        this.img = img;
        pixels = Util.getPixelArray(img);
        done = new ArrayList();
        wholeFootnote = new ArrayList();
        partPoints = new ArrayList();
        long start = System.currentTimeMillis();
        //System.out.println("start");
        makeBorders();
        return done;
    }

}
