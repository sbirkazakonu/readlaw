/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import Utils.Util;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
public class TextFinder2 {

    List<HashMap> result;
    List<BufferedImage> imageParts;
    List<HashMap> templates, partPoints;
    BufferedImage image, topPart, lowerPart, originalImage;

    String footText;

    int high;

    int pixels[][];
    //veyme vsechno, co to tam našlo - por úcely hledani spojenejch pismen
    //pokud je false - najde jen dve nejpravdepodobnejsi - pro footnote
    //pokud je true - najde vse
    boolean getAll;

    // Minimalni shoda y deseti ruynzch templatu stejneho pismena, abz bzlo povazovano za jasny bod templtua
    final int MIN_TEMPLATE_OCCURENCE = 4;
    final double DOT_COUNT_SHARE = 0.8;

    public TextFinder2() {
        footText = "";
    }

    /**
     * najde na obrázku všechny písemna a vrátí horní a spodní část
     *
     * TOHLE JE POUZE NA FOOTNOTES - hledá to závorku a k ní přiřazuje Čísla
     *
     *
     * @param image
     * @return
     */
    public HashMap TextFinderFootnote(BufferedImage image) {
        getAll = false;
        this.image = image;
        this.originalImage = Util.deepCopy(image);
        declarations();
        findAllCharacters();
        for (HashMap h : result) {
            System.out.println("h: " + h.get("name"));
        }

        HashMap output = new HashMap();

        return output;
    }

    private void declarations() {
        pixels = Util.getPixelArray(image);
        imageParts = new ArrayList();
        templates = new ArrayList();
        result = new ArrayList();
        getSinglePartImageList();
        getTemplates();
    }

    private void findAllCharacters() {
        int index = 0;
        for (BufferedImage img : imageParts) {
            findCharactersInImage(img);
        }
    }

    private void findCharactersInImage(BufferedImage img) {
        int pix[][] = Util.getPixelArray(img);

        int xBegin = getPixelsXBegin(pix);
        int yBegin = getPixelsYBegin(pix);
        int xEnd = getPixelsXEnd(pix);
        int yEnd = getPixelsYEnd(pix);

        for (int i = yBegin; i < yEnd; i++) {
            for (int j = xBegin; j < xEnd; j++) {
                HashMap h = isThereAnything(pix, j, i);
            }
        }
    }

    public HashMap isThereAnything(int[][] pix, int x, int y) {
        HashMap res = new HashMap();
        for (HashMap h : templates) {
            HashMap dataTemplate = getTemplateMatch((int[][])h.get("template"),pix,x,y);
        }

        return res;
    }
    
    public HashMap getTemplateMatch(int[][] temp, int[][] pix,int x,int y ){
        HashMap res=  new HashMap();
        
        return res;
    }

    //312 napise J tam kde je pouze P
    int count = 0;
    

    private HashMap getBestScore(BufferedImage img, HashMap temp) {
        HashMap returned = new HashMap();
        returned.put("found", false);

        int currPix[][] = Util.getPixelArray(img);
        //Zacatky, kde se fakt neco deje, abych neprojizdel prazdny radky ()
        int xBegin = getPixelsXBegin(currPix);
        int yBegin = getPixelsYBegin(currPix);
        int xEnd = getPixelsXEnd(currPix);
        int yEnd = getPixelsYEnd(currPix);

        int template[][] = (int[][]) temp.get("template");
        int bestScore = 0;
        int score = 0;
        int badScore = 0;
        Point bestPoint = new Point(0, 0);
//        System.out.println("xBeg: " + xBegin + " yBegin: " + yBegin);
//        System.out.println("yEnd: " + yEnd + " cond2: " + (pixels[0].length - template[0].length));
        for (int i = xBegin; i < xEnd; i++) {

            for (int j = yBegin; j < yEnd; j++) {

                HashMap data = matchTemplateToImg(img, i, j, template);

                score = (int) data.get("good");
                //System.out.println("socre: "+score);
                badScore = (int) data.get("bad");

                if (score - badScore > bestScore) {

                    bestScore = score - badScore;
                    bestPoint = new Point(i, j);
                }

            }
        }
        int dotCount = (int) temp.get("dotCount");
        //System.out.println("bestScore: "+bestScore);
        if ((temp.get("name") + "").equals(")")) {
            for (int[] row : template) {
                //System.out.println(Arrays.toString(row));
            }
            System.out.println("result: " + isTemplate(bestScore, dotCount, temp.get("name") + ""));
            System.out.println("score: " + bestScore + "bad: " + badScore);
            System.out.println("--------------------------------------------------");
        }
        if (isTemplate(bestScore, dotCount, temp.get("name") + "")) {
            BufferedImage save = Util.deepCopy(img);

            //colorTemplateInImage(save, bestPoint, template);
            List<Point> points = getTakenPoints(currPix, template, bestPoint);
            returned.put("found", true);
            returned.put("score", bestScore);
            returned.put("good", score);
            returned.put("bad", badScore);
            returned.put("name", temp.get("name"));
            returned.put("point", bestPoint);
            returned.put("dimension", new Dimension(template[0].length, template.length));
            returned.put("points", points);

        }

        return returned;
    }

    private HashMap matchTemplateToImg(BufferedImage img, int x, int y, int[][] template) {
        int score = 0;
        int badScore = 0;
        int pix[][] = Util.getPixelArray(img);
        for (int i = 0; i < template.length && y + template.length < pix.length + 1; i++) {
            //System.out.println("pixels: "+ Arrays.toString(pixels[y+i]));
            for (int j = 0; j < template[i].length && x + template[0].length < pix[0].length + 1; j++) {
                //prevedeni na 1/0 - je tam obrazek nebo neni
                int pixValue = pix[y + i][x + j] == -1 ? 0 : 1;
                if (pixValue == 1 && pixValue == template[i][j]) {
                    score++;
                } else if (pixValue == 1 && template[i][j] != 1) {
                    int distance = getDistance(template, i, j);
                    badScore += distance;
                }
            }
        }
        HashMap r = new HashMap();
        r.put("good", score);
        r.put("bad", badScore);
        return r;
    }

    private void colorTemplateInImage(BufferedImage save, Point bestPoint, int[][] template) {
        int pix[][] = Util.getPixelArray(save);
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                int pixValue = pix[bestPoint.y + i][bestPoint.x + j] == -1 ? 0 : 1;
                if (template[i][j] == 1 && pixValue == 1) {
                    save.setRGB(bestPoint.x + j, bestPoint.y + i, 400000);
                }
            }
        }
    }

    private void colorTemplateInImage(BufferedImage save, List<Point> points) {
        int col = (int) (Math.random() * 10000000);
        for (Point p : points) {
            save.setRGB(p.x, p.y, col);
        }
    }

    private List<Point> getTakenPoints(int[][] pix, int[][] template, Point bestPoint) {
        List<Point> result = new ArrayList();
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                int pixValue = pix[bestPoint.y + i][bestPoint.x + j] == -1 ? 0 : 1;
                if (template[i][j] == 1 && pixValue == 1) {
                    result.add(new Point(bestPoint.x + j, bestPoint.y + i));
                    //save.setRGB(bestPoint.x + j, bestPoint.y + i, 400000);
                }
            }
        }
        return result;
    }

    private List<HashMap> getProbableCombo(List<HashMap> partResult) {
        HashMap resOne = null;
        HashMap resTwo = null;
        double share = 0.8;
        int bestTotal = 0;
        List<HashMap> rert = new ArrayList();
        for (int i = 0; i < partResult.size(); i++) {
            int oneCount = ((List<Point>) partResult.get(i).get("points")).size();
            //resOne = partResult.get(i);
            for (int j = 0; j < partResult.size(); j++) {
                if (i != j) {
                    int twoCount = ((List<Point>) partResult.get(j).get("points")).size();
                    int overlay = getOverlay(((List<Point>) partResult.get(i).get("points")), ((List<Point>) partResult.get(j).get("points")));
                    int total = oneCount + twoCount - overlay;
                    if (overlay > share * twoCount) {

                        continue;
                    }
                    if (total > bestTotal) {

                        bestTotal = total;
//                        System.out.println("letters: " + partResult.get(i).get("name") + " - " + partResult.get(j).get("name"));
//                        System.out.println("count: " + total);
//                        System.out.println("overLay: " + overlay);
                        resOne = partResult.get(i);
                        resTwo = partResult.get(j);
                    }
                }
            }
        }

        //System.out.println("resOne: " + resOne);
        //System.out.println("resTwo: " + resTwo);
        // Jedna možnost přidání do rert i nahoře po testu overlay
        if (resOne != null && resTwo != null) {
            rert.add(resOne);
            rert.add(resTwo);
            //System.out.println("if");
        } else if (partResult.size() == 1) {
            rert.add(partResult.get(0));
            //System.out.println("else if");
        } else {
            //System.out.println("else");
        }

        return rert;
    }

    private List<HashMap> reduceOverlays(List<HashMap> partResult) {
        List<HashMap> parts = new ArrayList();
        if (partResult.size() == 1) {
            parts.add(partResult.get(0));
        }
        for (int i = 0; i < partResult.size(); i++) {
            for (int j = 0; j < partResult.size(); j++) {
                if (i == j) {
                    continue;
                }
                HashMap one = partResult.get(i);
                HashMap two = partResult.get(j);
                HashMap better = getBetter(one, two);
                if ((boolean) better.get("over")) {
                    parts.add((HashMap) better.get("hash"));
                    //System.out.println("only ONE");
                } else {
                    parts.add(partResult.get(i));
                    parts.add(partResult.get(j));
                }
            }
        }
        //odstraní pokud jsou v parts duplicity
        List<HashMap> pom = new ArrayList();
        for (int i = 0; i < parts.size(); i++) {
            boolean check = true;
            for (int j = i + 1; j < parts.size(); j++) {
                if (parts.get(i) == parts.get(j)) {
                    check = false;
                    //System.out.println("DUPL");
                    break;
                }
            }
            if (check) {
                pom.add(parts.get(i));
            }
        }
        parts = pom;

        return parts;
    }

    private BufferedImage getTopPart(BufferedImage input) {

        List<HashMap> delete = getDeleteHash();
        //Smaže čísla a indexy
        for (HashMap h : delete) {
            System.out.println("h>point: " + h.get("point"));
            List<Point> points = (List<Point>) h.get("points");
            for (Point p : points) {
                input.setRGB(p.x, p.y, 16777215);
            }
        }
        repairOthers(input, delete);
        smushIt(input);
        return input;
    }

    private BufferedImage getBottomPart(BufferedImage input) {
        List<Point> points = new ArrayList();
        List<HashMap> delete = getDeleteHash();
        for (HashMap h : delete) {
            points.addAll((List<Point>) h.get("points"));
        }
        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {
                input.setRGB(i, j, 16777215);
            }
        }
        for (Point p : points) {
            input.setRGB(p.x, p.y, originalImage.getRGB(p.x, p.y));
        }
        return input;
    }

    private void getFootNumber(BufferedImage input) {

    }

    private List<HashMap> getDeleteHash() {
        List<HashMap> res = new ArrayList();
        int bestRight = 0;
        HashMap end = new HashMap();

        for (HashMap h : result) {
            System.out.println("h: " + h.get("name"));
            if (h.get("name").equals(')')) {
                if (((Point) h.get("point")).x > bestRight) {
                    res = new ArrayList();
                    bestRight = ((Point) h.get("point")).x;
                    res.add(h);
                    end = h;

                }
            }
        }
        Point zav = (Point) end.get("point");
        for (HashMap h : result) {

            if (Character.isDigit((char) h.get("name"))) {
                Point p = (Point) h.get("point");
                Dimension dim = (Dimension) h.get("dimension");
                if (p.y - 12 < zav.y && p.y + dim.height > zav.y) {
                    res.add(h);

                }
            }
        }
        Comparator comp = new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                Point one = (Point) ((HashMap) o1).get("point");
                Point two = (Point) ((HashMap) o2).get("point");
                return ((Integer) one.x).compareTo((Integer) two.x);
            }
        };
        res.sort(comp);
        footText = "<foot>";
        high = originalImage.getHeight();
        for (HashMap h : res) {

            Point p = (Point) h.get("point");
            if (p.y < high) {
                high = p.y;
            }
            footText = footText + h.get("name");
        }
        footText += "</foot>";
        return res;
    }

    private void repairOthers(BufferedImage input, List<HashMap> delete) {

        for (HashMap h : result) {
            if (delete.contains(h)) {
                continue;
            }
            if (!Character.isDigit((char) h.get("name"))) {
                List<Point> points = (List<Point>) h.get("points");
                for (Point p : points) {
                    input.setRGB(p.x, p.y, originalImage.getRGB(p.x, p.y));
                }
            }
        }
    }

// -------------------------====== UTIL METHODS ======-------------------------//
    /**
     * Veyme obrázek a rozdělí ho na jednotlivý spojitý sekvence - zbytek vymaže
     *
     * Vrací list obrázků, kde je vždy jen jedna jeho část a zbytek je vymazán
     */
    private void getSinglePartImageList() {
        FooterFinder f = new FooterFinder(image);
        partPoints = f.partPoints;

        //Util.saveImage(f.img, "/Users/Martin/Documents/zakony/big_foot/d_"+MainFoot.count+".png");
        //System.out.println("LIST.SIZZE: "+ list.size());
        int index = 0;
        BufferedImage original = Util.deepCopy(image);

        for (HashMap h : partPoints) {
            List<Point> points = (List<Point>) h.get("list");
            for (int i = 0; i < image.getWidth(); i++) {
                for (int j = 0; j < image.getHeight(); j++) {
                    image.setRGB(i, j, 16777215);
                }
            }
            for (Point p : points) {
                image.setRGB(p.x, p.y, original.getRGB(p.x, p.y));
            }
            BufferedImage addit = Util.deepCopy(image);
            imageParts.add(addit);
            index++;
        }

    }

    /**
     * Přečte všechny templaty a vratí je
     */
    private void getTemplates() {

        List<String> paths = Util.readAllFilePaths("/Users/Martin/Documents/zakony/templates/40/");
        for (String path : paths) {
            String name = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf(".txt"));
            String templateText = Util.readTXT(path);
            String rows[] = templateText.split("\n");
            int template[][] = new int[rows.length][rows[0].split(";").length];
            int rowCount = 0;
            int templateCount = 0;
            for (String row : rows) {
                String solo[] = row.split(";");
                int cellCount = 0;
                for (String cell : solo) {
                    int value = Integer.parseInt(cell) > MIN_TEMPLATE_OCCURENCE ? 1 : 0;
                    if (value == 1) {
                        templateCount++;
                    }
                    template[rowCount][cellCount] = value;
                    cellCount++;
                }
                rowCount++;
            }
            HashMap te = new HashMap();
            template = cropArray(template);
            if (name.equals(")")) {
                for (int t[] : template) {
                    //System.out.println(Arrays.toString(t));
                }

            }

            te.put("dotCount", templateCount);
            te.put("name", name.toCharArray()[0]);
            te.put("template", template);
            templates.add(te);
        }
        // ========== RESIZE ACCORDING TO SIZE OF TEXT =================//
        // zjisti velikost zavorky -> podle toho prizpusobi velikost templatu
        //if(true)return;
        int height = getReferenceHeight();
        if (height == 0) {
            System.out.println("NOT MADE");
            return;
        }
        int refHeight = 0;
        for (HashMap temp : templates) {
            if ((char) temp.get("name") == ')') {
                refHeight = ((int[][]) temp.get("template")).length;
            }
        }
        double ratio = (double) height / refHeight;

        for (HashMap temp : templates) {
            int template[][] = resizeTemplate(temp, ratio);

            //změní template na jednicky a nuly
            for (int i = 0; i < template.length; i++) {
                for (int j = 0; j < template[i].length; j++) {
                    if (template[i][j] == -1) {
                        template[i][j] = 0;
                    } else {
                        template[i][j] = 1;
                    }
                }
            }
            template = cropArray(template);
            temp.put("template", template);
        }

    }

    /**
     * Tam vrati index prvního nenulovýho sloupce v Array[][]
     *
     * @return
     */
    private int getPixelsXBegin(int[][] pixels) {
        for (int i = 0; i < pixels[0].length; i++) {
            boolean check = true;
            for (int j = 0; j < pixels.length; j++) {
                if (pixels[j][i] != -1) {
                    return i - 3 >= 0 ? i - 3 : 0;
                }
            }
        }
        System.out.println("RETURNE -1 INDEX:  " + this.getClass().getSimpleName());
        return -1;
    }

    /**
     * Tam vrati index posledního nenulovýho sloupce v Array[][]
     *
     * @return
     */
    private int getPixelsXEnd(int[][] pixels) {
        for (int i = pixels[0].length - 1; i >= 0; i--) {
            boolean check = true;

            for (int j = 0; j < pixels.length; j++) {
                if (pixels[j][i] != -1) {

                    return i + 3 >= pixels[0].length ? i : i + 3;
                }
            }

        }

        System.out.println("RETURNE -1 INDEX:  " + this.getClass().getSimpleName());
        return -1;
    }

    /**
     * Tam vrati index prvního nenulovýho readku v Array[][]
     *
     * @return
     */
    private int getPixelsYBegin(int[][] pixels) {
        for (int i = 0; i < pixels.length; i++) {
            boolean check = true;
            for (int j = 0; j < pixels[i].length; j++) {
                if (pixels[i][j] != -1) {
                    return i - 3 >= 0 ? i - 3 : 0;
                }
            }
        }
        System.out.println("RETURNE -1 INDEX:  " + this.getClass().getSimpleName());
        return -1;
    }

    /**
     * Tam vrati index posledního nenulovýho radku v Array[][]
     *
     * @return
     */
    private int getPixelsYEnd(int[][] pixels) {
        for (int i = pixels.length - 1; i >= 0; i--) {
            boolean check = true;

            for (int j = 0; j < pixels[i].length; j++) {
                if (pixels[i][j] != -1) {
                    return i + 3 >= pixels.length ? i : i + 1;
                }
            }

        }

        System.out.println("RETURNE -1 INDEX:  " + this.getClass().getSimpleName());
        return -1;
    }

    /**
     * Oddela z pole praydnz sloupce a radky
     *
     */
    private int[][] cropArray(int[][] result) {
        boolean check = true;
        for (int i = 0; i < result.length; i++) {
            if (result[i][0] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length][result[0].length - 1];
            for (int i = 0; i < result.length; i++) {
                pom[i] = Arrays.copyOfRange(result[i], 1, result[0].length);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result.length; i++) {
            if (result[i][result[i].length - 1] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length][result[0].length - 1];
            for (int i = 0; i < result.length; i++) {
                pom[i] = Arrays.copyOfRange(result[i], 0, result[0].length - 1);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result[0].length; i++) {
            if (result[0][i] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length - 1][result[0].length];
            for (int i = 1; i < result.length; i++) {
                for (int j = 0; j < result[i].length; j++) {
                    pom[i - 1][j] = result[i][j];
                }
//                pom[i] = Arrays.copyOfRange(result[i], 1, result[0].length);
            }
            result = pom;
        }
        check = true;
        for (int i = 0; i < result[0].length; i++) {
            if (result[result.length - 1][i] != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            int pom[][] = new int[result.length - 1][result[0].length];
            for (int i = 0; i < result.length - 1; i++) {
                for (int j = 0; j < result[i].length; j++) {
                    pom[i][j] = result[i][j];
                }
//                pom[i] = Arrays.copyOfRange(result[i], 0, result[0].length - 1);
            }
            result = pom;
        }
        return result;
    }

    /**
     * Zjistí, jak moc se překrývají nalezená písmenka
     *
     * @param one
     * @param two
     * @return
     */
    private int getOverlay(List<Point> one, List<Point> two) {
        int resultInt = 0;
        for (int i = 0; i < one.size(); i++) {
            for (int j = 0; j < two.size(); j++) {
                if (one.get(i).x == two.get(j).x
                        && one.get(i).y == two.get(j).y) {
                    resultInt++;
                }
            }
        }
        return resultInt;
    }

    private boolean allAround(int[][] template, int i, int j) {
        for (int k = -1; k < 2; k++) {
            for (int l = -1; l < 2; l++) {
                if (k == l || i + k < 0 || j + l < 0 || i + k >= template.length || j + l >= template[i + k].length) {
                    continue;
                }
                if (template[i + k][j + l] == 1) {
                    return false;
                }

            }
        }
        return true;
    }

    private HashMap getBetter(HashMap oneHash, HashMap twoHash) {
        int score = 0;
        List<Point> one = (List<Point>) oneHash.get("points");
        List<Point> two = (List<Point>) twoHash.get("points");
        HashMap pom = new HashMap();
        for (Point o : one) {
            for (Point t : two) {
                if (o.x == t.x && o.y == t.y) {
                    score++;
                }
            }
        }
        double shareOne = (double) score / one.size();
        double shareTwo = (double) score / two.size();
        pom.put("over", false);
        if (shareOne > 0.7 || shareTwo > 0.7) {
            pom.put("hash", shareOne > shareTwo ? twoHash : oneHash);
            pom.put("over", true);
        }

        return pom;
    }

    private boolean isTemplate(int bestScore, int dotCount, String name) {
        if (name.equals(")") && bestScore > dotCount * DOT_COUNT_SHARE * 0.3) {
            return true;
        }
        if (name.equals("y") && bestScore > dotCount * DOT_COUNT_SHARE * 0.9) {
            return true;
        }
        if (name.equals("6") && bestScore > dotCount * DOT_COUNT_SHARE * 0.3) {
            return true;
        }
        if (bestScore > dotCount * DOT_COUNT_SHARE * 0.6) {
            return true;
        }
        return false;

    }

    /**
     * Zbaví vstupní obrázek zbytečnýho bordelu - pouze šedé pixely, malý zbytky
     * po odstraňování čísel
     *
     * @param input
     */
    private void smushIt(BufferedImage input) {
        int pixs[][] = Util.getPixelArray(input);
        for (int i = 0; i < pixs.length; i++) {
            for (int j = 0; j < pixs[i].length; j++) {
                if (Math.abs(pixs[i][j]) < 5000000) {
                    input.setRGB(j, i, 16777215);
                }
            }
        }
        FooterFinder f = new FooterFinder(input);
        List<HashMap> list = f.partPoints;
        for (HashMap h : list) {
            List<Point> points = (List<Point>) h.get("list");
            if (points.size() < 10) {
                for (Point p : points) {
                    input.setRGB(p.x, p.y, 16777215);
                }
                continue;
            }
        }
    }

    public BufferedImage paintAll() {
        BufferedImage img = Util.deepCopy(originalImage);
        for (HashMap h : result) {
            int color = (int) (Math.random() * 16777215);
            List<Point> points = (List<Point>) h.get("points");
            for (Point p : points) {
                img.setRGB(p.x, p.y, color);
            }
        }
        return img;
    }

    public BufferedImage paintAll(List<HashMap> input) {
        BufferedImage img = Util.deepCopy(originalImage);
        for (HashMap h : input) {
            int color = (int) (Math.random() * 16777215);
            List<Point> points = (List<Point>) h.get("points");
            for (Point p : points) {
                img.setRGB(p.x, p.y, color);
            }
        }
        return img;
    }

    private BufferedImage getTopAndBottom(BufferedImage im) {
        List<HashMap> delete = getDeleteBottomPart();
        topPart = Util.deepCopy(im);
        for (int i = 0; i < im.getWidth(); i++) {
            for (int j = 0; j < im.getHeight(); j++) {
                im.setRGB(i, j, 16777215);
            }
        }

        for (HashMap h : delete) {
            List<Point> points = (List<Point>) h.get("points");
            for (Point p : points) {
                im.setRGB(p.x, p.y, originalImage.getRGB(p.x, p.y));
                topPart.setRGB(p.x, p.y, 16777215);
            }
        }

        smushIt(topPart);
        FooterFinder f = new FooterFinder();
        List<HashMap> list = f.getPartPoints(topPart);
        for (HashMap h : list) {
            List<Point> points = (List<Point>) h.get("list");
            int highest = topPart.getHeight();
            for (Point p : points) {
                if (p.y < highest) {
                    highest = p.y;
                }
            }
            if (highest > topPart.getHeight() / 5 * 2) {
                for (Point p : points) {
                    im.setRGB(p.x, p.y, originalImage.getRGB(p.x, p.y));
                    topPart.setRGB(p.x, p.y, 16777215);
                }
            }

        }

        lowerPart = Util.deepCopy(im);
        return im;
    }

    private List<HashMap> getDeleteBottomPart() {
        List<HashMap> res = new ArrayList();
        for (HashMap h : result) {
            Point point = (Point) h.get("point");
            if (point.y > image.getHeight() / 3) {
                res.add(h);
            }
        }
        for (HashMap h : partPoints) {
            List<Point> points = (List<Point>) h.get("list");
            int top = image.getHeight();
            for (Point p : points) {
                if (p.y < top) {
                    top = p.y;
                }
            }
            if (top > image.getHeight() / 5 * 2) {
                HashMap addit = new HashMap();
                addit.put("points", points);
                res.add(addit);
            }
        }

        return res;
    }

    private List<HashMap> noOverlays(List<HashMap> partResult) {
        List<HashMap> remove = new ArrayList();
        List<HashMap> parts = new ArrayList();
        sortPartResult(partResult);
        for (int i = 0; i < partResult.size(); i++) {
            HashMap one = partResult.get(i);
            boolean addit = true;
            for (int j = i + 1; j < partResult.size(); j++) {
                HashMap two = partResult.get(j);
                if (isOkey(one, two)) {
                    addit = false;
                    break;
                }
            }
            if (addit) {
                parts.add(one);

            } else {

            }
        }
        return parts;
    }

    private void sortPartResult(List<HashMap> partResult) {
        Comparator comp = new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                HashMap one = (HashMap) o1;
                HashMap two = (HashMap) o2;
                List<Point> listOne = (List<Point>) one.get("points");
                List<Point> listTwo = (List<Point>) two.get("points");

                return Integer.valueOf(listOne.size()).compareTo(listTwo.size());
            }
        };
        Collections.sort(partResult, comp);
    }

    private boolean isOkey(HashMap one, HashMap two) {
        List<Point> pSmaller = (List<Point>) one.get("points");
        List<Point> pBigger = (List<Point>) two.get("points");
        HashMap over = new HashMap();
        for (Point p : pBigger) {
            over.put(p, 1);
        }
        for (Point p : pSmaller) {
            if (over.get(p) != null) {
                int a = (int) over.get(p);
                a++;
                over.put(p, a);
            }
        }
        int count = 0;
        for (Object key : over.keySet()) {
            Point k = (Point) key;
            if ((int) over.get(k) > 1) {
                count++;
            }
        }
        if (pSmaller.size() * 0.3 < count) {
            return true;
        } else {
            return false;
        }
    }

    private int getReferenceHeight() {
        FooterFinder find = new FooterFinder();
        List<Rectangle> done = find.getAllDoneRectangles(originalImage);

        System.out.println("find.size: " + done.size());
        int result = 0;
        if (done.size() > 0) {
            System.out.println("done height: " + done.get(0).height);
            result = done.get(0).height;
        }
        return result;
    }

    private int[][] resizeTemplate(HashMap temp, double ratio) {
        int template[][] = (int[][]) temp.get("template");
        BufferedImage im = new BufferedImage(template[0].length, template.length, BufferedImage.TYPE_3BYTE_BGR);
        for (int i = 0; i < template.length; i++) {
            for (int j = 0; j < template[i].length; j++) {
                if (template[i][j] != 0) {
                    im.setRGB(j, i, 0);
                } else {
                    im.setRGB(j, i, 16777215);
                }
            }
        }
        BufferedImage scaled = Util.scale(im,
                BufferedImage.TYPE_3BYTE_BGR,
                ratio, ratio);
        scaled = Util.deepCopy(scaled);
        int newTemp[][] = Util.getPixelArray(scaled);
        return newTemp;
    }

    private int getDistance(int[][] template, int y, int x) {
        //obazek == 1, template v bode i j == 0....hleda se nejbližší jednička
        for (int dist = 0; true; dist++) {
            if (isTemplateDotInDistance(template, x, y, dist)) {
                return dist;
            }
        }
    }

    private boolean isTemplateDotInDistance(int[][] template, int x, int y, int dist) {
        int top = y - dist >= 0 ? y - dist : 0;
        int bottom = y + dist < template.length ? y + dist : template.length - 1;
        int left = x - dist >= 0 ? x - dist : 0;
        int right = x + dist < template[0].length ? x + dist : template[0].length - 1;

        for (int i = left; i < right; i++) {
            if (template[top][i] == 1) {
                return true;
            }
            if (template[bottom][i] == 1) {
                return true;
            }
        }
        for (int i = top; i < bottom; i++) {
            if (template[i][left] == 1) {
                return true;
            }
            if (template[i][right] == 1) {
                return true;
            }
        }
        return false;
    }

}
