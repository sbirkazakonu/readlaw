/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManualFootnotes;

import Utils.Util;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Martin
 */
class ImageSeparator {

    List<BufferedImage> parts;
    List<Rectangle> foots, all;
    BufferedImage img, originalImg;
    FooterFinder input;
    List<String> templates;

    String text;

    public ImageSeparator(FooterFinder input) {
        //templates = getTemplates();
        text = "";
        this.input = input;
        this.img = input.getImg();
        originalImg = Util.deepCopy(img);
        parts = new ArrayList();
        this.all = input.getAllRects();
        this.foots = input.getWholeFootnote();
        System.out.println("SEPARATOR2");

        separate();
        //Util.saveImage(originalImg, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + ".png");

    }

    private List<String> getTemplates() {
        List<String> templates = Util.readAllFileNames("/Users/Martin/Documents/zakony/templates/");
        for (int i = 0; i < templates.size(); i++) {
            templates.set(i, templates.get(i).replace(".txt", ""));
        }
        return templates;
    }

    public List<BufferedImage> getParts() {
        return parts;
    }

    private void separate() {
        Util.saveImage(img, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + "c.png");
        index = 0;
        
        for (Rectangle rect : foots) {

            TextFinder finder = new TextFinder();
            HashMap h = finder.TextFinderFootnote(Util.getSubImage(originalImg, rect));
            
            blankFootnote(img, h, rect);
            
            //paintRectangle(img,rect);
            //Util.saveImage(img, "/Users/Martin/Documents/zakony/test" + MainFoot.count + "_" + index + ".png");
            System.out.println("h.getName: " + h.get("text"));
            rect = new Rectangle(rect.x, rect.y + (int) h.get("high") - 1, rect.width, rect.height - (int) h.get("high"));
            BufferedImage before = getBefore(rect);
            index++;
            Util.saveImage(before, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + "_" + index + "a.png");

        }
        Util.saveImage(img, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + "_" + index + "b.png");

        MainFoot.count++;
    }
    int index = 0;

    private BufferedImage getBefore(Rectangle foot) {
        BufferedImage result = Util.deepCopy(img);
        for (Rectangle rect : all) {
            //je ke smazani nebo neni
            if (isRepairNeeded(rect, foot)) {
                repair(result, rect, foot);
            } else if (isBefore(rect, foot)) {
                blankRectangle(result, rect);
            } else {
                blankRectangle(img, rect);
            }
            //Musi byt nejaka oprava? jsou zde spojena pismena?

        }

        //Util.saveImage(img, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + "_" + index + "a.png");
        //Util.saveImage(result, "/Users/Martin/Documents/zakony/foot_parts/" + MainFoot.count + "_" + index + "b.png");
        //MainFoot.count++;
        return result;
    }

    private boolean isBefore(Rectangle rect, Rectangle foot) {
        int rectTop = rect.y;
        int rectBottom = rect.y + rect.height;
        int rectFront = rect.x;
        int rectBack = rect.x + rect.width;

        int footTop = foot.y;
        int footBottom = foot.y + foot.height;
        int footFront = foot.x;
        int footBack = foot.x + foot.width;
        int footMiddle = foot.x + foot.height / 2;
        //Cokoliv co je pod footerem (-5 je offset, obcas tam byva index)
        if (rectTop > footBottom - 5) {
            return true;
        }
        // je za footerem ale začíná až pod horní hranou footeru 
        //tzn. stejný řádek ale až za nim
        if (rectFront > footBack && rectTop > footTop - 5) {
            return true;
        }

        return false;
    }

    private boolean isRepairNeeded(Rectangle rect, Rectangle foot) {
        int rectTop = rect.y;
        int rectBottom = rect.y + rect.height;
        int rectFront = rect.x;
        int rectBack = rect.x + rect.width;

        int footTop = foot.y;
        int footBottom = foot.y + foot.height;
        int footFront = foot.x;
        int footBack = foot.x + foot.width;
        int footMiddle = foot.y + foot.height / 2;
        if (rect.height > foot.height && rectFront > footBack) {
            if (rectBottom < footBottom && footTop > rectTop && footTop < rectBottom) {
                return true;
            }
        }
        if (rect.height > foot.height && rectBack < footFront) {
            if (rectTop > footTop && footBottom > rectTop && footBottom < rectBottom) {
                return true;
            }
        }
        return false;
    }

    private void repair(BufferedImage result, Rectangle rect, Rectangle foot) {
        TextFinder finder = new TextFinder();
        BufferedImage sep = Util.getSubImage(img, rect);
        HashMap output = finder.separateConnectedLetters(sep);
        BufferedImage top = (BufferedImage) output.get("top");
        BufferedImage bottom = (BufferedImage) output.get("bottom");
        for (int i = 0; i < rect.width; i++) {
            for (int j = 0; j < rect.height; j++) {
                result.setRGB(i + rect.x, j + rect.y, top.getRGB(i, j));
                img.setRGB(i + rect.x, j + rect.y, bottom.getRGB(i, j));
            }
        }

    }

    //========================    UTIL a BORDEL    =============================//
    private BufferedImage getFoot() {
        BufferedImage result = Util.deepCopy(img);

        return result;
    }

    private BufferedImage blankRectangle(BufferedImage inputImg, Rectangle rect) {
        //16777215
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            for (int j = startY; j < startY + rect.getHeight() && j < img.getHeight(); j++) {
                // if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                inputImg.setRGB(i, j, 16777215);

            }
        }
        return inputImg;
    }

    private void paintRectangle(Rectangle rect) {
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            img.setRGB(i, (int) rect.getY(), 7000000);
            if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                img.setRGB(i, (int) ((int) rect.getY() + rect.getHeight()), 7000000);
            }

        }
        for (int i = startY; i < startY + rect.getHeight() && i < img.getHeight(); i++) {
            img.setRGB((int) rect.getX(), i, 7000000);
            img.setRGB((int) (rect.getX() + rect.getWidth()), i, 7000000);
        }
    }

    private void paintRectangle(BufferedImage image, Rectangle rect) {
        int startX = (int) rect.getX();
        int startY = (int) rect.getY();
        for (int i = startX; i < startX + rect.getWidth() && i < img.getWidth(); i++) {
            image.setRGB(i, (int) rect.getY(), 7000000);
            if ((int) ((int) rect.getY() + rect.getHeight()) != img.getHeight()) {
                image.setRGB(i, (int) ((int) rect.getY() + rect.getHeight()), 7000000);
            }
        }
        for (int i = startY; i < startY + rect.getHeight() && i < img.getHeight(); i++) {
            image.setRGB((int) rect.getX(), i, 7000000);
            image.setRGB((int) (rect.getX() + rect.getWidth()), i, 7000000);
        }
    }

    public String getText() {
        return text;
    }

    //SMaže footnote
    private void blankFootnote(BufferedImage before, HashMap f, Rectangle rect) {
        BufferedImage top = (BufferedImage) f.get("top");
        //Util.saveImage(top,"/Users/Martin/Documents/zakony/test/"+MainFoot.count+"_"+index+".png");
        if (rect.width == top.getWidth()) {
            System.out.println("OK");
        } else {
            System.out.println("NON SAME WIDTH");
        }
        if (rect.height == top.getHeight()) {
            System.out.println("OK");
        } else {
            System.out.println("NON SAME HEIGHT");
        }
        for (int i = 0; i < rect.width; i++) {
            for (int j = 0; j < rect.height; j++) {
                before.setRGB(rect.x + i, rect.y + j, top.getRGB(i, j));
            }
        }
    }

}
